<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Repository;

use App\Entity\AdminLogEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminLogEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminLogEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminLogEntity[]    findAll()
 * @method AdminLogEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminLogEntity::class);
    }

    /**
     * @param null|int $lastLogCount - The last count of AdminLogs, to return only new logs
     *
     * @return AdminLogEntity[]
     */
    public function getLatest(?int $lastLogCount): array
    {
         $result = $this->_em->getRepository(AdminLogEntity::class)->findBy([], null, null, $lastLogCount);
         return \array_reverse($result);
    }
}
