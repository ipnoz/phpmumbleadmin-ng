<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Administration\Admins;

use App\Controller\BaseController;
use App\Domain\Action\Administration\Admins\CreateAdmin\AdminCreated;
use App\Domain\Action\Administration\Admins\CreateAdmin\CreateAdminHandler;
use App\Infrastructure\Symfony\Form\CreateAdminData;
use App\Infrastructure\Symfony\Form\CreateAdminType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateAdminController extends BaseController
{
    private CreateAdminHandler $handler;

    public function __construct(CreateAdminHandler $createAdminHandler)
    {
        $this->handler = $createAdminHandler;
    }

    /**
     * @Route("/administration/admins/create", name="cmd_create_admin", methods={"POST"})
     */
    public function createAdminAction(Request $request): Response
    {
        $data = new CreateAdminData();
        $form = $this->createForm(CreateAdminType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $busResponse = $commandBus->handle($data->getCommand());
        }

        return $this->createAjaxFormResponse(AdminCreated::class, $form, $busResponse ?? null);
    }
}
