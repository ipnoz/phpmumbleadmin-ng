<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Administration\Admins;

use App\Controller\BaseController;
use App\Domain\Action\Administration\Admins\DeleteAdmin\AdminNotFound;
use App\Domain\Action\Administration\Admins\DeleteAdmin\DeleteAdminCommand;
use App\Domain\Action\Administration\Admins\DeleteAdmin\DeleteAdminHandler;
use App\Domain\Action\UnauthorizedAction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteAdminController extends BaseController
{
    private DeleteAdminHandler $handler;

    public function __construct(DeleteAdminHandler $deleteAdminHandler)
    {
        $this->handler = $deleteAdminHandler;
    }

    /**
     * @Route("/administration/admins/delete/{id}", name="cmd_delete_admin", methods={"GET"})
     */
    public function deleteAdminAction(int $id): Response
    {
        $command = new DeleteAdminCommand($id);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $busResponse = $commandBus->handle($command);

        if ($busResponse->hasEvent(UnauthorizedAction::class)) {
            throw $this->createAccessDeniedException();
        }

        if ($busResponse->hasEvent(AdminNotFound::class)) {
            throw $this->createNotFoundException();
        }

        return $this->redirectToRoute('administration_admins');
    }
}
