<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Administration\Admins;

use App\Controller\BaseController;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminCommand;
use App\Domain\Action\Administration\Admins\EditAdmin\EditAdminHandler;
use App\Entity\AdminEntity;
use App\Infrastructure\Symfony\Form\EditAdminData;
use App\Infrastructure\Symfony\Form\EditAdminType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_ACCESS_TO_ADMINISTRATION")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditAdminController extends BaseController
{
    private EditAdminHandler $handler;
    private SecurityServiceInterface $securityService;

    public function __construct(EditAdminHandler $handler, SecurityServiceInterface $securityService)
    {
        $this->handler = $handler;
        $this->securityService = $securityService;
    }

    /**
     * @Route("/administration/admins/edit/{id}", name="administration_admin_edit", methods={"GET", "POST"})
     */
    public function editAdminAction(Request $request, AdminEntity $admin): Response
    {
        if (! $this->securityService->canEditRole($admin)) {
            throw $this->createNotFoundException();
        }

        $data = EditAdminData::fromAdmin($admin);

        $form = $this->createForm(EditAdminType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = new EditAdminCommand($admin, $data->login, $data->email, $data->role, $data->password);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $commandBus->handle($command);

            return $this->redirectToRoute('administration_admin_edit', ['id' => $admin->getId()]);
        }

        return $this->render('Page/Administration/Admins/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
