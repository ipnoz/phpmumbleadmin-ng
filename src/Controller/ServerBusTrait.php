<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Domain\Murmur\Middleware\ServerBusMiddleware;
use App\Domain\Service\BusServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ServerBusTrait
{
    use ConnectionBusMiddlewareTrait;

    protected ServerBusMiddleware $serverMiddleware;

    /**
     * @required
     */
    public function setServerBusMiddleware(ServerBusMiddleware $serverMiddleware): void
    {
        $this->serverMiddleware = $serverMiddleware;
    }

    // Helper to add both connectionMiddleware and serverMiddleware to the BusService
    protected function addServerMiddlewares(BusServiceInterface $busService): void
    {
        $busService->addMiddleWare($this->connectionMiddleware);
        $busService->addMiddleWare($this->serverMiddleware);
    }
}
