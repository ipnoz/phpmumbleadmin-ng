<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Bans;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Bans\DeleteBan\DeleteBanCommand;
use App\Domain\Action\Server\Bans\DeleteBan\DeleteBanHandler;
use App\Domain\Service\FlashMessageServiceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteBanController extends ServerController
{
    private DeleteBanHandler $handler;
    private FlashMessageServiceInterface $flashMessageService;

    public function __construct(DeleteBanHandler $handler, FlashMessageServiceInterface $flashMessageService)
    {
        $this->handler = $handler;
        $this->flashMessageService = $flashMessageService;
    }

    /**
     * @Route("/server/{serverId}/cmd/ban/delete/{id}", name="cmd_server_ban_delete", methods={"GET"})
     */
    public function deleteAction(int $serverId, int $id): RedirectResponse
    {
        $command = new DeleteBanCommand($serverId, $id);

        $commandBus = $this->busFactory->buildForCommand($this->handler);
        $this->addServerMiddlewares($commandBus);

        $busResponse = $commandBus->handle($command);

        $httpCode = $this->getStatusCode($busResponse);
        $this->flashMessageService->basedOnHttpCode($httpCode, $busResponse->getEvents()[0]->getMessage());

        return $this->redirectToRoute('server_bans', ['serverId' => $serverId]);
    }
}
