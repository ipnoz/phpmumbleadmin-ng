<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\GetUserCertificate\GetUserCertificateHandler;
use App\Domain\Action\Server\Channels\GetUserCertificate\GetUserCertificateQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class GetUserCertificateController extends ServerController
{
    private GetUserCertificateHandler $handler;

    public function __construct(GetUserCertificateHandler $getUserCertificateHandler)
    {
        $this->handler = $getUserCertificateHandler;
    }

    /**
     * @Route("/server/{serverId}/user_certificate/{sessionId}", name="server_user_certificate", methods={"GET"}, options={"expose" = true})
     */
    public function getCertificateAction(int $serverId, int $sessionId): Response
    {
        $query = new GetUserCertificateQuery($serverId, $sessionId);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);
        $busResponse = $queryBus->handle($query);

        return $this->render('Page/Server/Channels/Partial/user_certificate.html.twig', [
            'certificate' => $busResponse->getData(),
        ]);
    }
}
