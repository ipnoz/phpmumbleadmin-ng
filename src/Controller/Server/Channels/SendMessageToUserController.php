<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\SendMessageToUser\MessageSentToUser;
use App\Domain\Action\Server\Channels\SendMessageToUser\SendMessageToUserCommand;
use App\Domain\Action\Server\Channels\SendMessageToUser\SendMessageToUserHandler;
use App\Infrastructure\Symfony\Form\SendMessageToUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToUserController extends ServerController
{
    private SendMessageToUserHandler $handler;

    public function __construct(SendMessageToUserHandler $sendMessageHandler)
    {
        $this->handler = $sendMessageHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/send_message_to_user", name="cmd_send_message_to_user", methods={"POST"})
     */
    public function sendMessageAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(SendMessageToUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new SendMessageToUserCommand($serverId, (int) $data['userSession'], $data['message']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(MessageSentToUser::class, $form, $busResponse ?? null);
    }
}
