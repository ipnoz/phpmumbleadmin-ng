<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\EditChannel\ChannelEdited;
use App\Domain\Action\Server\Channels\EditChannel\EditChannelCommand;
use App\Domain\Action\Server\Channels\EditChannel\EditChannelHandler;
use App\Infrastructure\Symfony\Form\EditChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditChannelController extends ServerController
{
    private EditChannelHandler $handler;

    public function __construct(EditChannelHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/server/{serverId}/cmd/edit_channel", name="cmd_edit_channel", methods={"POST"})
     */
    public function editAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(EditChannelType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $command = new EditChannelCommand(
                $serverId,
                (int) $data['parentId'],
                isset($data['defaultChannel']),
                $data['name'] ?? '',
                $data['description'] ?? '',
                $data['position'],
                $data['password'] ?? ''
            );

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(ChannelEdited::class, $form, $busResponse ?? null, [ChannelNameIsInvalid::class => 'name']);
    }
}
