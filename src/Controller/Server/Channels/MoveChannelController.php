<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\MoveChannel\ChannelMoved;
use App\Domain\Action\Server\Channels\MoveChannel\MoveChannelCommand;
use App\Domain\Action\Server\Channels\MoveChannel\MoveChannelHandler;
use App\Domain\Action\Server\Channels\MoveChannel\MoveTheRootChannelNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToChildrenChannelNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToItselfNotAllowed;
use App\Domain\Action\Server\Channels\MoveChannel\MoveToParentChannelNotAllowed;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Symfony\Form\MoveChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MoveChannelController extends ServerController
{
    private MoveChannelHandler $handler;

    public function __construct(MoveChannelHandler $moveChannelHandler)
    {
        $this->handler = $moveChannelHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/move_channel", name="cmd_move_channel", methods={"POST"})
     */
    public function moveChannelAction(int $serverId, Request $request): JsonResponse
    {
        $command = new MoveChannelCommand($serverId);

        $form = $this->createForm(MoveChannelType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);

            $busResponse = $commandBus->handle($command);
            $statusCode = $this->getStatusCode($busResponse, [
                MoveTheRootChannelNotAllowed::class => 403,
                MoveToItselfNotAllowed::class => 403,
                MoveToParentChannelNotAllowed::class => 403,
                MoveToChildrenChannelNotAllowed::class => 403,
            ]);
        }

        return $this->createXmlHttpResponse(ChannelMoved::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
