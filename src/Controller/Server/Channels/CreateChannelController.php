<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\CreateChannel\ChannelCreated;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelCommand;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelHandler;
use App\Infrastructure\Symfony\Form\CreateChannelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateChannelController extends ServerController
{
    private CreateChannelHandler $handler;

    public function __construct(CreateChannelHandler $createChannelHandler)
    {
        $this->handler = $createChannelHandler;
    }

    /**
     * @Route("/server/{serverId}/cmd/create_channel", name="cmd_create_channel", methods={"POST"})
     */
    public function createChannelAction(int $serverId, Request $request): Response
    {
        $form = $this->createForm(CreateChannelType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $command = new CreateChannelCommand($serverId, $data['name'], $data['parentId']);

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $busResponse = $commandBus->handle($command);
        }

        return $this->createAjaxFormResponse(ChannelCreated::class, $form, $busResponse ?? null, [ChannelNameIsInvalid::class => 'name']);
    }
}
