<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Channels;

use App\Controller\Server\ServerController;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\ActivatePrioritySpeakerCommand;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\ActivatePrioritySpeakerHandler;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\PrioritySpeakerActivated;
use App\Domain\Action\Server\Channels\ActivatePrioritySpeaker\PrioritySpeakerAlreadyActive;
use App\Domain\Model\HttpCode;
use App\Infrastructure\Symfony\Form\ActivatePrioritySpeakerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ActivatePrioritySpeakerController extends ServerController
{
    private ActivatePrioritySpeakerHandler $handler;

    public function __construct(ActivatePrioritySpeakerHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route(
     *     "/server/{serverId}/cmd/activate_priority_speaker",
     *     name="cmd_activate_priority_speaker",
     *     methods={"POST"}
     *     )
     */
    public function activatePriorityAction(int $serverId, Request $request): JsonResponse
    {
        $command = new ActivatePrioritySpeakerCommand($serverId);

        $form = $this->createForm(ActivatePrioritySpeakerType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);

            $busResponse = $commandBus->handle($command);
            $statusCode = $this->getStatusCode($busResponse, [PrioritySpeakerAlreadyActive::class => 403]);
        }

        return $this->createXmlHttpResponse(PrioritySpeakerActivated::class, $busResponse ?? null, $statusCode ?? HttpCode::PRECONDITION_FAILED);
    }
}
