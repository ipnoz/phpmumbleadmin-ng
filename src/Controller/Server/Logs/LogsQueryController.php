<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Server\Logs;

use App\Controller\Server\ServerQueryController;
use App\Domain\Action\Server\Logs\LogsHandler;
use App\Domain\Action\Server\Logs\LogsQuery;
use App\Domain\Action\Server\Logs\LogsViewModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_SUPER_USER_RU")
 * @Route("/server/{serverId}/logs", name="server_logs", methods={"GET"})
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LogsQueryController extends ServerQueryController
{
    private LogsHandler $handler;

    public function __construct(LogsHandler $logsHandler)
    {
        $this->handler = $logsHandler;
    }

    /**
     * @Route("", name="")
     */
    public function logsAction(Request $request, int $serverId): Response
    {
        $viewModel = new LogsViewModel($serverId);
        $query = new LogsQuery($viewModel, $serverId, -1);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);
        $statusCode = $this->getStatusCode($busResponse);

        if ($request->isXmlHttpRequest()) {
            return new Response($viewModel->getLogsBag(), $statusCode);
        }

        if ($viewModel->isRunning()) {
            $viewModel->setConnectionUri($this->getConnectionUri($query->Murmur, $query->prx));
        }

        $view = $this->renderView('Page/Server/Logs/logs.html.twig', ['viewModel' => $viewModel]);
        return new Response($view, $statusCode);
    }

    /**
     * @Route("/latest/{lastLogLen}", name="_latest", options={"expose"=true})
     */
    public function latestLogsAction(int $serverId, int $lastLogLen): Response
    {
        $viewModel = new LogsViewModel($serverId);
        $query = new LogsQuery($viewModel, $serverId, $lastLogLen);

        $queryBus = $this->busFactory->buildForQuery($this->handler);
        $this->addServerMiddlewares($queryBus);

        $busResponse = $queryBus->handle($query);

        return new Response($viewModel->getLogsBag(), $this->getStatusCode($busResponse));
    }
}
