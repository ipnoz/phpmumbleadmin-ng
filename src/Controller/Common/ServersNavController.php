<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use App\Domain\Murmur\Service\ServersListServiceInterface;
use App\Infrastructure\Service\RouterService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/cmd/servers_nav", name="cmd_servers_nav_")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServersNavController extends BaseController
{
    private ServersListServiceInterface $serversListService;
    private RouterService $routerService;

    public function __construct(ServersListServiceInterface $serversListService, RouterService $routerService)
    {
        $this->serversListService = $serversListService;
        $this->routerService = $routerService;
    }

    /**
     * @Route("/refresh_server_list", name="refresh_server_list", methods={"GET"})
     */
    public function refreshServerlistAction(Request $request): RedirectResponse
    {
        $this->serversListService->invalidateCache();

        return $this->redirectToReferer($request);
    }

    /**
     * @Route("/select_server", name="select_server", methods={"GET"})
     */
    public function selectServerAction(Request $request): RedirectResponse
    {
        $query = $request->query;
        $id = $query->get('select_server_id');

        if (\is_numeric($id)) {
            $route = $this->routerService->getNameOnServersListSelection($request);
            return $this->redirectToRoute($route, ['serverId' => $id]);
        }

        return $this->redirectToReferer($request);
    }
}
