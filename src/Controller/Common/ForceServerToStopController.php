<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Controller\Common;

use App\Controller\BaseController;
use App\Controller\ServerBusTrait;
use App\Domain\Action\Common\ForceServerToStop\ForceServerToStopCommand;
use App\Domain\Action\Common\ForceServerToStop\ForceServerToStopHandler;
use App\Infrastructure\Symfony\Form\ForceServerToStopType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("APP_CAN_EDIT_SERVER_STATUS")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ForceServerToStopController extends BaseController
{
    use ServerBusTrait;

    private ForceServerToStopHandler $handler;

    public function __construct(ForceServerToStopHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("/confirm_server_to_stop/{serverId}", name="confirm_server_to_stop", methods={"GET"})
     */
    public function displayForceServerToStopConfirmationAction($serverId): Response
    {
        $form = $this->createForm(ForceServerToStopType::class, null, [
            'action' => $this->generateUrl('cmd_forceServerToStop'),
            'serverId' => $serverId,
        ]);

        return $this->render('Page/Common/confirm_server_to_stop.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cmd/forceServerToStop", name="cmd_forceServerToStop", methods={"POST"})
     */
    public function forceServerToStopAction(Request $request): RedirectResponse
    {
        $form = $this->createForm(ForceServerToStopType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $form->get('comfirm_to_stop_server')->isClicked()) {

            $data = $form->getData();

            $command = new ForceServerToStopCommand(
                (int) $data['serverId'],
                $data['kickUsers'],
                (string) $data['message'],
                (string) $data['reason'],
                true
            );

            $commandBus = $this->busFactory->buildForCommand($this->handler);
            $this->addServerMiddlewares($commandBus);
            $commandBus->handle($command);
        }

        $session = $request->getSession();

        $referer = $session->get(StopServerController::REFERER_CONFIRM_KEY);
        $session->remove(StopServerController::REFERER_CONFIRM_KEY);
        return $this->redirect($referer);
    }
}
