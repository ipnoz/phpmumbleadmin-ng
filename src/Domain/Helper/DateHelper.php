<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DateHelper
{
    /*
     * Display a nice uptime
     *
     * @param $uptime int - uptime in second
     * @param $format int - custom uptime output format
     *
     * $format = 1 : 250 days 23:59:59
     * $format = 2 : 250 days 23:59
     * $format = 3 : 250 days
     */
    public static function uptime(int $uptime, int $format = 1): string
    {
        $str = '';

        if ($uptime < 0) {
            $uptime = abs($uptime);
        }
        if (! in_array($format,range(1,3))) {
            $format = 1;
        }
        $days = (int) floor($uptime / 86400);
        $uptime %= 86400;
        $hours = sprintf('%02d', floor($uptime / 3600));
        $uptime %= 3600;
        $mins = sprintf('%02d', floor($uptime / 60));
        $secs = $uptime % 60;
        $secs = sprintf('%02d', $secs);
        if ($days > 0) {
            if ($days === 1) {
                $str = $days.' day';
            } else {
                $str = $days.' days';
            }
            if (in_array($format,[1,2])) {
                $str .= ' ';
            }
        }
        if ($format === 1) {
            $str .= $hours.'h'.$mins.'m'.$secs.'s';
        } elseif ($format === 2 OR ($format === 3 && $days === 0)) {
            $str .= $hours.'h'.$mins.'m';
        }

        return strToLower($str);
    }

    /**
     * Transform a datetime (time format for database like mysql, sqlite)
     * to unix timestamp.
     *
     * @param $datetime - datetime string
     *
     * @return integer|false - unix timestamp or false on invalid datetime
     */
    public static function datetimeToTimestamp(string $datetime)
    {
        $dateRegex = '[0-9]{4}-[0-9]{2}-[0-9]{2}';
        $timeRegex = '[0-9]{2}:[0-9]{2}:[0-9]{2}';

        // ie: 2012-04-07 17:25:03
        $regex1 =  '/^'.$dateRegex.' '.$timeRegex.'$/';
        // ie: 2012-04-07T17:25:03
        $regex2 =  '/^'.$dateRegex.'T'.$timeRegex.'$/';
        // ie: 2012-04-07
        $regex3 =  '/^'.$dateRegex.'$/';

        /*
        * Transformation into a valid $regex1.
        */
        if (preg_match($regex2, $datetime) === 1) {
            $datetime = str_replace('T', ' ', $datetime);
        } elseif (preg_match($regex3, $datetime) === 1) {
            $datetime .= ' 00:00:00';
        }

        /*
         * Invalid datetime
         */
        if (1 !== preg_match($regex1, $datetime)) {
            return false;
        }

        /*
         * Transformation into timestamp.
         */
        list($date, $time) = explode(' ', $datetime);
        list($year, $month, $day) = explode('-', $date);
        list($hour, $minute, $second) = explode(':', $time);
        $timestamp = gmmktime(
            (int)$hour, (int)$minute, (int)$second,
            (int)$month, (int)$day, (int)$year
        );

        return $timestamp;
    }
}
