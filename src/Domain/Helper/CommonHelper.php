<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CommonHelper
{
    /*
     * Convert an array of decimal to it's character value.
     */
    public static function decimalArrayToChars(array $array): string
    {
        $result = '';

        foreach ($array as $decimal) {
            $result .= chr($decimal);
        }

        return $result;
    }

    public static function der2pem(string $der, string $type = 'CERTIFICATE'): string
    {
        $pem = \chunk_split(\base64_encode($der), 64, PHP_EOL);
        return '-----BEGIN '.$type.'-----'.PHP_EOL.$pem.'-----END '.$type.'-----'.PHP_EOL;
    }
}
