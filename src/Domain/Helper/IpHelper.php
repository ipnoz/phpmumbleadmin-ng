<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpHelper
{
    /*
     * Check for a valid IPv4.
     */
    public static function isIPv4(string $str): bool
    {
        $regexIPv4 = '/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/';
        return (preg_match($regexIPv4, $str) === 1);
    }

    /*
     * Check for a valid IPv6.
     */
    public static function isIPv6(string $str): bool
    {
        $regexIPv6 = '/^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/';
        return ($str === '::' OR preg_match($regexIPv6, $str) === 1);
    }

    /*
     * Add multiple 0 at the begining of a string.
     *
     * @param $str - The string which require to add some zero.
     * @param $limit - max lenght of the string.
     */
    public static function zeroPad(string $str, int $limit): string
    {
        $len = strlen($str);
        if ($len < $limit) {
            $str = str_repeat('0', $limit - $len).$str;
        }
        return $str;
    }

    /*
     * Transform ipv4 & ipv6 decimal array to string
     */
    public static function decimalTostring(array $array): array
    {
        if (count($array) !== 16) {
            $retval['type'] = 'invalid';
            $retval['ip'] = '';
        } elseif (
            // ipv4
            $array[0] === 0 && $array[1] === 0 && $array[2] === 0
            && $array[3] === 0 && $array[4] === 0 && $array[5] === 0
            && $array[6] === 0 && $array[7] === 0 && $array[8] === 0
            && $array[9] === 0 && $array[10] == 255 && $array[11] === 255
        ) {
            $retval['type'] = 'ipv4';
            $retval['ip'] = $array[12].'.'.$array[13].'.'.$array[14].'.'.$array[15];
            // ipv6
        } else {
            $retval['type'] = 'ipv6';
            $i = 0;
            $hex = '';
            $ipv6 = array();
            foreach ($array as $dec) {
                // Add missing zeros.
                $hex .= self::zeroPad( dechex($dec ), 2);
                ++$i;
                if ($i === 2) {
                    $ipv6[] = $hex;
                    // Reset
                    $i = 0;
                    $hex = '';
                }
            }
            $str = join(':', $ipv6);
            $retval['ip'] = self::compressIPv6String($str);
        }
        return $retval;
    }

    /*
     * Transform IPv4 mask to IPv6.
     * IPv4 range is 1-32, so in a IPv6 format, the mask can be only 96-128.
     */
    public static function mask4To6($bits)
    {
        return 128 - (32 - $bits);
    }

    /*
     * Transform IPv6 mask to IPv4.
     */
    public static function mask6To4($bits)
    {
        return 32 - (128 - $bits);
    }

    /*
     * Transform an IPv4 string address into a decimal array.
     */
    public static function stringToDecimalIPv4($str): array
    {
        $e = explode('.', $str);
        return array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, (int)$e[0], (int)$e[1], (int)$e[2], (int)$e[3]);
    }

    /*
     * Transform an IPv6 string address into a decimal array.
     */
    public static function stringToDecimalIPv6($str): array
    {
        $ip = array();

        if ($str === '::') {
            $str = '0:0:0:0:0:0:0:0';
        }
        /**
         * Uncompress ipv6
         * ie:
         * 2001:0DB8::1 => 2001:0DB8:0000:0000:0000:0000:0000:1
         */
        if (false !== strpos($str, '::')) {
            list($start, $end) = explode('::', $str);
            $c = 8 - count(explode(':', $start)) - count(explode(':', $end));
            $str = $start.':'.str_repeat('0000:', $c).$end;
        }
        /**
         * 1. Add missing zeros: ( ie: 1 => 0001,  20 => 0020, 300 => 0300).
         * 2. Split each segments into 2 other segments.
         * 3. Hexadecimal to decimal.
         */
        $array = explode(':', $str);
        foreach ($array as $segment) {
            $segment = self::zeroPad($segment, 4);
            list($a, $b) = str_split($segment, 2);
            $ip[] = hexdec($a);
            $ip[] = hexdec($b);
        }
        return $ip;
    }

    /*
     * IPv6 string address compression.
     */
    public static function compressIPv6String($str)
    {
        // Already compressed
        if (false !== strpos($str, '::')) {
            return $str;
        }
        $str = ':'.$str.':';
        // remove zeros: 0001 => 1,  0020 => 20, 0300 => 300.
        $str = preg_replace('/:00/', ':', $str);
        $str = preg_replace('/:0/', ':', $str);

        preg_match_all('/(:0)+/', $str, $matchs);

        // Compress the first match only
        if (count($matchs[0]) > 0) {
            $delimiter = $matchs[0][0];
            $str = preg_replace('/'.$delimiter.'/', ':', $str, 1);
        }

        // Remove first ":" if it's not a compression (example "::1")
        if (substr($str, 0, 2) !== '::') {
            $str = substr($str, 1);
        }
        // Remove last ":" if it's not a compression (example "Fe80::")
        if (substr($str, -2) !== '::') {
            $str = substr($str, 0, -1);
        }
        return $str;
    }
}
