<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Helper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class HtmlHelper
{
    /*
     * Find and transform a HTTP URL for HTML format in a string.
     * example : http://www.example.com
     * return : <a href="http://www.example.com">http://www.example.com</a>
     */
    public static function URLtoHTML(string $string): string
    {
        return preg_replace(
            '/https?:\/\/[\pL\pN\-\.!~?&=+\*\'"(),\/]+/',
            '<a href="$0">$0</a>',
            $string
        );
    }

    public static function spaceToEntity(?string $string): string
    {
        if ($string === null || $string === '') {
            return '';
        }

        return \str_replace(' ', '&nbsp;', $string);
    }

    /**
     * Remove all javascript from string with HTML
     */
    public static function RawWithoutJavascript(?string $html): string
    {
        if ($html === null || $html === '') {
            return '';
        }

        // Remove <script></script> tags with all the content
        $html = \preg_replace('#<script(.*?)>(.*?)</script>#', '', $html);

        if ($html === '') {
            return '';
        }

        // Remove all javascript event attributes
        try {
            $dom = new \DOMDocument;
            $dom->loadHTML(\mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        } catch (\Exception $e) {
            return $html;
        }

        $xpath = new \DOMXPath($dom);
        // Find all javascript event attributes in the DOM
        $eventAttributes = $xpath->query('//@*[starts-with(name(), on)]');

        if (0 === \count($eventAttributes)) {
            return $html;
        }

        foreach ($eventAttributes as $attribute) {
            $nodes = $xpath->query('//*[@'.$attribute->name.']');
            foreach ($nodes as $node) {
                $node->removeAttribute($attribute->name);
            }
        }

        // DOMDocument::saveHTML add <p><p> if the HTML contains only text
        \preg_match('#<body><p>(.*?)</p></body>#', $dom->saveHTML($dom->documentElement), $matches);
        if (! isset($matches[1])) {
            \preg_match('#<body>(.*?)</body>#', $dom->saveHTML($dom->documentElement), $matches);
        }

        return $matches[1] ?? '';
    }
}
