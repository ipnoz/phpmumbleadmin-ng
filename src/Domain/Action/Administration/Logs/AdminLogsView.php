<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Logs;

use App\Domain\Action\AbstractViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminLogsView extends AbstractViewModel
{
    private string $logs;

    public function getJsonLogs(): string
    {
        return $this->logs;
    }

    public function setJsonLogs(string $logs): void
    {
        $this->logs = $logs;
    }
}
