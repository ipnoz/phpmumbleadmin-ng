<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\DeleteAdmin;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AdminDeleted extends AbstractEvent
{
    public const KEY = 'admin_deleted';

    public int $userId;
    public string $username;
    public string $role;
    public int $deletedBy;

    public function __construct(int $userId, string $username, string $role, int $deletedBy)
    {
        $this->userId = $userId;
        $this->username = $username;
        $this->role = $role;
        $this->deletedBy = $deletedBy;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
