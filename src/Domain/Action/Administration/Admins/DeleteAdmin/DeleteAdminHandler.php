<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\DeleteAdmin;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Service\UserServiceInterface;
use App\Domain\Exception\NoPermissionToModifyUserException;
use App\Domain\Manager\AdminManagerInterface;
use App\Domain\Model\AdminInterface;
use App\Domain\Action\UnauthorizedAction;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteAdminHandler implements CommandHandler
{
    private AdminManagerInterface $adminManager;
    private UserServiceInterface $userService;
    private RoleNameServiceInterface $roleNameService;

    public function __construct(AdminManagerInterface $adminManager, UserServiceInterface $userService, RoleNameServiceInterface $roleNameService)
    {
        $this->adminManager = $adminManager;
        $this->userService = $userService;
        $this->roleNameService = $roleNameService;
    }

    public function listenTo(): string
    {
        return DeleteAdminCommand::class;
    }

    /**
     * @throws NoPermissionToModifyUserException
     */
    public function handle(DeleteAdminCommand $command): BusResponse
    {
        $currentUserId = $this->userService->getAdminId();

        try {
            $admin = $this->adminManager->find($command->id);
        } catch (NoPermissionToModifyUserException $e) {
            return new BusResponse([new UnauthorizedAction($currentUserId, $e)]);
        }

        if (! $admin instanceof AdminInterface) {
            return new BusResponse([new AdminNotFound($command->id)]);
        }

        $this->adminManager->delete($admin);
        $role = $this->roleNameService->find($admin);

        return new BusResponse([
            new AdminDeleted($command->id, $admin->getUsername(), $role, $currentUserId)
        ]);
    }
}
