<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Administration\Admins\CreateAdmin;

use App\Domain\Bus\EventHandler;
use App\Domain\Service\FlashMessageServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DisplayFlashMessageOnAdminCreated implements EventHandler
{
    private FlashMessageServiceInterface $flashMessage;

    public function __construct(FlashMessageServiceInterface $flashMessage)
    {
        $this->flashMessage = $flashMessage;
    }

    public function listenTo(): string
    {
        return AdminCreated::class;
    }

    public function handle(AdminCreated $event): void
    {
        $this->flashMessage->success($event->getKey());
    }
}
