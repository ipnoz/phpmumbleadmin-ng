<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Bans\DeleteBan;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BanDeleted extends AbstractEvent
{
    public const KEY = 'ban_deleted';

    public int $serverId;
    public int $banId;

    public function __construct(int $serverId, int $banId)
    {
        $this->serverId = $serverId;
        $this->banId = $banId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
