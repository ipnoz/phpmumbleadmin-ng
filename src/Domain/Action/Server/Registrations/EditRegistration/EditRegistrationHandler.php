<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Murmur\Exception\Server\InvalidUserException;
use App\Domain\Action\Server\Registrations\Registration;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditRegistrationHandler implements CommandHandler
{
    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security = $security;
    }

    public function listenTo(): string
    {
        return EditRegistrationCommand::class;
    }

    public function handle(EditRegistrationCommand $command): BusResponse
    {
        $prx = $command->prx;
        $errors = [];
        $new = [];

        $registration = new Registration($command->registrationId, $command->originalData);

        // Edit username
        if ($this->security->canModifyRegistrationUsername() && $command->username !== $registration->username) {
            if (! $prx->validateUserChars($command->username)) {
                $errors[] = new InvalidCharacterForUsername($command->serverId, $command->registrationId, $command->username);
            }
            $new[0] = $command->username;
        }

        // Edit email
        if ($command->email !== $registration->email) {
            $new[1] = $command->email;
        }

        // Edit description
        if ($command->description !== $registration->description) {
            $new[2] = $prx->checkIsAllowHtmlOrStripTags($command->description, $stripped);
            if ($stripped) {
                $errors[] = new ServerDontAllowHtmlTags($command->serverId, $command->registrationId, $command->description);
            }
        }

        // Edit password
        if ($this->security->canModifyRegistrationPassword() && '' !== $command->plainPassword) {
            $new[4] = $command->plainPassword;
        }

        // Do not update registration on error(s)
        if (! empty($errors)) {
            return new BusResponse($errors);
        }

        // Update registration
        if (! empty($new)) {

            $update = \array_replace($command->originalData, $new);

            try {
                $prx->updateRegistration($command->registrationId, $update);
            } catch (InvalidUserException $e) {
                $event = new UsernameAlreadyExist($command->serverId, $command->username);
                return new BusResponse([$event]);
            }

            $event = new RegistrationEdited($command->serverId, $command->registrationId);
            return new BusResponse([$event]);
        }

        return new BusResponse([]);
    }
}
