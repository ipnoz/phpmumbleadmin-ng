<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\DeleteRegistration;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class DeleteRegistrationCommand
{
    use ServerActionTrait;

    public int $registrationId;

    public function __construct(int $serverId, int $registrationId)
    {
        $this->serverId = $serverId;
        $this->registrationId = $registrationId;
    }
}
