<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Registrations\Registration;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class RegistrationDoNotExist extends AbstractEvent
{
    public const KEY = 'user_registration_do_not_exist';

    public int $serverId;
    public int $registrationId;

    public function __construct(int $serverId, int $registrationId)
    {
        $this->serverId = $serverId;
        $this->registrationId = $registrationId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
