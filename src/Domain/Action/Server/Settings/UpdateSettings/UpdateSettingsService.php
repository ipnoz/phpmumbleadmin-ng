<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Settings\UpdateSettings;

use App\Domain\Murmur\Exception\Server\ServerException;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Murmur\Model\ServerSetting;
use App\Domain\Murmur\Model\ServerSettingsManager;
use App\Domain\Service\FlashMessageServiceInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UpdateSettingsService
{
    private FlashMessageServiceInterface $flashMessage;
    private ServerSettingsManager $parametersManager;

    private ServerInterface $prx;
    private array $defaultSettingValues;

    /** @var ServerSetting[] */
    private array $parameters;

    public function __construct(FlashMessageServiceInterface $flashMessage, ServerSettingsManager $parametersManager)
    {
        $this->flashMessage = $flashMessage;
        $this->parametersManager = $parametersManager;

        $this->defaultSettingValues = [];
    }

    public function setDefaultSettingValues(array $defaultSettingValues): void
    {
        $this->defaultSettingValues = $defaultSettingValues;
    }

    public function setServer(ServerInterface $prx): void
    {
        $this->prx = $prx;
    }

    /**
     * @return ServerSetting[]
     *
     * @throws ServerException|\Exception
     */
    function getSettings(): array
    {
        if (isset($this->parameters)) {
            return $this->parameters;
        }

        $this->parameters = [];
        $parameters = $this->parametersManager->getAll();
        $customSettingsValues = $this->prx->getCustomConf();

        foreach ($parameters as $parameter) {

            // Set default parameters
            if (isset($this->defaultSettingValues[$parameter->getKey()])) {
                $parameter->setDefaultValue($this->defaultSettingValues[$parameter->getKey()]);
            }

            // Set custom parameters
            if (isset($customSettingsValues[$parameter->getKey()])) {
                $parameter->setCustomValue($customSettingsValues[$parameter->getKey()]);
            }

            $this->parameters[$parameter->getKey()] = $parameter;
        }

        return $this->parameters;
    }

    public function saveSettingsOfServer(array $data): void
    {
        if (! isset($this->parameters)) {
            $this->getSettings();
        }

        foreach ($data as $key => $value) {

            if (\in_array($key, ['key', 'certificate'])) {
                continue;
            }

            $parameter = $this->getSetting($key);

            if (! $parameter instanceof ServerSetting) {
                continue;
            }

            // On empty value
            if ('' === $value || \is_null($value)) {
                // Remove custom parameter if exists
                if ($parameter->isModified()) {
                    $this->setConf($parameter->getKey(), '');
                }
                continue;
            }

            if (\is_int($value)) {
                $value = (string) $value;
            }

            // Do not add custom value if it's the same as the $defaultConf.
            if ($value === $parameter->getDefaultValue()) {
                // A custom value is set for the key, remove it.
                if ($parameter->isModified()) {
                    $this->setConf($parameter->getKey(), '');
                }
                continue;
            }

            // The custom value didn't change, do anything.
            if ($value === $parameter->getCustomValue()) {
                continue;
            }

            if ('host' === $parameter->getKey()) {
                // Host particularity
                if ($this->prx->isRunning()) {
                    $this->flashMessage->success('host_modified_success_restart_needed');
                }
            } elseif ('port' === $parameter->getKey()) {
                // Port particularity
                if ($this->prx->isRunning()) {
                    $this->flashMessage->success('port_modified_success_restart_needed');
                }
            }

            $this->setConf($parameter->getKey(), $value);
        }

        if (! empty($data['key']) && ! empty($data['certificate'])) {
            $this->setCertificate($data['key'], $data['certificate']);
        }
    }

    /**
     * @param mixed $key
     */
    private function getSetting($key): ?ServerSetting
    {
        return $this->parameters[$key] ?? null;
    }

    private function setCertificate(string $privateKey, string $certificate): void
    {
        $this->setConf('key', $privateKey);
        $this->setConf('certificate', $certificate);

        if ($this->prx->isRunning()) {
            $this->flashMessage->success('certificate_modified_success_restart_needed');
        }
    }

    private function setConf(string $parameter, $value): void
    {
        // Memo:
        // Murmur_prx::setConf method with "zero", "one" or "multiple" spaces
        // will always remove the key in murmur DB.
        $this->prx->setConf($parameter, (string) $value);
    }
}
