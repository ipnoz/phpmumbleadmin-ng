<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Action\Server\Channels\UserNotConnected;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Murmur\Exception\Server\InvalidSessionException;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Helper\HtmlHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SendMessageToUserHandler implements CommandHandler
{
    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security = $security;
    }

    public function listenTo(): string
    {
        return SendMessageToUserCommand::class;
    }

    public function handle(SendMessageToUserCommand $command): BusResponse
    {
        $prx = $command->prx;
        $message = $command->message;
        $stripped = false;

        // Nothing to send
        if ('' === $message) {
            return new BusResponse([new EmptyMessage($command->serverId, $command->userSessionId)]);
        }

        // If the virtual server doesn't allow HTML and the user in not at least
        // ROOT_ADMIN, remove HTML tags
        if (! $this->security->isGrantedRootAdmin()) {
            $message = $prx->checkIsAllowHtmlOrStripTags($message, $stripped);
        }

        $message = HtmlHelper::URLtoHTML($message);

        try {
            $prx->sendMessage($command->userSessionId, $message);
        } catch (InvalidSessionException $e) {
            return new BusResponse([new UserNotConnected($command->serverId, $command->userSessionId)]);
        }

        return new BusResponse([new MessageSentToUser($command->serverId, $command->userSessionId, $stripped)]);
    }
}
