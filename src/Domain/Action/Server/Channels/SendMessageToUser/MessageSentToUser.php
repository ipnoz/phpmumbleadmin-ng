<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MessageSentToUser extends AbstractEvent
{
    public const KEY = 'message_sent_to_user';

    public int $serverId;
    public int $userSessionId;
    public bool $stripped;

    public function __construct(int $serverId, int $userSessionId, bool $stripped)
    {
        $this->serverId = $serverId;
        $this->userSessionId = $userSessionId;
        $this->stripped = $stripped;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
