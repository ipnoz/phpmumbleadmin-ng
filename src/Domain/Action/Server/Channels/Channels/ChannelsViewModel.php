<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\Channels;

use App\Domain\Action\Server\ServerViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelsViewModel extends ServerViewModel
{
    private $jsonViewer;
    private bool $isGrantedToModifyUserName;

    public function __construct(int $serverId)
    {
        $this->isGrantedToModifyUserName = false;

        parent::__construct($serverId);
    }

    public function getJsonViewer(): ?string
    {
        return $this->jsonViewer;
    }

    public function setJsonViewer(?string $jsonViewer): void
    {
        $this->jsonViewer = $jsonViewer;
    }

    public function getIsGrantedToModifyUserName(): bool
    {
        return $this->isGrantedToModifyUserName;
    }

    public function setIsGrantedToModifyUserName(bool $isGranted): void
    {
        $this->isGrantedToModifyUserName = $isGranted;
    }
}
