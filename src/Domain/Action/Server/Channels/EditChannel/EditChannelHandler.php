<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\NestingLimitReached;
use App\Domain\Action\Server\Channels\RequestedChannelNotExist;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Exception\Server\NestingLimitException;
use App\Domain\Murmur\Manager\AclManager;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EditChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return EditChannelCommand::class;
    }

    public function handle(EditChannelCommand $command): BusResponse
    {
        $prx = $command->prx;

        try {
            $channelState = $prx->getChannelState($command->channelId);
        } catch (InvalidChannelException $e) {
            return new BusResponse([new RequestedChannelNotExist($command->serverId, $command->channelId)]);
        }

        // Set channel name
        // Disallow to edit the Root channel name, because murmur will not keep
        // the state, and the Mumble UI disallow the action
        if ($command->channelId > 0 && $command->name !== $channelState->name) {

            if (! $prx->validateChannelChars($command->name)) {
                return new BusResponse([new ChannelNameIsInvalid($command->serverId, $command->channelId, $command->name)]);
            }

            $channelState->name = $command->name;
        }

        // Set channel description
        if ($command->description !== $channelState->description) {
            // As anybody can modify channel description, always remove HTML tags
            $channelState->description = $prx->checkIsAllowHtmlOrStripTags($command->description, $stripped);
            if ($stripped) {
                return new BusResponse([new ServerDontAllowHtmlTags($command->serverId, $command->channelId, $command->name)]);
            }
        }

        // Set channel position
        if (\is_numeric($command->position) || '' === $command->position) {
            $channelState->position = (int) $command->position;
        } else {
            return new BusResponse([new InvalidNumericalForPosition($command->serverId, $command->channelId, $command->position)]);
        }

        try {
            $prx->setChannelState($channelState);
        } catch (NestingLimitException $e) {
            return new BusResponse([new NestingLimitReached()]);
        }

        // Set default channel
        if ($command->default) {

            if ($channelState->temporary) {
                return new BusResponse([new CantEditChannelWhenIsTemporary($command->serverId, $command->channelId)]);
            }

            // Memo: setConf() require a string for second parameter
            $prx->setConf('defaultchannel', (string) $command->channelId);
        }

        // Set channel password
        $prx->getACL($command->channelId, $aclList, $groupList, $inherit);

        $aclManager = new AclManager($aclList, $groupList, $inherit);

        if ('' !== $command->password) {
            $aclManager->setPassword($command->password);
        } else {
            $aclManager->removePassword();
        }

        $prx->setACL($command->channelId, $aclManager->getAclList(), $aclManager->getGroupList(), $aclManager->getInherit());

        return new BusResponse([new ChannelEdited($command->serverId, $command->channelId)]);
    }
}
