<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\EditChannel;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class InvalidNumericalForPosition extends AbstractEvent
{
    public const KEY = 'invalid_numerical_for_position';

    public int $serverId;
    public int $channelId;
    public string $position;

    public function __construct(int $serverId, int $channelId, string $position)
    {
        $this->serverId = $serverId;
        $this->channelId = $channelId;
        $this->position = $position;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
