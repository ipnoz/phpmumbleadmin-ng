<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\KickUser;

use App\Domain\Action\Server\Channels\UserNotConnected;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class KickUserHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return KickUserCommand::class;
    }

    public function handle(KickUserCommand $command): BusResponse
    {
        $prx = $command->prx;

        $users = $prx->getUsers();

        if (! isset($users[$command->userSessionId])) {
            return new BusResponse([new UserNotConnected($command->serverId, $command->userSessionId)]);
        }

        $user = $users[$command->userSessionId];

        $prx->kickUser($command->userSessionId, $command->reason);

        $event = new UserKicked($command->serverId, $command->userSessionId, $user->name);
        return new BusResponse([$event]);
    }
}
