<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\KickUser;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UserKicked extends AbstractEvent
{
    public const KEY = 'user_has_been_kicked_off_the_server';

    public int $serverId;
    public int $userSessionId;
    public string $login;

    public function __construct(int $serverId, int $userSessionId, string $login)
    {
        $this->serverId = $serverId;
        $this->userSessionId = $userSessionId;
        $this->login = $login;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    public function getTranslationDomain(): string
    {
        return 'channels';
    }

    public function getTranslationParameters(): array
    {
        return ['%user%' => $this->login];
    }
}
