<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\LinkChannel;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class LinkChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return LinkChannelCommand::class;
    }

    public function handle(LinkChannelCommand $command): BusResponse
    {
        if ($command->channelId === $command->withChannelId) {
            $event = new CannotLinkSelf($command->serverId, $command->channelId, $command->withChannelId);
            return new BusResponse([$event]);
        }

        $prx = $command->prx;

        $channelState = $prx->getChannelState($command->channelId);
        $channelState->links[] = $command->withChannelId;
        $prx->setChannelState($channelState);

        $event = new ChannelLinked($command->serverId, $command->channelId, $command->withChannelId);
        return new BusResponse([$event]);
    }
}
