<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\DeactivatePrioritySpeaker;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class PrioritySpeakerNotActive extends AbstractEvent
{
    public const KEY = 'priority_speaker_not_active';

    public int $serverId;
    public int $userSession;

    public function __construct(int $serverId, int $userSession)
    {
        $this->serverId = $serverId;
        $this->userSession = $userSession;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
