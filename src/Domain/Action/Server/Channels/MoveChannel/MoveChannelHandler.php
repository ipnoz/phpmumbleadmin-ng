<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\MoveChannel;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Murmur\Exception\Server\InvalidChannelException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MoveChannelHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return MoveChannelCommand::class;
    }

    public function handle(MoveChannelCommand $command): BusResponse
    {
        $prx = $command->prx;

        // Move the root channel do not throw any error, but Murmur will not do
        // it. It's better to inform the user in my opinion.
        if ($command->channelId === 0) {
            return new BusResponse([new MoveTheRootChannelNotAllowed()]);
        }

        if ($command->channelId === $command->toChannelId) {
            return new BusResponse([new MoveToItselfNotAllowed()]);
        }

        $state = $prx->getChannelState($command->channelId);

        // Move a channel to it's parent do not throw any error, but Murmur will
        // not do it. So it's better to inform the user in my opinion.
        if ($state->parent === $command->toChannelId) {
            return new BusResponse([new MoveToParentChannelNotAllowed()]);
        }

        $state->parent = $command->toChannelId;

        try {
            $prx->setChannelState($state);
        } catch (InvalidChannelException $e) {
            // In this case, the probably is to trying to move to a children channel.
            return new BusResponse([new MoveToChildrenChannelNotAllowed()]);
        }

        return new BusResponse([new ChannelMoved()]);
    }
}
