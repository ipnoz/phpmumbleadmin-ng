<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UserNameModified extends AbstractEvent
{
    public const KEY = 'user_name_modified';

    public int $serverId;
    public int $userSessionId;
    public string $oldName;
    public string $newName;

    public function __construct(int $serverId, int $userSessionId, string $oldName, string $newName)
    {
        $this->serverId = $serverId;
        $this->userSessionId = $userSessionId;
        $this->oldName = $oldName;
        $this->newName = $newName;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
