<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Murmur\Exception\Server\InvalidSessionException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ModifyUserNameHandler implements CommandHandler
{
    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security = $security;
    }

    public function listenTo(): string
    {
        return ModifyUserNameCommand::class;
    }

    public function handle(ModifyUserNameCommand $command): BusResponse
    {
        $prx = $command->prx;

        if (! $this->security->canModifyMumbleUsernameOnChannelTree($command->Murmur)) {
            return new BusResponse([new CantModifyUserName()]);
        }

        try {
            $user = $prx->getState($command->userSessionId);
        } catch (InvalidSessionException $e) {
            $event = new UserNotConnected($command->serverId, $command->userSessionId);
            return new BusResponse([$event]);
        }

        if ($user->name === $command->newName || '' === $command->newName) {
            $event = new InvalidNameArgument($command->serverId, $command->userSessionId, $command->newName);
            return new BusResponse([$event]);
        }

        $oldName = $user->name;
        $user->name = $command->newName;
        $prx->setState($user);

        $event = new UserNameModified($command->serverId, $command->userSessionId, $oldName, $command->newName);
        return new BusResponse([$event]);
    }
}
