<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Server\Channels\MoveUser;

use App\Domain\Murmur\Model\ServerActionTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MoveUserCommand
{
    use ServerActionTrait;

    public int $sessionId;
    public int $toChannelId;

    public function __construct(int $serverId, int $sessionId, int $toChannelId)
    {
        $this->serverId = $serverId;
        $this->sessionId = $sessionId;
        $this->toChannelId = $toChannelId;
    }
}
