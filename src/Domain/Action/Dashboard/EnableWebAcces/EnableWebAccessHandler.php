<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\EnableWebAcces;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class EnableWebAccessHandler implements CommandHandler
{
    public function listenTo(): string
    {
        return EnableWebAccessCommand::class;
    }

    public function handle(EnableWebAccessCommand $command): BusResponse
    {
        $prx = $command->prx;
        $prx->setConf('PMA_permitConnection', 'true');
        return new BusResponse([new WebAccessEnabled($prx->getSid())]);
    }
}
