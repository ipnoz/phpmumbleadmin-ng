<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\CreateServer;

use App\Domain\Action\Server\Settings\UpdateSettings\UpdateSettingsService;
use App\Domain\Bus\BusResponse;
use App\Domain\Bus\CommandHandler;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateServerHandler implements CommandHandler
{
    private UpdateSettingsService $settingsManager;

    public function __construct(UpdateSettingsService $settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function listenTo(): string
    {
        return CreateServerCommand::class;
    }

    public function handle(CreateServerCommand $command): BusResponse
    {
        $Murmur = $command->Murmur;
        $prx = $Murmur->newServer();
        $prx->setConf('boot', 'false');

        $data = [];

        if (\is_string($command->registername)) {
            $data['registername'] = $command->registername;
        }

        if (\is_string($command->password)) {
            $data['password'] = $command->password;
        }

        if (\is_int($command->users)) {
            $data['users'] = $command->users;
        }

        if (! empty($data)) {
            $this->settingsManager->setDefaultSettingValues($command->Murmur->getDefaultConf());
            $this->settingsManager->setServer($prx);
            $this->settingsManager->saveSettingsOfServer($data);
        }

        return new BusResponse([new ServerCreated($prx->getSid())]);
    }
}
