<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\Dashboard;

use App\Domain\Model\PaginatedInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DashboardView
{
    public string $murmur_version;
    public int $murmurStartedAt;
    public int $total_of_servers = 0;
    public int $total_of_booted_servers = 0;
    public int $total_of_connected_users = 0;
    public PaginatedInterface $servers;
}
