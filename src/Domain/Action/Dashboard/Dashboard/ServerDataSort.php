<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerDataSort
{
    private string $direction;

    public function __construct(string $direction = 'asc')
    {
        $this->direction = (\strtolower($direction) === 'desc') ? 'desc' : 'asc';
    }

    public function sortByIsBooted(array &$data): void
    {
        \usort($data, 'self::compareIsBooted');
    }

    // Compare isBooted status, and if it's equal, compare the key
    private function compareIsBooted(ServerData $a, ServerData $b)
    {
        if ($a->isBooted !== $b->isBooted) {
            return ($this->direction === 'asc')
                ? $a->isBooted < $b->isBooted
                : $a->isBooted > $b->isBooted;
        }

        return ($this->direction === 'asc')
            ? $a->key > $b->key
            : $a->key < $b->key;
    }
}
