<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\MurmurDefaultConfig;

use App\Domain\Bus\BusResponse;
use App\Domain\Bus\QueryHandler;
use App\Domain\Manager\CertificateManager;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MurmurDefaultConfigHandler implements QueryHandler
{
    public function listenTo(): string
    {
        return MurmurDefaultConfigQuery::class;
    }

    public function handle(MurmurDefaultConfigQuery $query): BusResponse
    {
        $query->view->setHasConnection();

        $defaultConf = $query->Murmur->getDefaultConf();

        // Murmur 1.3.0 do not expose private key anymore
        $defaultConf['key'] = $defaultConf['key'] ?? '';

        $certificateManager = new CertificateManager($defaultConf['key'].$defaultConf['certificate']);

        $query->view->setKey($defaultConf['key']);
        $query->view->setCertificate($defaultConf['certificate']);
        $query->view->setCertificateInformation($certificateManager->parse());

        unset($defaultConf['key'], $defaultConf['certificate']);

        $query->view->setDefaultConfiguration($defaultConf);
        return new BusResponse();
    }
}
