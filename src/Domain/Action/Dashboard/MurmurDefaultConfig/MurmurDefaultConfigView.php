<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Dashboard\MurmurDefaultConfig;

use App\Domain\Action\Common\MurmurConnectionViewModel;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurDefaultConfigView extends MurmurConnectionViewModel
{
    private array $defaultConfiguration;
    private array $certificateInformation;
    private string $certificate;
    private string $key;

    public function getDefaultConfiguration(): array
    {
        return $this->defaultConfiguration;
    }

    public function setDefaultConfiguration(array $defaultConfiguration): void
    {
        $this->defaultConfiguration = $defaultConfiguration;
    }

    public function getCertificateInformation(): array
    {
        return $this->certificateInformation;
    }

    public function setCertificateInformation(array $certificateInformation): void
    {
        $this->certificateInformation = $certificateInformation;
    }

    public function getCertificate(): string
    {
        return $this->certificate;
    }

    public function setCertificate(string $certificate): void
    {
        $this->certificate = $certificate;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }
}
