<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class UnauthorizedAction extends AbstractEvent
{
    public const KEY = 'unauthorized_action';

    public int $userId;
    public \Exception $exception;
    public string $action;

    public function __construct(int $userId, \Exception $exception, string $action = '')
    {
        $this->userId = $userId;
        $this->action = $action;
        $this->exception = $exception;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
