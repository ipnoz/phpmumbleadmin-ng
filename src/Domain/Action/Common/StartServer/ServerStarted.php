<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Action\Common\StartServer;

use App\Domain\Bus\AbstractEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ServerStarted extends AbstractEvent
{
    public const KEY = 'server_started';

    public int $serverId;

    public function __construct(int $serverId)
    {
        $this->serverId = $serverId;
    }

    public function getKey(): string
    {
        return self::KEY;
    }
}
