<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Viewer;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Viewer
{
    public int $serverId;
    public array $nodes;

    public function __construct(int $serverId, array $nodes)
    {
        $this->serverId = $serverId;
        $this->nodes = $nodes;
    }
}
