<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Middleware;

use App\Domain\Bus\BusResponse;
use App\Domain\Murmur\Connection\ConnectionHandlerInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Service\BusMiddlewareInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionBusMiddleware implements BusMiddlewareInterface
{
    public const MURMUR_VERSION_STRING = 'MURMUR_VERSION_STRING';

    private ConnectionHandlerInterface $connection;

    public function __construct(ConnectionHandlerInterface $connection)
    {
        $this->connection = $connection;
    }

    public function execute($action, callable $next): BusResponse
    {
        try {
            $action->Murmur = $this->connection->connect();
            $_SERVER[self::MURMUR_VERSION_STRING] = $action->Murmur->getMurmurVersion()['str'];
        } catch (ConnectionException $e) {
            return new BusResponse([new ConnectionError($e->getMessage())], \get_class($e));
        }

        return $next($action);
    }
}
