<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface MetaInterface
{
    /**
     * @return ServerInterface[]
     */
    public function getAllServers(): array;

    /**
     * @return ServerInterface[]
     */
    public function getBootedServers(): array;

    public function newServer(): ?ServerInterface;

    public function getServer(int $id): ?ServerInterface;

    public function getUptime(): int;

    public function getDefaultConf(): array;

    public function getVersion(&$major, &$minor, &$patch, &$text): void;

    public function getSliceChecksums(): array;

    public function getMurmurVersion(): array;
}
