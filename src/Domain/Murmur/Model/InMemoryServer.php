<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

use App\Domain\Murmur\Exception\Server\InvalidChannelException;
use App\Domain\Murmur\Exception\Server\InvalidSessionException;
use App\Domain\Murmur\Factory\MurmurObjectFactory;
use App\Domain\Murmur\Model\Mock\AclMock;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\Mock\GroupMock;
use App\Domain\Murmur\Model\Mock\UserMock;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InMemoryServer extends AbstractServer
{
    private int $id;
    private bool $isRunning;
    private array $conf = [];
    /** @var BanMock[] $bans */
    private array $bans = [];
    /** @var ChannelMock[] $channels */
    private array $channels = [];
    /** @var UserMock[] $users */
    private array $users = [];
    private array $acl = [];

    public function __toString(): string
    {
        return 's/' . $this->getSid();
    }

    public function getSid(): int
    {
        return $this->id;
    }

    public function with_id(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function with_default_conf(array $data): self
    {
        $this->defaultConf = $data;
        return $this;
    }

    public function addChannel(string $name, int $parent): int
    {
        // TODO: Implement addChannel() method.
    }

    public function delete(): void
    {
        // TODO: Implement delete() method.
    }

    public function getACL(int $channelId, &$aclList, &$groupList, &$inherit): void
    {
        if (! isset($this->channels[$channelId])) {
            throw new InvalidChannelException();
        }

        $aclList = $this->acl[$channelId]['aclList'];
        $groupList = $this->acl[$channelId]['aclGroup'];
        $inherit = $this->acl[$channelId]['inherit'];
    }

    public function getAllConf(): array
    {
        // TODO: Implement getAllConf() method.
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomConf(): array
    {
        return $this->conf;
    }

    public function with_custom_conf(array $data): self
    {
        $this->conf = $data;
        return $this;
    }

    public function getBans(): array
    {
        return $this->bans;
    }

    public function getCertificateList(int $sessionId): array
    {
        // TODO: Implement getCertificateList() method.
    }

    /**
     * {@inheritDoc}
     */
    public function getChannelState(int $channelId): object
    {
        foreach ($this->channels as $channel) {
            if ($channel->id === $channelId) {
                return $channel;
            }
        }

        throw new InvalidChannelException();
    }

    /**
     * {@inheritDoc}
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    public function with_channels(array $channels): self
    {
        foreach ($channels as $channel) {
            if (! $channel instanceof ChannelMock) {
                throw new \InvalidArgumentException();
            }

            $this->channels[$channel->id] = $channel;
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getConf(string $key): string
    {
        return $this->conf[$key] ?? '';
    }

    public function getLog(int $first, int $last): array
    {
        // TODO: Implement getLog() method.
    }

    public function getLogLen(): int
    {
        // TODO: Implement getLogLen() method.
    }

    public function getRegisteredUsers(string $filter): array
    {
        // TODO: Implement getRegisteredUsers() method.
    }

    public function getRegistration(int $userId): array
    {
        // TODO: Implement getRegistration() method.
    }

    public function getState(int $sessionId): object
    {
        // TODO: Implement getState() method.
    }

    public function getTexture(int $userId): array
    {
        // TODO: Implement getTexture() method.
    }

    public function getTree(): object
    {
        // TODO: Implement getTree() method.
    }

    public function getUptime(): int
    {
        // TODO: Implement getUptime() method.
    }

    /**
     * {@inheritDoc}
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    public function with_users(array $users): self
    {
        foreach ($users as $user) {
            if (! $user instanceof UserMock) {
                throw new \InvalidArgumentException();
            }
        }

        $this->users = $users;

        return $this;
    }

    public function hasPermission(int $sessionId, int $channelId, int $permission): bool
    {
        // TODO: Implement hasPermission() method.
    }

    public function id(): int
    {
        // TODO: Implement id() method.
    }

    public function isRunning(): bool
    {
        return $this->isRunning;
    }

    /**
     * {@inheritDoc}
     */
    public function kickUser(int $sessionId, string $reason): void
    {
        foreach ($this->users as $key => $user) {
            if ($user->session === $sessionId) {
                unset($this->users[$key]);
                return;
            }
        }

        throw new InvalidSessionException();
    }

    public function registerUser(array $userInfoMap): int
    {
        // TODO: Implement registerUser() method.
    }

    public function removeChannel(int $channelId): void
    {
        // TODO: Implement removeChannel() method.
    }

    public function sendMessage(int $sessionId, string $text): void
    {
        // TODO: Implement sendMessage() method.
    }

    public function sendMessageChannel(int $channelId, bool $tree, string $text): void
    {
        // TODO: Implement sendMessageChannel() method.
    }

    public function setACL(int $channelId, array $aclList, array $aclGroup, bool $inherit): void
    {
        if (! isset($this->channels[$channelId])) {
            throw new InvalidChannelException();
        }

        foreach ($aclList as $acl) {
            if (! $acl instanceof AclMock) {
                throw new \InvalidArgumentException('invalid value for sequence element `__MOCK::Murmur::ACLList');
            }
        }

        foreach ($aclGroup as $group) {
            if (! $group instanceof GroupMock) {
                throw new \InvalidArgumentException('invalid value for sequence element `__MOCK::Murmur::GroupList');
            }
        }

        $this->acl[$channelId] = [
            'aclList' => $aclList,
            'aclGroup' => $aclGroup,
            'inherit' => $inherit,
        ];
    }

    public function setBans(array $banList): void
    {
        try {
            $this->bans = MurmurObjectFactory::transformBansToBanMocks($banList);
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException('invalid value for sequence element `__MOCK::Murmur::BanList');
        }
    }

    public function setChannelState(object $channel): void
    {
        // TODO: Implement setChannelState() method.
    }

    /**
     * {@inheritDoc}
     */
    public function setConf(string $key, string $value): void
    {
        $this->conf[$key] = $value;
    }

    public function setState(object $user): void
    {
        // TODO: Implement setState() method.
    }

    public function setTexture(int $userId, array $texture): void
    {
        // TODO: Implement setTexture() method.
    }

    /**
     * {@inheritDoc}
     */
    public function start(): void
    {
        $this->isRunning = true;
    }

    public function is_started(): self
    {
        $this->isRunning = true;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function stop(): void
    {
        $this->isRunning = false;
    }

    public function is_stopped(): self
    {
        $this->isRunning = false;
        return $this;
    }

    public function unregisterUser(int $userId): void
    {
        // TODO: Implement unregisterUser() method.
    }

    public function updateRegistration(int $userId, array $userInfoMap): void
    {
        // TODO: Implement updateRegistration() method.
    }

    public function verifyPassword(string $name, string $password): int
    {
        // TODO: Implement verifyPassword() method.
    }
}
