<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Model;

use App\Domain\Murmur\Factory\ExceptionFactory;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractServer implements ServerInterface
{
    // List of default parameters of Murmur
    protected array $defaultConf;

    protected function exceptionFactory(\Exception $e): \Exception
    {
        return ExceptionFactory::get($e);
    }

    /**
     * {@inheritDoc}
     */
    public function kickAllUsers($message = ''): void
    {
        foreach ($this->getUsers() as $user) {
            $this->kickUser($user->session, $message);
        }
    }

    protected function validateCharsFor(string $key, string $string): bool
    {
        // Get pattern in config
        $pattern = $this->getParameter($key);
        return (1 === \preg_match('/^'.$pattern.'$/u', $string));
    }

    public function validateUserChars(string $string): bool
    {
        return $this->validateCharsFor('username', $string);
    }

    public function validateChannelChars(string $string): bool
    {
        return $this->validateCharsFor('channelname', $string);
    }

    public function isAllowHtml(): bool
    {
        return 'true' === $this->getParameter('allowhtml');
    }

    public function getServerName(): string
    {
        return $this->getParameter('registername');
    }

    /**
     * {@inheritDoc}
     */
    public function checkIsAllowHtmlOrStripTags(string $string, &$stripped): string
    {
        $stripTags = $string;

        if (! $this->isAllowHtml()) {
            $stripTags = \strip_tags($string);
        }

        $stripped = ($string !== $stripTags);

        return $stripTags;
    }

    /**
     * {@inheritDoc}
     */
    public function getParameter(string $key): string
    {
        $customConf = $this->getCustomConf();

        if (isset($customConf[$key])) {
            $parameter = $customConf[$key];
        } else {
            if (isset($this->defaultConf[$key])) {

                $parameter = $this->defaultConf[$key];

                // Murmur default port workaround.
                if ('port' === $key) {
                    $parameter += ($this->getSid() -1);
                    $parameter = (string) $parameter;
                }
            }
        }

        return $parameter ?? '';
    }
}
