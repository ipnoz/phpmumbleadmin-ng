<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Murmur\Factory;

use App\Domain\Murmur\Exception\Ice as IceException;
use App\Domain\Murmur\Exception\Server as ServerException;

/**
 * Return the App exception based on the Ice or Murmur exception submitted.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ExceptionFactory
{
    public static function get(\Exception $e): \Exception
    {
        switch (\get_class($e)) {
            case \Ice_MemoryLimitException::class:
            case \Ice\MemoryLimitException::class:
                return new IceException\MemoryLimitException();
            case \Murmur_ServerBootedException::class:
            case \Murmur\ServerBootedException::class:
                return new ServerException\ServerBootedException();
            case \Murmur_ServerFailureException::class:
            case \Murmur\ServerFailureException::class:
                return new ServerException\ServerFailureException();
            case \Murmur_InvalidServerException::class:
            case \Murmur\InvalidServerException::class:
                return new ServerException\InvalidServerException();
            case \Murmur_InvalidSecretException::class:
            case \Murmur\InvalidSecretException::class:
                return new ServerException\InvalidSecretException();
            case \Murmur_InvalidChannelException::class:
            case \Murmur\InvalidChannelException::class:
                return new ServerException\InvalidChannelException();
            case \Murmur_InvalidUserException::class:
            case \Murmur\InvalidUserException::class:
                return new ServerException\InvalidUserException();
            case \Murmur_InvalidSessionException::class:
            case \Murmur\InvalidSessionException::class:
                return new ServerException\InvalidSessionException();
            case \Murmur_NestingLimitException::class:
            case \Murmur\NestingLimitException::class:
                return new ServerException\NestingLimitException();
            case \Murmur_InvalidTextureException::class:
            case \Murmur\InvalidTextureException::class:
                return new ServerException\InvalidTextureException();
            case \Murmur_InvalidCallbackException::class:
            case \Murmur\InvalidCallbackException::class:
                return new ServerException\InvalidCallbackException();
        }

        return $e;
    }
}
