<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

use App\Domain\Helper\IpHelper;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpAddress
{
    private const IPV4 = 'ipv4';
    private const IPV6 = 'ipv6';

    public string $address;
    public int $bitmask;

    private string $type;

    /**
     * @throws \InvalidArgumentException
     */
    public static function fromString(string $address): IpAddress
    {
        $ipAddress = new IpAddress();

        if (false !== \stripos($address, '/')) {
            [$ipAddress->address, $mask] = \explode('/', $address);
        } else {
            $ipAddress->address = $address;
            $mask = '';
        }

        if (IpHelper::isIPv4($ipAddress->address)) {
            $ipAddress->type = self::IPV4;
            $range = \range(1, 32);
        } elseif (IpHelper::isIPv6($ipAddress->address)) {
            $ipAddress->type = self::IPV6;
            $range = \range(1, 128);
        } else {
            throw new \InvalidArgumentException('Invalid ip address');
        }

        if ($mask === '') {
            $mask = \end($range);
        }

        $ipAddress->bitmask = (int) $mask;

        if (! \in_array($ipAddress->bitmask, $range, true)) {
            throw new \InvalidArgumentException('Invalid bitmask');
        }

        return $ipAddress;
    }

    public function isIpv4(): bool
    {
        return $this->type === self::IPV4;
    }

    public function isIpv6(): bool
    {
        return $this->type === self::IPV6;
    }
}
