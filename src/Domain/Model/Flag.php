<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Flag
{
    public string $id;
    public string $name;
    public string $src;
    public bool $selected;

    public function __construct(string $id, string $name, string $src)
    {
        $this->id = $id;
        $this->name = $name;
        $this->src = $src;

        $this->selected = false;
    }
}
