<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Log
{
    public int $position;
    public string $id;
    public int $timestamp;
    public string $text;
    public string $date;
    public string $time;

    public function __construct(int $position, int $timestamp, string $text)
    {
        $this->position = $position;
        $this->timestamp = $timestamp;
        $this->text = $text;

        $this->id = $timestamp.'_'.$position;
        $this->date = '';
        $this->time = '';
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    public function setTime(string $time): void
    {
        $this->time = $time;
    }
}
