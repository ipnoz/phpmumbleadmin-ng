<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Model;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface InfoPanelInterface
{
    public function getText(): string;
    public function getArguments(): array;
}
