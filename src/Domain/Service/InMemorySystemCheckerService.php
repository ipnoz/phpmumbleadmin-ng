<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
*/
class InMemorySystemCheckerService implements  SystemCheckerServiceInterface
{
    private bool $extension_ssl_is_loaded;
    private bool $upload_file_is_allowed;

    public function extension_ssl_is_loaded(): bool
    {
        return $this->extension_ssl_is_loaded;
    }

    public function with_ssl_extension(): self
    {
        $this->extension_ssl_is_loaded = true;
        return $this;
    }

    public function without_ssl_extension(): self
    {
        $this->extension_ssl_is_loaded = false;
        return $this;
    }

    public function upload_file_is_allowed(): bool
    {
        return $this->upload_file_is_allowed;
    }

    public function with_upload_file_allowed(): self
    {
        $this->upload_file_is_allowed = true;
        return $this;
    }

    public function without_upload_file_allowed(): self
    {
        $this->upload_file_is_allowed = false;
        return $this;
    }
}
