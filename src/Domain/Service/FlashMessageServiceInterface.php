<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Service;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface FlashMessageServiceInterface
{
    public const SUCCESS = 'success';
    public const ERROR = 'danger';
    public const WARNING = 'warning';

    public function basedOnHttpCode(int $code, $message, array $transParameters = []): void;
    public function success($message, array $transParameters = []): void;
    public function error($message, array $transParameters = []): void;
    public function warning($message, array $transParameters = []): void;
}
