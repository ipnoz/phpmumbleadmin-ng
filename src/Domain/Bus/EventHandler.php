<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface EventHandler extends HandlerInterface
{
    /*
     * Impossible to implement this method
     * See:
     * https://stackoverflow.com/questions/60200054/passing-an-interface-to-another-interface-method
     * https://www.php.net/manual/en/language.oop5.variance.php
     */
//    public function handle(Event $event): void;
}
