<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Domain\Bus;

use App\Domain\Service\BusMiddlewareInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EventDispatcherBusMiddleware implements BusMiddlewareInterface
{
    private EventBus $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function execute($action, callable $next): BusResponse
    {
        $response = $next($action);

        foreach ($response->getEvents() as $event) {
            $this->eventBus->dispatch($event);
        }

        return $response;
    }
}
