<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Validator\Constraints;

use App\Entity\AdminEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UniqueEmailValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (! $constraint instanceof UniqueEmail) {
            throw new UnexpectedTypeException($constraint, UniqueEmail::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (! \is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        $user = $this->entityManager->getRepository(AdminEntity::class)->findOneBy(['email' => $value]);

        if ($user instanceof AdminEntity) {

            // If the submitted user identifier has an id, and this id is same
            // as the user found, it means that it is a user edition, and the
            // user identifier didn't change
            $object = $this->context->getObject();
            if (\is_object($object) && \property_exists($object, 'id') && $object->id === $user->getId()) {
                return;
            }

            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
