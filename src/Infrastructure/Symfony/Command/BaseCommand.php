<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Service\TranslatorServiceInterface;
use App\Entity\AdminEntity;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class BaseCommand extends Command
{
    protected const VALIDATION_SUCCESS = 0;
    protected const VALIDATION_FAILURE = 1;

    protected TokenStorageInterface $tokenStorage;
    protected ValidatorInterface $validator;
    protected TranslatorServiceInterface $translator;

    /**
     * @required
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage): void
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @required
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * @required
     */
    public function setTranslator(TranslatorServiceInterface $translator): void
    {
        $this->translator = $translator;
    }

    // Authenticate the console process has SuperAdmin
    protected function consoleIsSuperAdmin(): void
    {
        $user = new AdminEntity();
        $user->setRoles([SecurityServiceInterface::ROLE_SUPER_ADMIN]);

        $token = new UsernamePasswordToken($user, 'plainPassword', 'main', $user->getRoles());
        $this->tokenStorage->setToken($token);
    }

    // Validate submitted data and output errors on console
    protected function validateData(OutputInterface $output, $data): int
    {
        $violationList = $this->validator->validate($data);

        if ($violationList->count() > 0) {
            /** @var ConstraintViolation $violation */
            foreach ($violationList as $violation) {
                $output->writeln($this->translator->trans($violation->getMessage()));
            }
            return self::VALIDATION_FAILURE;
        }

        return self::VALIDATION_SUCCESS;
    }
}
