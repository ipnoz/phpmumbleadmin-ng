<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Command;

use App\Domain\Action\Administration\Admins\CreateAdmin\CreateAdminHandler;
use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Form\CreateAdminData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class CreateSuperAdminCommand extends BaseCommand
{
    private CreateAdminHandler $createAdminHandler;

    public function __construct(CreateAdminHandler $createAdminHandler)
    {
        parent::__construct();
        $this->createAdminHandler = $createAdminHandler;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        parent::configure();
        $this
            ->setName('app:super-admin:create')
            ->setDescription('Create a Super Admin')
            ->setHelp('This command allows you to create a SuperAdmin...')
            ->addArgument('username', InputArgument::REQUIRED, 'SuperAdmin login')
            ->addArgument('password', InputArgument::REQUIRED, 'SuperAdmin password')
            ->addOption('email', null, InputOption::VALUE_REQUIRED, 'SuperAdmin email')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $createAdmin = new CreateAdminData();
        $createAdmin->login = $input->getArgument('username');
        $createAdmin->password = $input->getArgument('password');
        $createAdmin->email = $input->getOption('email');
        $createAdmin->role = SecurityServiceInterface::ROLE_SUPER_ADMIN;

        if ($this->validateData($output, $createAdmin) !== self::VALIDATION_SUCCESS) {
            return Command::FAILURE;
        }

        $this->consoleIsSuperAdmin();
        $this->createAdminHandler->handle($createAdmin->getCommand());

        $output->writeln('SuperAdmin '.$input->getArgument('username').' created');
        return Command::SUCCESS;
    }
}
