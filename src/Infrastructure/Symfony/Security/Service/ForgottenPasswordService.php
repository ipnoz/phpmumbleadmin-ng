<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Infrastructure\Symfony\Security\Service;

use App\Entity\AdminEntity;
use App\Entity\ForgottenPassword;
use App\Infrastructure\Handler\MailHandler;
use App\Infrastructure\Service\AdminPasswordHasherService;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordService
{
    private const VALID_UNTIL = '+1day';

    private EntityManagerInterface $entityManager;
    private AdminPasswordHasherService $hasherService;
    private MailHandler $mailHandler;

    public function __construct(EntityManagerInterface $em, AdminPasswordHasherService $hasherService, MailHandler $mailHandler)
    {
        $this->entityManager = $em;
        $this->hasherService = $hasherService;
        $this->mailHandler = $mailHandler;
    }

    /*
     * Create or update a ForgottenPassword
     */
    public function handle(string $username): void
    {
        $admin = $this->entityManager->getRepository(AdminEntity::class)->findOneBy(['username' => $username]);

        if (! $admin instanceof AdminEntity || ! \filter_var($admin->getEmail(), FILTER_VALIDATE_EMAIL)) {
            return;
        }

        $forgottenPassword = $this->entityManager->getRepository(ForgottenPassword::class)->findOneBy(['admin' => $admin]);

        if (! $forgottenPassword instanceof ForgottenPassword) {

            $forgottenPassword = new ForgottenPassword();
            $forgottenPassword->setAdmin($admin);
            $forgottenPassword->setCreatedAt(new \DateTimeImmutable());
        }

        $forgottenPassword->setValidUntil(new \DateTimeImmutable(self::VALID_UNTIL));
        $forgottenPassword->generateHash();

        // Regenerate the hash until it is unique (I can't figure out how to test that part, but I know it's works ;) )
        while ($this->hashExists($forgottenPassword->getHash())) {
            $forgottenPassword->generateHash();
        }

        $this->entityManager->persist($forgottenPassword);
        $this->entityManager->flush();

        $this->mailHandler->sendForgottenPassword($forgottenPassword);
    }

    public function remove(ForgottenPassword $forgottenPassword): void
    {
        $this->entityManager->remove($forgottenPassword);
        $this->entityManager->flush();
    }

    public function updatePassword(ForgottenPassword $forgottenPassword, string $password): void
    {
        $admin = $forgottenPassword->getAdmin();

        $encodedPassword = $this->hasherService->hash($admin, $password);
        $admin->setPassword($encodedPassword);

        $this->remove($forgottenPassword);
    }

    public function hashExists(string $hash): bool
    {
        $entity = $this->entityManager->getRepository(ForgottenPassword::class)->findOneBy(['hash' => $hash]);

        return $entity instanceof ForgottenPassword;
    }
}
