<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Signature\Exception\ExpiredSignatureException;
use Symfony\Component\Security\Core\Signature\Exception\InvalidSignatureException;
use Symfony\Component\Security\Core\Signature\SignatureHasher;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\RememberMe\AbstractRememberMeHandler;
use Symfony\Component\Security\Http\RememberMe\RememberMeDetails;

/**
 * Overriding the SignatureRememberMeHandler instead of using the service
 * parameter of remember_me, because the security-bundle doesn't inject
 * remember_me options on the custom service. @see RememberMeFactory::createAuthenticator
 *
 * @author Wouter de Jong <wouter@wouterj.nl>
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SignatureRememberMeHandler extends AbstractRememberMeHandler
{
    public const SERVER_SEPARATOR = '///server-id///';

    private SignatureHasher $signatureHasher;

    public function __construct(SignatureHasher $signatureHasher, UserProviderInterface $userProvider, RequestStack $requestStack, array $options, LoggerInterface $logger = null)
    {
        parent::__construct($userProvider, $requestStack, $options, $logger);

        $this->signatureHasher = $signatureHasher;
    }

    /**
     * {@inheritdoc}
     */
    public function createRememberMeCookie(UserInterface $user): void
    {
        $expires = \time() + $this->options['lifetime'];
        $value = $this->signatureHasher->computeSignatureHash($user, $expires);

        $userIdentifier = $user->getUserIdentifier();

        // For MumbleUserProvider
        if (\method_exists($user, 'getServerId')) {
            $userIdentifier .= self::SERVER_SEPARATOR.$user->getServerId();
        }

        $details = new RememberMeDetails(\get_class($user), $userIdentifier, $expires, $value);
        $this->createCookie($details);
    }

    /**
     * {@inheritdoc}
     */
    public function processRememberMe(RememberMeDetails $rememberMeDetails, UserInterface $user): void
    {
        try {
            $this->signatureHasher->verifySignatureHash($user, $rememberMeDetails->getExpires(), $rememberMeDetails->getValue());
        } catch (InvalidSignatureException $e) {
            throw new AuthenticationException('The cookie\'s hash is invalid.', 0, $e);
        } catch (ExpiredSignatureException $e) {
            throw new AuthenticationException('The cookie has expired.', 0, $e);
        }

        $this->createRememberMeCookie($user);
    }
}
