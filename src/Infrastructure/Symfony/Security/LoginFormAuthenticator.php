<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security;

use App\Entity\AdminEntity;
use App\Infrastructure\Murmur\Service\MumbleUserService;
use App\Infrastructure\Service\AdminPasswordHasherService;
use App\Infrastructure\Symfony\Form\LoginType;
use App\Infrastructure\Symfony\Security\Service\DefaultRedirectionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoginFormAuthenticator extends AbstractAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';
    public const TARGET_PATH_REDIRECTION_KEY = 'x-target-path-redirection';

    private const SERVER_SEPARATOR = '///-SERVER_SEPARATOR-///';

    private EntityManagerInterface $entityManager;
    private UrlGeneratorInterface $urlGenerator;
    private CsrfTokenManagerInterface $csrfTokenManager;
    private AdminPasswordHasherService $passwordHasher;
    private FormFactoryInterface $formFactory;
    private MumbleUserService $mumbleUserService;
    private DefaultRedirectionService $defaultRedirection;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        AdminPasswordHasherService $passwordHasher,
        FormFactoryInterface $formFactory,
        MumbleUserService $mumbleUserService,
        DefaultRedirectionService $defaultRedirection
    ) {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordHasher = $passwordHasher;
        $this->formFactory = $formFactory;
        $this->mumbleUserService = $mumbleUserService;
        $this->defaultRedirection = $defaultRedirection;
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): bool
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route') && $request->isMethod('POST');
    }

    /**
     * @inheritDoc
     */
    public function authenticate(Request $request): Passport
    {
        $form = $this->formFactory->create(LoginType::class);
        $form->handleRequest($request);

        $username = $form->get('username')->getData();
        $password = $form->get('password')->getData();
        $server = $form->get('server')->getData();
        $csrfToken = $request->request->all(LoginType::BLOCK_PREFIX)['_token'] ?? '';

        // Store in session the last username submitted to log in
        $request->getSession()->set(Security::LAST_USERNAME, $username);

        if (\is_numeric($server)) {

            // Mumble users part
            $username .= self::SERVER_SEPARATOR.$server;

            $userLoader = $this->mumbleUsersLoader();
            $passwordChecker = function ($password, MumbleUser $user) {
                return $this->mumbleUserService->passwordIsValid($user, $password);
            };

        } else {

            // Admins part
            $userLoader = $this->adminsLoader();
            $passwordChecker = function ($password, AdminEntity $user) {
                return $this->passwordHasher->isPasswordValid($user, $password);
            };

        }

        return new Passport(
            new UserBadge($username, $userLoader),
            new CustomCredentials($passwordChecker, $password),
            [
                new CsrfTokenBadge(LoginType::BLOCK_PREFIX, $csrfToken),
                new RememberMeBadge(),
            ]
        );
    }

    private function adminsLoader(): \Closure
    {
        return function (string $username): AdminEntity {

            $admin = $this->entityManager->getRepository(AdminEntity::class)->findOneBy(['username' => $username]);

            if (! $admin instanceof AdminEntity) {
                throw new UserNotFoundException();
            }

            return $admin;
        };
    }

    private function mumbleUsersLoader(): \Closure
    {
        return function (string $username): MumbleUser {

            [$username, $server] = \explode(self::SERVER_SEPARATOR, $username);

            $user = $this->mumbleUserService->find((int)$server, $username);

            if (! $user instanceof MumbleUser) {
                throw new UserNotFoundException();
            }

            return $user;
        };
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $firewallName): RedirectResponse
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath.'?'.self::TARGET_PATH_REDIRECTION_KEY);
        }

        return $this->defaultRedirection->getHomepage($token->getUser());
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        return null;
    }
}
