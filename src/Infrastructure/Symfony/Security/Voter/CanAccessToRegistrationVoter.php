<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Security\Voter;

use App\Domain\Service\SecurityServiceInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanAccessToRegistrationVoter extends Voter
{
    public const KEY = 'APP_CAN_ACCESS_TO_REGISTRATION';

    private SecurityServiceInterface $security;

    public function __construct(SecurityServiceInterface $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        return $attribute === self::KEY && is_int($subject) && $subject >= 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        // SuperuserRu can't access to the superuser registration
        if (0 === $subject) {
            return $this->security->isGrantedSuperUser();
        }

        return true;
    }
}
