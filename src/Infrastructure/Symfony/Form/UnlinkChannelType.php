<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Domain\Action\Server\Channels\UnlinkChannel\UnlinkChannelCommand;
use App\Infrastructure\Symfony\Form\Type\VueJs\Model\ChannelIdVueJsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\Model\ActionOnChannelIdVueJsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UnlinkChannelType extends AbstractType
{
    public const BLOCK_PREFIX = 'unlink_channel';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('channelId', ChannelIdVueJsType::class)
            ->add('unlinkId', ActionOnChannelIdVueJsType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UnlinkChannelCommand::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
