<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\EndDateType;
use App\Infrastructure\Symfony\Form\Type\ModalJqueryAjaxFormType;
use App\Infrastructure\Symfony\Form\Type\VueJs\HiddenInputForVuejsType;
use App\Infrastructure\Symfony\Form\Type\VueJs\Model\UserSessionVueJsType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BanUserType extends ModalJqueryAjaxFormType
{
    public const BLOCK_PREFIX = 'ban_user';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('userSession', UserSessionVueJsType::class)
            ->add('ip', HiddenInputForVuejsType::class, [
                'variable_name' => 'selectedNode.ip',
                'constraints' => [
                    new NotBlank(['message' => 'Process error: Empty user ip. Please retry']),
                ],
            ])
            ->add('name', HiddenInputForVuejsType::class, [
                'variable_name' => 'selectedNode.name',
                'constraints' => [
                    new NotBlank(['message' => 'Process error: Empty user name. Please retry']),
                ],
            ])
            ->add('hash', HiddenInputForVuejsType::class, [
                'variable_name' => 'selectedNode.hash',
            ])
            ->add('reason', null, [
                'label' => 'reason',
                'required' => false,
                'translation_domain' => 'Form',
            ])
            ->add('endDate', EndDateType::class, [
                'label' => 'end_date',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'ban',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'form-ban',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ModalJqueryAjaxFormType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
