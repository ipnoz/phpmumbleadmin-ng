<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type;

use App\Domain\Service\SecurityServiceInterface;
use App\Entity\AdminEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminRoleChoiceType extends AbstractType
{
    private SecurityServiceInterface $securityService;

    public function __construct(SecurityServiceInterface $securityService)
    {
        $this->securityService = $securityService;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choice_translation_domain' => false,
            'choices' => $this->roleChoices(),
        ]);
    }

    private function roleChoices(): array
    {
        $roles = AdminEntity::AVAILABLE_ROLES;

        $choices = [];

        foreach ($roles as $roleName => $role) {
            if ($this->securityService->canEditRole($role)) {
                $choices[$roleName] = $role;
            }
        }

        return $choices;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
