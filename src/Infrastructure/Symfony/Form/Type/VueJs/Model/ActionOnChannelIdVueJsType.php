<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Symfony\Form\Type\VueJs\Model;

use App\Infrastructure\Symfony\Form\Type\VueJs\HiddenIntegerForVuejsType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActionOnChannelIdVueJsType extends HiddenIntegerForVuejsType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'variable_name' => 'action.nodeId',
            'constraints' => [
                new NotBlank(['message' => 'Process error: Empty action nodeId. Please retry']),
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return HiddenIntegerForVuejsType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_action_on_channel_id_vuejs';
    }
}
