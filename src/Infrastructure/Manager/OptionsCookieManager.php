<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Manager;

use App\Domain\Model\OptionsCookie;
use App\Infrastructure\Service\RequestService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Manage the options cookie of the user
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class OptionsCookieManager
{
    private ParameterBagInterface $parameterBag;
    private ?Request $request;

    private OptionsCookie $optionsCookie;
    private ?string $cookieName;

    public function __construct(RequestService $requestService, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->request = $requestService->getMainRequest();

        $this->optionsCookie = new OptionsCookie();
        $this->initialize();
    }

    private function initialize(): void
    {
        if (! $this->request instanceof Request) {
            return;
        }

        $this->cookieName = $this->parameterBag->get('app.options_cookie_name');

        if (! $this->request->cookies->has($this->cookieName)) {
            return;
        }

        $data = $this->request->cookies->get($this->cookieName);
        $this->optionsCookie->unserialize($data);
    }

    public function getCookie(): OptionsCookie
    {
        return $this->optionsCookie;
    }

    public function updateCookie(): void
    {
        // expires at 2038-01-19 04:14:07
        \setcookie($this->cookieName, $this->optionsCookie->serialize(), 2147483647, '/');
    }
}
