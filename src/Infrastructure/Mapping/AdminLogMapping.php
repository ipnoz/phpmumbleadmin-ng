<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Mapping;

use App\Domain\Model\AdminLog;
use App\Entity\AdminLogEntity;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogMapping
{
    public function fromEntity(AdminLogEntity $entity): AdminLog
    {
        $adminLog = new AdminLog();

        $adminLog->setAddress($entity->getAddress());
        $adminLog->setContext($entity->getContext());
        $adminLog->setFacility($entity->getFacility());
        $adminLog->setLevel($entity->getLevel());
        $adminLog->setMessage($entity->getMessage());
        $adminLog->setTimestamp($entity->getTimestamp());

        return $adminLog;
    }

    public function fromEntityToArray(AdminLogEntity $entity): array
    {
        $adminLog = [];

        $adminLog['address'] = $entity->getAddress();
        $adminLog['context'] = $entity->getContext();
        $adminLog['facility'] = $entity->getFacility();
        $adminLog['level'] = $entity->getLevel();
        $adminLog['message'] = $entity->getMessage();
        $adminLog['timestamp'] = $entity->getTimestamp();

        return $adminLog;
    }

    /**
     * @param AdminLogEntity[] $entities
     * @return AdminLog[]
     */
    public function fromEntities(array $entities): array
    {
        $adminLogs = [];

        foreach ($entities as $entity) {
            $adminLogs[] = $this->fromEntity($entity);
        }

        return $adminLogs;
    }

    /**
     * @param AdminLogEntity[] $entities
     */
    public function fromEntitiesToArray(array $entities): array
    {
        $adminLogs = [];

        foreach ($entities as $entity) {
            $adminLogs[] = $this->fromEntityToArray($entity);
        }

        return $adminLogs;
    }

    public function toEntity(AdminLog $adminLog): AdminLogEntity
    {
        $entity = new AdminLogEntity();

        $entity->setAddress($adminLog->getAddress());
        $entity->setContext($adminLog->getContext());
        $entity->setFacility($adminLog->getFacility());
        $entity->setLevel($adminLog->getLevel());
        $entity->setMessage($adminLog->getMessage());
        $entity->setTimestamp($adminLog->getTimestamp());

        return $entity;
    }
}
