<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Service;

use App\Domain\Murmur\Connection\ConnectionHandlerInterface;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Domain\Murmur\Model\ServerInterface;
use App\Infrastructure\Symfony\Security\MumbleUser;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class MumbleUserService
{
    private ConnectionHandlerInterface $connection;
    private ?MetaInterface $meta;

    public function __construct(ConnectionHandlerInterface $connection)
    {
        $this->connection = $connection;
    }

    private function connection(): void
    {
        if (isset($this->meta)) {
            return;
        }

        try {
            $this->meta = $this->connection->connect();
        } catch (ConnectionException $e) {
            $this->meta = null;
        }
    }

    private function getServer(int $id): ?ServerInterface
    {
        $this->connection();

        if (! $this->meta instanceof MetaInterface) {
            return null;
        }

        return $this->meta->getServer($id);
    }

    public function find(int $serverId, string $username): ?MumbleUser
    {
        $server = $this->getServer($serverId);

        if (! $server instanceof ServerInterface) {
            return null;
        }

        try {
            $users = $server->getRegisteredUsers($username);
        } catch (\Exception $e) {
            return null;
        }

        foreach ($users as $id => $name) {
            if ($username === $name) {
                $mumbleUser = new MumbleUser();
                $mumbleUser->setServerId($serverId);
                $mumbleUser->setRegistrationId($id);
                $mumbleUser->setUsername($name);
                $mumbleUser->setRoles([SecurityServiceInterface::ROLE_MUMBLE_USER]);
                return $mumbleUser;
            }
        }

        return null;
    }

    public function authenticate(int $serverId, string $username, string $password): bool
    {
        $server = $this->getServer($serverId);

        if (! $server instanceof ServerInterface) {
            return false;
        }

        try {
            $result = $server->verifyPassword($username, $password);
            return $result > 0;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function passwordIsValid(MumbleUser $mumbleUser, string $password): bool
    {
        return $this->authenticate($mumbleUser->getServerId(), $mumbleUser->getUsername(), $password);
    }
}
