<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Service;

use App\Domain\Helper\IpHelper;
use App\Domain\Murmur\Service\MurmurConnectionUriServiceInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurConnectionUriService implements MurmurConnectionUriServiceInterface
{
    private ParameterBagInterface $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * Construct the mumble connection URI
     */
    public function constructUri(string $login, ?string $password, string $host, $port, ?string $version = null): string
    {
        if (IpHelper::isIPv6($host)) {
            $host = '['.$host.']';
        }

        $parametredHost = $this->parameterBag->get('murmur.url_connection_host');

        if (\is_string($parametredHost) && '' !== $parametredHost) {
            $host = $parametredHost;
        }

        if (\is_string($password) && '' !== $password) {
            $login .= ':'.$password;
        }

        if (\is_null($version) || '' === $version) {
            $version = '1.2.0';
        }

        return 'mumble://'.$login.'@'.$host.':'.$port.'/?version='.$version;
    }
}
