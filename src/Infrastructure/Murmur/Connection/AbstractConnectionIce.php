<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Connection;

use App\Domain\Murmur\Connection\ConnectionInterface;
use App\Domain\Murmur\Exception\ConnectionException;
use App\Domain\Murmur\Model\MetaInterface;
use App\Infrastructure\Murmur\Model\Meta;

/**
 * Base for Ice connection
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class AbstractConnectionIce implements ConnectionInterface
{
    protected const MURMUR_VERSION_MIN = '1.2.3';

    protected const ERR_EMPTY_HOST_PARAM = 'empty_host_parameter';
    protected const ERR_ICE_PHP_FILE_NOT_LOADED = 'could_not_load_Ice_php_file';
    protected const ERR_SLICE_FILE_NOT_FOUND = 'slice_file_not_found';
    protected const ERR_SLICE_FILE_NOT_LOADED = 'slice_file_not_loaded';
    protected const ERR_SLICE_FILE_INVALID = 'slice_file_invalid';
    protected const ERR_MURMUR_VERSION_NOT_SUPPORTED = 'Murmur_version_not_supported';

    /*
     * Instance of ICE_Connector interface.
     */
    protected $ICE;

    /*
     * Instance of proxy.
     */
    protected $proxy;

    /*
     * Instance of MurmurMeta interface (proxy).
     */
    protected $MurmurMeta;

    /*
     * Instance of Meta.
     */
    protected ?MetaInterface $meta;

    /*
     * Connection parameters: host
     */
    protected string $host;

    /*
     * Connection parameters: port
     */
    protected int $port;

    /*
     * Connection parameters: timeout
     */
    protected int $timeout;

    /*
     * Connection parameters: secret context
     */
    protected array $secret;

    /**
     * @inheritDoc
     */
    public function setParameters(string $host, int $port, int $timeout, string $secret): void
    {
        $this->host = $host;
        $this->port = $port;
        $this->timeout = $timeout;
        $this->secret = ['secret' => $secret];
    }

    /**
     * Connection sequence.
     *
     * @inheritDoc
     */
    public function connection(): ?MetaInterface
    {
        try {
            $this->checkParameters();
            $this->checkSliceDefinitions();
            $this->initializeIce();
            $this->getProxy();
            $this->getMurmurMeta();
            $this->createMeta();
            $this->compareSliceChecksums();
            $this->checkMurmurDefaultConf();
            $this->checkMurmurVersion();
        } catch (\Exception $e) {
            // Catch any exception during the sequence and throw a ConnectionException
            $this->throwException($e);
        }

        return $this->meta;
    }

    /**
     * IceExceptions do not return any reason with getMessage() but use a
     * custom variable. Try to find this variable, and throw a common ConnectionException
     *
     * @throws ConnectionException
     */
    protected function throwException($error): void
    {
        if (\is_object($error)) {
            // Ice_unknown exception
            if (isset($error->unknown)) {
                $message = $error->unknown;
                // Ice_MarshalException
            } elseif(isset($error->reason)) {
                $message = $error->reason;
                // Ice_EndpointParseException give no reason with getMessage method
            } elseif(isset($error->str)) {
                $message = $error->str;
            } else {
                $message = $error->getMessage();
            }
            // MEMO : Ice_exception messages can return an EOL and break logs
            $message = \trim($message);
            $class = \get_class($error);
        } else {
            $class = 'ConnectionException';
            $message = $error;
        }

        throw new ConnectionException($class.': '.$message);
    }

    /**
     * @throws ConnectionException
     */
    protected function checkParameters(): void
    {
        // Empty host parameter throw a "EndpointParseException". Prevent
        // it and set a specific error message.
        if ($this->host === '') {
            throw new ConnectionException(self::ERR_EMPTY_HOST_PARAM);
        }

        if ($this->timeout < 0) {
            $this->timeout = \abs($this->timeout);
        }

        /*
         * Ice timeout in milliseconds.
         *
         * Zeroc ice use an automatic retries function if a timeout expired.
         * By default, it's configured for 1 retry.
         * see: https://doc.zeroc.com/display/Ice36/Automatic+Retries
         *
         * Workaround:
         * Divide the timeout parameter by 2 to respect user's choice.
         */
        $this->timeout = (int) \round($this->timeout / 2);
    }

    /**
     * @throws ConnectionException
     */
    protected function checkSliceDefinitions(): void
    {
        if (! \interface_exists('Ice_Object')) {
            throw new ConnectionException(self::ERR_ICE_PHP_FILE_NOT_LOADED);
        }
        if (! \interface_exists('Murmur_Meta')) {
            throw new ConnectionException(self::ERR_SLICE_FILE_NOT_LOADED);
        }
    }

    protected function initializeIce(): void
    {
        $this->ICE = \Ice_initialize();
    }

    /**
     * Memo for Meta -e 1.0, see:
     * @see https://doc.zeroc.com/technical-articles/general-topics/encoding-version-1-1
     * Memo:
     * Surround host (-h) parameter with quotes ("") to avoid ipv6 random errors
     */
    protected function getProxy(): void
    {
        $this->proxy = $this->ICE->stringToProxy(
            'Meta -e 1.0 :tcp -h "'.$this->host.'" -p '.$this->port.' -t '.$this->timeout
        );
    }

    protected function getMurmurMeta(): void
    {
        $this->MurmurMeta = \Murmur_MetaPrxHelper::checkedCast($this->proxy)->ice_context($this->secret);
    }

    protected function createMeta(): void
    {
        $this->meta = new Meta($this->MurmurMeta, $this->secret['secret']);
    }

    protected function getClientChecksums(): array
    {
        return \Ice_sliceChecksums();
    }

    /**
     * @throws ConnectionException
     */
    protected function compareSliceChecksums(): void
    {
        // Checksums from the loaded Murmur.ice file
        $clientChecksums = $this->getClientChecksums();
        // Checksums from the Murmur daemon
        $daemonCheckSums = $this->meta->getSliceChecksums();

        foreach($clientChecksums as $key => $value) {
            // Check only Murmur sums
            if ('::Murmur::' !== \substr($key, 0, 10)) {
                continue;
            }
            if (! isset($daemonCheckSums[$key]) || $value !== $daemonCheckSums[$key]) {
                throw new ConnectionException(self::ERR_SLICE_FILE_INVALID);
            }
        }
    }

    /**
     * Check for at least a good read secret with DefaultConf
     */
    protected function checkMurmurDefaultConf(): void
    {
        $this->meta->getDefaultConf();
    }

    /**
     * Check for Murmur version support.
     *
     * @throws ConnectionException
     */
    protected function checkMurmurVersion(): void
    {
        $version = $this->meta->getMurmurVersion();

        if (\version_compare($version['str'], self::MURMUR_VERSION_MIN, '<')) {
            throw new ConnectionException(self::ERR_MURMUR_VERSION_NOT_SUPPORTED);
        }
    }
}
