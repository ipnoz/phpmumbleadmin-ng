<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Connection;

use App\Domain\Murmur\Exception\ConnectionException;

/**
 * Ice connection class for zeroc Ice 3.7.
 *
 * Zeroc Ice 3.7 use namespace now...
 * Now all proxies are instances of Ice\ObjectPrx class.
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionIce37 extends ConnectionIce36
{
    /**
     * Ice_ObjectPrx interface become \Ice\ObjectPrx class, and it is loaded with
     * the PHP module during the HTTP server startup.
     * Murmur_Meta interface become \Murmur\MetaPrxHelper class.
     *
     * @throws ConnectionException
     */
    protected function checkSliceDefinitions(): void
    {
        if (! \interface_exists('Ice\ObjectFactory')) {
            throw new ConnectionException(self::ERR_ICE_PHP_FILE_NOT_LOADED);
        }
        if (! \class_exists('\Murmur\MetaPrxHelper')) {
            throw new ConnectionException(self::ERR_SLICE_FILE_NOT_LOADED);
        }
    }

    protected function initializeIce(): void
    {
        $this->ICE = \Ice\initialize();
    }

    protected function getMurmurMeta(): void
    {
        $this->MurmurMeta = \Murmur\MetaPrxHelper::checkedCast($this->proxy)->ice_context($this->secret);
    }

    protected function getClientChecksums(): array
    {
        return \Ice\sliceChecksums();
    }
}
