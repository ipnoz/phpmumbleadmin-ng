<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Murmur\Helper;

use App\Domain\Murmur\Connection\ConnectionInterface;
use App\Domain\Murmur\Connection\ConnectionNull;
use App\Infrastructure\Murmur\Connection;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IceHelper
{
    private const PATH = __DIR__.'/../Slices/ice-%s/Murmur-%s.php';

    /*
     * Get Ice version
     */
    public static function getIceVersion(): string
    {
        $iceVersion = '';

        if (\function_exists('Ice_stringversion')) {
            $iceVersion = \Ice_stringversion();
        } elseif (\function_exists('\Ice\stringversion')) {
            // Since php-ice 3.7
            $iceVersion = \Ice\stringversion();
        }

        return $iceVersion;
    }

    /**
     * Construct the full path of the Murmur slice file based on the server environment.
     *
     * @return null|string - null if any file is found
     */
    public static function getSlicePath(string $MurmurVersion): ?string
    {
        $iceVersion = self::getIceVersion();
        $sliceFile = \sprintf(self::PATH, $iceVersion, $MurmurVersion);

        if (\is_file($sliceFile)) {
            return $sliceFile;
        }

        $exploded = \explode('.', $iceVersion);

        $major = $exploded[0] ?? 0;
        $minor = $exploded[1] ?? 0;
        $fallbackIceVersion = $major.'.'.$minor;

        $sliceFile = \sprintf(self::PATH, $fallbackIceVersion, $MurmurVersion);

        if (\is_file($sliceFile)) {
            return $sliceFile;
        }

        return null;
    }

    /*
     * Get ConnectionInterface
     */
    public static function connectionFactory(): ConnectionInterface
    {
        $iceVersion = self::getIceVersion();

        if (! \extension_loaded('ice')) {
            $interface = new ConnectionNull();
            $interface->setError('ice_module_not_installed');
        } elseif (\version_compare($iceVersion, '3.7', '>=')) {
            $interface = new Connection\ConnectionIce37();
        } elseif (\version_compare($iceVersion, '3.6', '>=')) {
            $interface = new Connection\ConnectionIce36();
        } elseif (\version_compare($iceVersion, '3.5', '>=')) {
            $interface = new Connection\ConnectionIce35();
        } else {
            $interface = new ConnectionNull();
            $interface->setError('ice_module_version_not_supported');
        }

        return $interface;
    }
}
