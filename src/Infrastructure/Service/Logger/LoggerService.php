<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service\Logger;

use App\Domain\Model\AdminLog;
use App\Domain\Service\ClockInterface;
use App\Domain\Service\LoggerServiceInterface;
use App\Domain\Service\TranslatorServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoggerService extends AbstractLoggerService
{
    private LoggerInterface $logger;
    private TranslatorServiceInterface $translator;
    private ParameterBagInterface $parameterBag;

    public function __construct(
        LoggerInterface $logger,
        TranslatorServiceInterface $translator,
        ParameterBagInterface $parameterBag,
        ClockInterface $clock
    ) {
        $this->logger = $logger;
        $this->translator = $translator;
        $this->parameterBag = $parameterBag;
        parent::__construct($clock);
    }

    public function emergency(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_EMERGENCY, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->emergency($message);
    }

    public function alert(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_ALERT, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->alert($message);
    }

    public function critical(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_CRITICAL, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->critical($message);
    }

    public function error(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_ERROR, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->error($message);
    }

    public function warning(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_WARNING, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->warning($message);
    }

    public function notice(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_NOTICE, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->notice($message);
    }

    public function info(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_INFORMATIONAL, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->info($message);
    }

    public function debug(int $facility, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, LoggerServiceInterface::LEVEL_DEBUG, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->debug($message);
    }

    public function log(int $facility, int $level, string $message, array $context = []): void
    {
        $log = $this->createAdminLog($facility, $level, $message, $context);
        $message = $this->translateMessage($log);
        $this->logger->log($log->getLevel(), $message);
    }

    private function translateMessage(AdminLog $log): string
    {
        $locale = $this->parameterBag->get('app.logger_locale');

        $message = \trim($log->getMessage());
        return $this->translator->trans($message, $log->getContext(), 'Log', $locale);
    }
}
