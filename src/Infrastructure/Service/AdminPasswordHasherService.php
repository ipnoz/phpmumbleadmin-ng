<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Entity\AdminEntity;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminPasswordHasherService
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function hash(AdminEntity $admin, string $plainPassword): string
    {
        return $this->hasher->hashPassword($admin, $plainPassword);
    }

    public function isPasswordValid(AdminEntity $admin, string $plainPassword): bool
    {
        return $this->hasher->isPasswordValid($admin, $plainPassword);
    }
}
