<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Infrastructure\Service\Role;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Security\UserInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RoleBitService
{
    public const SUPER_ADMIN      = 1;
    public const ROOT_ADMIN       = 2;
    public const ADMIN            = 4;
    public const SUPER_USER       = 8;
    public const SUPER_USER_RU    = 16;
    public const MUMBLE_USER      = 32;
    public const ANONYMOUS        = 512;

    private RoleCheckerService $roleChecker;

    public function __construct(RoleCheckerService $roleChecker)
    {
        $this->roleChecker = $roleChecker;
    }

    public function findUserBit(UserInterface $user): int
    {
        if ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_ADMIN)) {
            $bit = self::SUPER_ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_ROOT_ADMIN)) {
            $bit =  self::ROOT_ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_ADMIN)) {
            $bit = self::ADMIN;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_USER)) {
            $bit = self::SUPER_USER;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_SUPER_USER_RU)) {
            $bit = self::SUPER_USER_RU;
        } elseif ($this->roleChecker->isGranted($user, SecurityServiceInterface::ROLE_MUMBLE_USER)) {
            $bit = self::MUMBLE_USER;
        } else {
            $bit = self::ANONYMOUS;
        }

        return $bit;
    }

    public function getFromString(string $role): int
    {
        // Remove the 'ROLE_' from the string
        $role = \substr($role, 5);
        return (defined('self::'. $role)) ? \constant('self::'. $role) : self::ANONYMOUS;
    }
}
