<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Dev\Controller;

use App\Controller\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PreviewEmailController extends BaseController
{
    /**
     * @Route("/preview_email", name="_preview_email")
     */
    public function previewAction(): Response
    {
        return $this->render('Mail/forgotten_password.html.twig', [
            'hash' => 'dummy_hash'
        ]);
    }
}
