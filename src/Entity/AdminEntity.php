<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Entity;

use App\Domain\Service\Role\RoleNameServiceInterface;
use App\Domain\Service\SecurityServiceInterface;
use App\Domain\Model\AdminInterface;
use App\Infrastructure\Symfony\Security\AbstractUser;
use App\Repository\AdminRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity(repositoryClass=AdminRepository::class)
 * @ORM\Table(name="admin")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminEntity extends AbstractUser implements AdminInterface, PasswordAuthenticatedUserInterface
{
    public const AVAILABLE_ROLES = [
        RoleNameServiceInterface::SUPER_ADMIN => SecurityServiceInterface::ROLE_SUPER_ADMIN,
        RoleNameServiceInterface::ROOT_ADMIN => SecurityServiceInterface::ROLE_ROOT_ADMIN,
        RoleNameServiceInterface::ADMIN => SecurityServiceInterface::ROLE_ADMIN
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(length=180, unique=true)
     */
    protected string $username;

    /**
     * @ORM\Column(unique=true, nullable=true)
     */
    protected ?string $email;

    /**
     * @ORM\Column(type="json")
     */
    protected array $roles;

    /**
     * @ORM\Column()
     */
    private string $password;


    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function getSalt(): void
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getAvailableRoles(): array
    {
        return self::AVAILABLE_ROLES;
    }
}
