<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

use App\Kernel;
use App\Tests\TestSetupHelper;

// This check prevents access to debug front controllers that are deployed by accident to production servers.
if (
    isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || ! (
        in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'], true)
        || substr(@$_SERVER['REMOTE_ADDR'], 0, '7')=='192.168'
        || substr(@$_SERVER['REMOTE_ADDR'], 0, '7')=='172.18.'
        || substr(@$_SERVER['REMOTE_ADDR'], 0, '3')=='fc0'
        || PHP_SAPI === 'cli-server'
    )
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.\basename(__FILE__).' for more information.');
}

$_SERVER['APP_RUNTIME_OPTIONS'] = [
    'env' => 'test',
];

require_once \dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {

    TestSetupHelper::setTimezoneForTests();
    require_once \dirname(__DIR__).'/config/app/bootstrap_custom.php';

    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
