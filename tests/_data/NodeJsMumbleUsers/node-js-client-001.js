
'use strict';

require('dotenv').config({path: '.env.test.local'});
const path = require('path');
const scriptName = path.basename(__filename);

// NodeJs lookup for docker hostname only (without domain name). Remove any domain name
const murmur_host = process.env.MURMUR_HOST.split('.')[0];
const murmur_port = parseInt(process.env.APP_TEST_VSERVER_PORT);

/**
 * Node.js Mumble client
 *
 * Doc: https://github.com/Johni0702/mumble-client
 * Doc: https://github.com/Johni0702/mumble-client-tcp
 */
// const MumbleClient = require('mumble-client');
const client001 = require('mumble-client-tcp');

/**
 * The options passed to tls.connect, use for client cert
 */
const tlsOptions = {
    rejectUnauthorized: false,
    minVersion: 'TLSv1'
};

client001(murmur_host, murmur_port, {
    tls: tlsOptions,
    username: scriptName,
    password: 'Pass123',
}, function (err, client) {

    if (err) {
        throw err;
    }

    // No error, connection established
    console.log('Connected');

    // Move to Root channel
    client.self.setChannel(client.getChannelById(0));
});