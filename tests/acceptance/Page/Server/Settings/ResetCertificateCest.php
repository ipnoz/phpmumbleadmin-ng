<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Settings;

use App\Tests\AcceptanceTester;
use App\Tests\Page\CertificatePage;
use App\Tests\Page\ServerSettingsPage as Page;

/**
 * Scenario on the server settings page
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ResetCertificateCest
{
    public function it_reset_the_server_certificate(AcceptanceTester $I): void
    {
        // Given I have a server with a valid certificate, and I'm on the settings page
        $server = $I->getTheTestServer();
        $server->setConf('key', CertificatePage::PRIVATE_KEY.PHP_EOL);
        $server->setConf('certificate', CertificatePage::CERTIFICATE.PHP_EOL);
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION.'#tab-certificate');
        $I->seeCertificateInformation(Page::CONTAINER);

        // When I click the button to reset the certificate
        $I->click('reset_certificate', Page::CONTAINER);

        // Then I see server certificate has been reset
        $I->assertSame('', $server->getConf('key'));
        $I->assertSame('', $server->getConf('certificate'));
        $I->see('no_certificate', Page::CONTAINER);
    }
}
