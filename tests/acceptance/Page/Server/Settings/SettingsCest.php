<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Settings;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerSettingsPage as Page;

/**
 * Scenario on the server settings page
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsCest
{
    public function _before(AcceptanceTester $I): void
    {
        // Stay logged for all steps of the scenario
        $I->amLoggedAsSuperAdmin();
    }

    public function it_query_the_server_settings_page(AcceptanceTester $I): void
    {
        // Given I have a fresh new test server
        $I->resetAllServers();
        $I->haveServerNameOnTheTestServer('test servername for settings page');

        // When I query the server settings page
        $I->amOnPage(Page::LOCATION);

        // Then I see the server settings page correctly
        $I->seeTheServerName('test servername for settings page');
        $I->seeTheConnectionToMumbleUrl();
        $I->seeTheContentOfTheServerSettingTab('server');
        $I->dontSeeTheContentOfTheServerSettingTab('administration');
        $I->dontSeeTheContentOfTheServerSettingTab('welcometext');
        $I->dontSeeTheContentOfTheServerSettingTab('certificate');
    }

    /** @depends it_query_the_server_settings_page */
    public function it_fill_fields_on_the_server_tab(AcceptanceTester $I): void
    {
        $I->fillServerSettingsField('registername');
        $I->fillServerSettingsField('password');
        $I->fillServerSettingsFieldWithNumber('usersperchannel');
        $I->selectServerSettingsOption('rememberchannel', 'deactivate');
        $I->fillServerSettingsFieldWithNumber('defaultchannel');
        $I->fillServerSettingsField('registerpassword');
        $I->fillServerSettingsField('registerhostname');
        $I->fillServerSettingsField('registerurl');
        $I->selectServerSettingsOption('certrequired', 'activate');
        $I->selectServerSettingsOption('suggestpositional', 'activate');
        $I->selectServerSettingsOption('suggestpushtotalk', 'activate');
    }

    /** @depends it_fill_fields_on_the_server_tab */
    public function it_fill_fields_on_the_administration_tab(AcceptanceTester $I): void
    {
        // Given
        $I->selectTabOfServerSettings('administration');

        // Then
        $I->fillServerSettingsField('host');
        $I->fillServerSettingsFieldWithNumber('port');
        $I->fillServerSettingsFieldWithNumber('timeout');
        $I->fillServerSettingsFieldWithNumber('bandwidth');
        $I->fillServerSettingsFieldWithNumber('users');
        $I->fillServerSettingsField('username', '[\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+');
        $I->fillServerSettingsField('channelname', '[\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+');
        $I->fillServerSettingsFieldWithNumber('textmessagelength');
        $I->fillServerSettingsFieldWithNumber('imagemessagelength');
        $I->selectServerSettingsOption('allowhtml', 'deactivate');
        $I->selectServerSettingsOption('bonjour', 'activate');
        $I->fillServerSettingsFieldWithNumber('opusthreshold');
        $I->fillServerSettingsFieldWithNumber('channelnestinglimit');
    }

    /** @depends it_fill_fields_on_the_administration_tab */
    public function it_fill_the_welcome_text_tab(AcceptanceTester $I): void
    {
        // Given
        $I->selectTabOfServerSettings('welcome_text');
        $I->dontSee('test welcometext field', Page::FORM);

        // Then
        $I->fillQuillEditor('test welcometext field');
    }

    /** @depends it_fill_the_welcome_text_tab */
    public function it_attach_the_certificate_file(AcceptanceTester $I): void
    {
        // Given
        $I->selectTabOfServerSettings('certificate');
        $I->dontSee('Valid from : 12 novembre 2018 à 23:37:11', Page::FORM);

        // Then
        $I->attachCertificateFileOnServerSettings(true);
    }

    /** @depends it_attach_the_certificate_file */
    public function it_submit_the_form(AcceptanceTester $I): void
    {
        // When I submit the server settings form
        $I->click('Submit', Page::FORM);

        // Then I see flash messages for host, port, and certificate has been modified
        $I->seeSuccessFlashMessage('host_modified_success_restart_needed');
        $I->seeSuccessFlashMessage('port_modified_success_restart_needed');
        $I->seeSuccessFlashMessage('certificate_modified_success_restart_needed');

        // I also see that the last selected tab (certificate) is shown, and the certificate has changed
        $I->seeTheContentOfTheServerSettingTab('certificate');
        $I->see('Valid from : 12 novembre 2018 à 23:37:11', Page::FORM);
    }

    /** @depends it_submit_the_form */
    public function it_select_the_server_tab(AcceptanceTester $I): void
    {
        // When I select the server tab
        $I->selectTabOfServerSettings('server');

        // Then I see modified fields
        $I->see('test registername field', Page::OUTPUT_ELEMENT.'.field-registername');
        $I->see('test password field', Page::OUTPUT_ELEMENT.'.field-password');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-usersperchannel');
        $I->see('disabled', Page::OUTPUT_ELEMENT.'.field-rememberchannel');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-defaultchannel');
        $I->see('test registerpassword field', Page::OUTPUT_ELEMENT.'.field-registerpassword');
        $I->see('test registerhostname field', Page::OUTPUT_ELEMENT.'.field-registerhostname');
        $I->see('test registerurl field', Page::OUTPUT_ELEMENT.'.field-registerurl');
        $I->see('enabled', Page::OUTPUT_ELEMENT.'.field-certrequired');
        $I->see('enabled', Page::OUTPUT_ELEMENT.'.field-suggestpositional');
        $I->see('enabled', Page::OUTPUT_ELEMENT.'.field-suggestpushtotalk');
    }

    /** @depends it_select_the_server_tab */
    public function it_select_the_administration_tab(AcceptanceTester $I): void
    {
        // When
        $I->selectTabOfServerSettings('administration');

        // Then
        $I->see('test host field', Page::OUTPUT_ELEMENT.'.field-host');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-timeout');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-timeout');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-bandwidth');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-users');
        $I->see('[\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+', Page::OUTPUT_ELEMENT.'.field-username');
        $I->see('[\.\<\>\/ \-=\w\#\[\]\{\}\(\)\@\|]+', Page::OUTPUT_ELEMENT.'.field-channelname');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-textmessagelength');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-imagemessagelength');
        $I->see('disabled', Page::OUTPUT_ELEMENT.'.field-allowhtml');
        $I->see('enabled', Page::OUTPUT_ELEMENT.'.field-bonjour');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-opusthreshold');
        $I->see(42, Page::OUTPUT_ELEMENT.'.field-channelnestinglimit');
    }

    /** @depends it_select_the_administration_tab */
    public function it_select_the_welcome_text_tab(AcceptanceTester $I): void
    {
        // When
        $I->selectTabOfServerSettings('welcome_text');

        // Then
        $I->see('test welcometext field', Page::FORM);
    }

    public function it_submit_invalid_data_on_server_settings(AcceptanceTester $I): void
    {
        $I->resetAllServers();

        // Given I'm on Server settings page, and I fill registername, port and certificate with invalid data
        $I->amOnPage(Page::LOCATION);
        $I->fillServerSettingsField('registername', 'invalid character §');
        $I->selectTabOfServerSettings('administration');
        $I->fillServerSettingsFieldWithNumber('port', 9999999);
        $I->selectTabOfServerSettings('certificate');
        $I->attachCertificateFileOnServerSettings(false);
        $I->dontSeeTooltipFormErrorOn('certificat');
        $I->dontSeeTooltipFormErrorOn('administration');
        $I->dontSeeTooltipFormErrorOn('server');

        // When I submit the settings form
        \usleep(100_000); // Without this sleep, the scrollTo method will fail most of the time
        $I->scrollTo(Page::FORM.' button[type="submit"]');
        $I->click('Submit', Page::FORM);

        // Then I see the form errors for all parameters
        $I->seeTheContentOfTheServerSettingTab('certificate');
        $I->seeTooltipFormErrorOn('certificat');
        $I->seeTooltipFormErrorOn('administration');
        $I->seeTooltipFormErrorOn('server');
        $I->seeFormErrorOn('validator_certificate_error_message', Page::FORM);
        $I->dontSeeFormErrorOn('validator_port_error_message', Page::FORM);
        $I->dontSeeFormErrorOn('validator_characters_error_message', Page::FORM);

        // When
        $I->selectTabOfServerSettings('administration');

        // Then
        $I->seeFormErrorOn('validator_port_error_message', Page::FORM);
        $I->dontSeeFormErrorOn('validator_certificate_error_message', Page::FORM);

        // When
        $I->selectTabOfServerSettings('server');

        // Then
        $I->seeFormErrorOn('validator_characters_error_message', Page::FORM);
    }
}
