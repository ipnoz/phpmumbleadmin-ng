<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Logs;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerLogsPage as Page;
use App\Tests\Page\ServerPage;

/**
 * Scenario on the server logs page
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerLogsCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_get_the_logs_page(AcceptanceTester $I): void
    {
        // Given
        $I->resetAllServers();
        $I->haveServerNameOnTheTestServer('test servername for logs page');

        // When I'm on the logs page of the test server
        $I->amOnPage(Page::LOCATION);

        // Then I see the logs page
        $I->seeTheServerName('test servername for logs page');
        $I->seeTheConnectionToMumbleUrl();
        $I->seetNotTranslatedText('logs_total', ['%total%' => 4], ServerPage::INFO_PANEL);
        $I->seeNumberOfDayOnLogsElements(1, Page::CONTAINER);
        $I->seeNumberOfLogElements(4, Page::CONTAINER);
        $I->seeLog(null, 'Not registering server as public', 2, Page::CONTAINER);
        $I->seeLog(null, 'Generating new server certificate', 3, Page::CONTAINER);
        $I->seeLog(null, 'Server listening on', 4, Page::CONTAINER);
        $I->seeLog(null, "Password for 'SuperUser' set to", 5, Page::CONTAINER);
    }

    /** @depends it_get_the_logs_page */
    public function it_wait_for_new_log(AcceptanceTester $I): void
    {
        // Given I do an action on the server which write a log
        $I->stopTheTestServer();

        // When I wait for the new log apparition
        $I->waitForNewLog('Stopped', Page::CONTAINER);

        // Then I see the logs and the infoPanel have been updated with javascript
        $I->seetNotTranslatedText('logs_total', ['%total%' => 5], ServerPage::INFO_PANEL);
        $I->seeDayElementsAreUnique(Page::CONTAINER);
        $I->seeNumberOfLogElements(5, Page::CONTAINER);
        $I->seeNewLog(null, 'Stopped', 2, Page::CONTAINER);
        $I->seeLog(null, 'Not registering server as public', 3, Page::CONTAINER);
        $I->seeLog(null, 'Generating new server certificate', 4, Page::CONTAINER);
        $I->seeLog(null, 'Server listening on', 5, Page::CONTAINER);
        $I->seeLog(null, "Password for 'SuperUser' set to", 6, Page::CONTAINER);
    }
}
