<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Registrations;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerRegistrationPage as Page;
use App\Tests\Page\ServerRegistrationsPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $I->haveServerNameOnTheTestServer('test servername for registration page');
        $I->haveOneMumbleRegistrationOnTheTestServer(['test_username_field', 'test_email_field', 'test_comment_field']);
        $I->haveTextureForRegistrationOnTheTestServer(1);
    }

    public function it_display_the_registration_page_and_click_on_the_back_button(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION.'/1');

        // Then
        $I->seeTheServerName('test servername for registration page');
        $I->see('registrations_total', '#info-panel');
        $I->seeTheServerMenuIsActiveFor('registrations');
        $I->seeTheConnectionToMumbleUrl();
        $I->seeElement(Page::CONTAINER);
        $I->see('id : 1', Page::CONTAINER.' table tr:nth-child(1)');
        $I->see('status : offline', Page::CONTAINER.' table tr:nth-child(2)');
        $I->see('username : test_username_field', Page::CONTAINER.' table tr:nth-child(3)');
        $I->see('email_address : test_email_field', Page::CONTAINER.' table tr:nth-child(4)');
        $I->see('last_activity : ', Page::CONTAINER.' table tr:nth-child(5)');
        $I->see('test_comment_field', Page::CONTAINER.' table tr:nth-child(6)');
        $I->seeElement(Page::CONTAINER.' table tr:nth-child(6) .avatar img');

        // When
        $I->click('a[title="back"]');

        // Then
        $I->seeCurrentUrlMatches('#'.ServerRegistrationsPage::LOCATION.'$#');
    }
}
