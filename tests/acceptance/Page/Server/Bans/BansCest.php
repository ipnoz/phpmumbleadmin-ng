<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Bans;

use App\Domain\Helper\IpHelper;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerBansPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BansCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveServerNameOnTheTestServer('test servername for bans page');
        $I->haveMurmurBansOnTheTestServer([
            new BanMock(IpHelper::stringToDecimalIPv4('127.0.0.1'), 128, 'test username', 'test hash', 'test reason', 900276300, 42),
            new BanMock(IpHelper::stringToDecimalIPv4('192.168.0.10'), 120, '', '', 'test reason2', 962567100, 0),
            new BanMock(IpHelper::stringToDecimalIPv6('::1'), 128, 'test username3', 'test hash3', '', 1531673100, 96)
        ]);
    }

    public function it_display_the_index_page(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeTheServerName('test servername for bans page');
        $I->seeTheConnectionToMumbleUrl();
        $I->seeElement(Page::CONTAINER);
        $I->seeDatatablesPluginIsEnabled(Page::TABLE_ELEMENT);
        $I->seeNumberOfLineInTable(Page::TABLE_ELEMENT, 3);
        // assert line 1
        $I->seeInTable('127.0.0.1', Page::TABLE_ELEMENT, 1, 2);
        $I->dontSeeInTable('127.0.0.1 /', Page::TABLE_ELEMENT, 1, 2);
        $I->seeInTable('test username', Page::TABLE_ELEMENT, 1, 2);
        $I->seeElementInTable('.name', Page::TABLE_ELEMENT, 1, 2);
        $I->seeInTable('test reason', Page::TABLE_ELEMENT, 1, 2);
        $I->seeElementInTable('.reason', Page::TABLE_ELEMENT, 1, 2);
        $I->seeCertificateIconInTable(Page::TABLE_ELEMENT, 1, 3);
        $I->seeInTable('12/07/1998 22:45:00', Page::TABLE_ELEMENT, 1, 4);
        $I->seeInTable('12/07/1998 22:45:42', Page::TABLE_ELEMENT, 1, 5);
        // assert line 2
        $I->seeInTable('192.168.0.10 / 24', Page::TABLE_ELEMENT, 2, 2);
        $I->dontSeeElementInTable('.name', Page::TABLE_ELEMENT, 2, 2);
        $I->seeInTable('test reason2', Page::TABLE_ELEMENT, 2, 2);
        $I->dontSeeCertificateIconInTable(Page::TABLE_ELEMENT, 2, 3);
        $I->seeInTable('2/07/2000 21:45:00', Page::TABLE_ELEMENT, 2, 4);
        $I->seeCellIsEmptyInTable(Page::TABLE_ELEMENT, 2, 5);
        // assert line 3
        $I->seeInTable('::1', Page::TABLE_ELEMENT, 3, 2);
        $I->seeInTable('test username3', Page::TABLE_ELEMENT, 3, 2);
        $I->dontSeeElementInTable('.reason', Page::TABLE_ELEMENT, 3, 2);
        $I->seeCertificateIconInTable(Page::TABLE_ELEMENT, 3, 3);
        $I->seeInTable('15/07/2018 18:45:00', Page::TABLE_ELEMENT, 3, 4);
        $I->seeInTable('15/07/2018 18:46:36', Page::TABLE_ELEMENT, 3, 5);
    }
}
