<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\SendMessageToChannel\MessageSentToChannel;
use App\Infrastructure\Symfony\Form\SendMessageToChannelType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SendMessageToChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_send_a_message_to_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);

        // When
        $I->clickTheActionButtonInTheViewer('#sendMessageToChannelModal');

        // Then
        $I->waitForElementHasFocus('#sendMessageToChannelModal [name="'.SendMessageToChannelType::BLOCK_PREFIX.'[message]"]');

        // When
        $I->fillField('#sendMessageToChannelModal [name="'.SendMessageToChannelType::BLOCK_PREFIX.'[message]"]', 'test message sent with phpMumbleAdmin');
        $I->clickTheSubmitButtonOfTheModalForm('#sendMessageToChannelModal');

        // Then
        $I->waitForElementNotVisible('#sendMessageToChannelModal');
        $I->waitForSuccessJsFlashMessage(MessageSentToChannel::KEY);
    }
}
