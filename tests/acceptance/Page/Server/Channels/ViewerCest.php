<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * Test common action on the viewer
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ViewerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();

        $prx = $I->getTheTestServer();
        $prx->addChannel('channel1', 0);

        $prx2 = $I->addNewMurmurServer();
        $prx2->start();
        $prx2->addChannel('channel1-on-server2', 0);
    }

    public function it_get_the_channels_page(AcceptanceTester $I): void
    {
        // When I am on the channels page
        $I->amOnPage(Page::LOCATION);

        // Then I see the server menu
        $I->seeTheServerMenu();
        $I->seeTheServerMenuIsActiveFor('channels');
    }

    /** @depends it_get_the_channels_page */
    public function it_activate_the_action_mode(AcceptanceTester $I): void
    {
        // Given I see the action mode is disabled, and the channel1 is draggable
        $I->dontSeeTheActionModeIsEnableOnTheViewer();
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) .ui-draggable');

        // When I open the action menu to link the channel
        $I->openTheActionMenuInTheViewer();
        $I->click(Page::VIEWER_MENU.' a[data-bs-target="#linkChannel"]');

        // Then I see the action mode is enabled and visible,
        $I->waitForElement(Page::VIEWER_CHANNELS.'.actionMode');
        $I->seeTheActionModeIsEnableOnTheViewer();
        // the terminate action button is visible,
        $I->seeElement(Page::VIEWER_MENU.' button#terminate-action');
        // and channel1 is no more clickable
        $I->dontSeeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) .ui-draggable');
    }

    /** @depends it_activate_the_action_mode */
    public function it_deactivate_the_action_mode(AcceptanceTester $I): void
    {
        // When I click the button to terminate action
        $I->click(Page::VIEWER_MENU.' button#terminate-action');

        // Then I see the action mode is disabled,
        $I->waitForElementNotVisible(Page::VIEWER_MENU.' button#terminate-action');
        $I->dontSeeTheActionModeIsEnableOnTheViewer();
        // And channel1 is draggable again
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) .ui-draggable');
    }

    /** @depends it_get_the_channels_page */
    public function it_toggle_the_bootstrap_popover_of_captions(AcceptanceTester $I): void
    {
        // Given I don't see the popover, and the text of "default channel"
        $I->dontSeeElement('.popover');
        $I->dontSee('icon_info_default_channel');

        // When I move the mouse hover the first icon of captions
        $I->moveMouseOver('.captions img:nth-child(1)');

        // Then I see the popover, with the text of "default channel"
        $I->waitForElementVisible('.popover');
        $I->see('icon_info_default_channel', '.popover');

        // When I move out the mouse
        $I->moveMouseOver('body');

        // Then I see the popover as disappear, the text too
        $I->waitForElementNotVisible('.popover');
        $I->dontSee('icon_info_default_channel');
    }

    /** @depends it_get_the_channels_page */
    public function it_keep_track_of_selected_node(AcceptanceTester $I): void
    {
        // Given I see the selected channel is Root
        $I->seeTheChannelInTheViewerIsSelected('root');

        // When I select channel1, and refresh the page
        $I->selectChannelInTheViewer('channel1');
        $I->seeTheChannelInTheViewerIsSelected('channel1');
        $I->amOnPage(Page::LOCATION);

        // Then I see channel1 is still selected
        $I->seeTheChannelInTheViewerIsSelected('channel1');

        // When
        $I->amOnPage(Page::PATH.'/2');

        // Then
        $I->seeTheChannelInTheViewerIsSelected('root');
    }

    /** @depends it_keep_track_of_selected_node */
    public function it_dont_keep_track_of_selected_node_when_the_server_has_changed(AcceptanceTester $I): void
    {
        // When I change the server
        $I->amOnPage(Page::PATH.'/2');

        // Then I see the channel id 2 exist,
        $I->seeElement(Page::VIEWER_CHANNELS.' div.channel:nth-child(2)');
        // and I see the selected channel is root
        $I->seeTheChannelInTheViewerIsSelected('root');
    }
}
