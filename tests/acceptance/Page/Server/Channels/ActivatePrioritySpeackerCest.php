<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ActivatePrioritySpeackerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_activate_priority_speaker_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->haveOneMumbleClient();
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->dontSeeElement(Page::VIEWER_CHANNELS.' .user img[alt="status_user_with_priority_speaker"]');

        // When
        $I->selectMumbleClientInTheViewer();
        $I->openTheActionMenuInTheViewer();
        $I->click(Page::VIEWER_MENU.' a[data-bs-target="#activatePrioritySpeaker"]');

        // Then
        $I->waitForElement(Page::VIEWER_CHANNELS.' .user img[alt="status_user_with_priority_speaker"]');
    }
}
