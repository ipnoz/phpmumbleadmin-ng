<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LinkChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();
        $prx->addChannel('channel_to_link', 0);
    }

    public function it_link_a_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeTheChannelInTheViewer('channel_to_link');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(1) img[alt="icon_root_channel"]');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) img[alt="icon_channel"]');

        // When
        $I->openTheActionMenuInTheViewer();
        $I->click(Page::VIEWER_MENU.' a[data-bs-target="#linkChannel"]');
        $I->click(Page::VIEWER_CHANNELS.' div:nth-child(2) .name');

        // Then
        $I->waitForElement(Page::VIEWER_CHANNELS.' div:nth-child(1) img[alt="isDirectLink"]');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) img[alt="isDirectLink"]');
    }
}
