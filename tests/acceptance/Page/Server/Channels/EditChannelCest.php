<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Infrastructure\Symfony\Form\EditChannelType;
use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->amLoggedAsSuperAdmin();
    }

    public function it_initialize_the_server(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();

        // I have a channel below the root channel
        $prx->addChannel('channel_to_edit', 0);
    }

    /** @depends it_initialize_the_server */
    public function it_get_the_channels_page(AcceptanceTester $I): void
    {
        // When I'm on the page of server channels
        $I->amOnPage(Page::LOCATION);

        // Then I see the created channel, and the selected channel is Root
        $I->seeTheChannelInTheViewer('channel_to_edit');
        $I->seeTheChannelInTheViewerIsSelected('root');
    }

    /** @depends it_get_the_channels_page */
    public function it_edit_the_root_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // When I open the edit channel modal
        $I->clickTheActionButtonInTheViewer('#editChannelModal');

        // Then I see the name field is disabled because it's the root channel,
        // and the default channel option is checked
        $I->seeElement('#editChannelModal input[name="'.EditChannelType::BLOCK_PREFIX.'[name]"][disabled="disabled"]');
        $I->seeCheckboxIsChecked('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[defaultChannel]"]');
    }

    /** @depends it_edit_the_root_channel_with_the_viewer */
    public function it_submit_the_edit_channel_form_for_root_channel(AcceptanceTester $I): void
    {
        // When I fill fields with new data, and I submit the edit channel form
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[position]"]', '424242');
        $I->fillField('#editChannelModal form textarea[name="'.EditChannelType::BLOCK_PREFIX.'[description]"]', 'root description edited');
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[password]"]', 'root password edited');
        $I->clickTheSubmitButtonOfTheModalForm('#editChannelModal');

        // Then I see the submitted data on the viewer
        $I->waitForText('root description edited', 10, Page::VIEWER_PANEL.' .comment');
        $I->see('424242', Page::VIEWER_PANEL.' .infos');
        $I->see('root password edited', Page::VIEWER_PANEL.' .infos');
        $I->waitForSuccessJsFlashMessage('channel_edited');
    }

    /** @depends it_submit_the_edit_channel_form_for_root_channel */
    public function it_select_the_sub_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // When I select the sub channel on the viewer, and open the modal to edit channel
        $I->selectChannelInTheViewer('channel_to_edit');
        $I->clickTheActionButtonInTheViewer('#editChannelModal');

        // Then I see the name field is enabled because it's not the root channel,
        // and the default channel option is not checked
        $I->dontSeeElement('#editChannelModal input[name="'.EditChannelType::BLOCK_PREFIX.'[name]"][disabled="disabled"]');
        $I->dontSeeCheckboxIsChecked('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[defaultChannel]"]');
    }

    /** @depends it_select_the_sub_channel_with_the_viewer */
    public function it_edit_the_sub_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // When I fill new data on the form, and I submit it
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[name]"]', 'channel name edited');
        $I->checkOption('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[defaultChannel]"]');
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[position]"]', '242424');
        $I->fillField('#editChannelModal form textarea[name="'.EditChannelType::BLOCK_PREFIX.'[description]"]', 'channel description edited');
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[password]"]', 'channel password edited');
        $I->clickTheSubmitButtonOfTheModalForm('#editChannelModal');

        // Then I see the submitted data on the viewer
        $I->waitForText('channel name edited', 10, Page::VIEWER_CHANNELS.' div:nth-child(2) #c-1');
        $I->dontSeeTheChannelInTheViewer('channel_to_edit');
        $I->see('channel description edited', Page::VIEWER_PANEL.' .comment');
        $I->see('242424', Page::VIEWER_PANEL.' .infos');
        $I->see('channel password edited', Page::VIEWER_PANEL.' .infos');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) img[alt="status_home_channel"]');
        $I->waitForSuccessJsFlashMessage('channel_edited');
    }

    /** @depends it_edit_the_sub_channel_with_the_viewer */
    public function it_edit_the_channel_with_invalid_characters(AcceptanceTester $I): void
    {
        // When I select the channel, click to edit the channel, and fill with an invalid character
        $I->clickTheActionButtonInTheViewer('#editChannelModal');
        $I->fillField('#editChannelModal form input[name="'.EditChannelType::BLOCK_PREFIX.'[name]"]', 'channel name § with invalid characters §');
        $I->clickTheSubmitButtonOfTheModalForm('#editChannelModal');

        // Then I get the form error
        $I->waitForText(ChannelNameIsInvalid::KEY, 5, '#editChannelModal form .modal-body');
        $I->seeTheChannelInTheViewer('channel name edite');
        $I->dontSeeTheChannelInTheViewer('channel name § with invalid characters §');
    }
}
