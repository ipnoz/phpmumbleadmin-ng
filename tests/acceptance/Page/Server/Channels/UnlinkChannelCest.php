<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Server\Channels;

use App\Tests\AcceptanceTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UnlinkChannelCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
        $prx = $I->getTheTestServer();
        $prx->addChannel('channel_to_unlink', 0);
        $channelState = $prx->getChannelState(1);
        $channelState->links[] = 0;
        $prx->setChannelState($channelState);
    }

    public function it_unlink_a_channel_with_the_viewer(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeTheChannelInTheViewer('channel_to_unlink');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(1) img[alt="isDirectLink"]');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) img[alt="isDirectLink"]');

        // When
        $I->openTheActionMenuInTheViewer();
        $I->click(Page::VIEWER_MENU.' a[data-bs-target="#unlinkChannel"]');
        $I->click(Page::VIEWER_CHANNELS.' div:nth-child(2) .name');

        // Then
        $I->waitForElement(Page::VIEWER_CHANNELS.' div:nth-child(1) img[alt="icon_root_channel"]');
        $I->seeElement(Page::VIEWER_CHANNELS.' div:nth-child(2) img[alt="icon_channel"]');
        // Unlink the last link deactivate the action mode of the viewer
        $I->dontSeeTheActionModeIsEnableOnTheViewer();
    }
}
