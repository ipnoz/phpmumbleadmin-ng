<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Acceptance\Page\Dashboard;

use App\Domain\Action\Dashboard\CreateServer\ServerCreated;
use App\Tests\AcceptanceTester;
use App\Tests\Page\DashboardPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateServerCest
{
    public function _before(AcceptanceTester $I): void
    {
        $I->resetAllServers();
    }

    public function it_create_a_server_on_the_dashboard(AcceptanceTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->amOnPage(Page::LOCATION);
        $I->seeNumberOfElements(Page::TABLE_ID.' tbody tr[data-server]', 1);

        // When (I fire up the modal to create a server)
        $I->click('[data-bs-target="'.Page::MODAL_CREATE_SERVER.'"]');

        // Then
        $I->waitForElementVisible(Page::MODAL_CREATE_SERVER);

        // When
        $I->fillField('registername', 'test registername field');
        $I->fillField('password', 'test password field');
        $I->fillField('users', 42);
        $I->click(Page::MODAL_CREATE_SERVER.' form [type="submit"]');

        // Then
        $I->waitForSuccessFlashMessage(ServerCreated::KEY);
        $I->seeNumberOfElements(Page::TABLE_ID.' tbody tr[data-server]', 2);
        $I->see('test registername field', Page::TABLE_ID.' tbody tr:nth-child(2)');
    }
}
