<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests;

final class TestSetupHelper
{
    // Force this timezone on tests to avoid incompatibility on dates between different configurations
    public static function setTimezoneForTests(): void
    {
        \ini_set('date.timezone', 'Europe/Paris');
    }

    public static function dumpInitialDatabase(): void
    {
        $start = microtime(true);

        $host = $_ENV['APP_DB_HOST'];
        $port = $_ENV['APP_DB_PORT'];
        $user = $_ENV['APP_DB_USER'];
        $password = $_ENV['APP_DB_PASSWORD'];
        $name = $_ENV['APP_DB_NAME'];

        $command = "mysql -h $host -P $port -u $user --password=$password $name < tests/_data/test-db.sql";

        echo PHP_EOL.$command.PHP_EOL;
        \exec($command);

        echo 'dump execution time : '.\round(\microtime(true) - $start, 3).' s'.PHP_EOL;
    }
}
