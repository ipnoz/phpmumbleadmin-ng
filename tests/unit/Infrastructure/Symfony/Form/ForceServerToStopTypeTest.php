<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\ForceServerToStopType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForceServerToStopTypeTest extends BaseTypeTest
{
    /*
     * Return an array of the fields and data based on the form type.
     */
    public static function getFormData(): array
    {
        return [
            'serverId' => 1,
            'kickUsers' => false,
            'reason' => 'The reason to insert in the logs of the App',
            'message' => 'a message send to all user in the server'
        ];
    }

    public function test_form_is_synchronized(): void
    {
        $formData = self::getFormData();

        $form = $this->factory->create(ForceServerToStopType::class);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
    }

    public function test_form_by_submiting_all_fields_with_valid_data(): void
    {
        $formData = self::getFormData();

        $form = $this->factory->create(ForceServerToStopType::class);
        $form->submit($formData);

        $this->assertTrue($form->isValid());
    }

    public function test_form_by_submiting_valid_data_and_only_required_fields(): void
    {
        $formData = self::getFormData();
        unset($formData['kickUsers'], $formData['reason'], $formData['message']);

        $form = $this->factory->create(ForceServerToStopType::class);
        $form->submit($formData);

        $this->assertTrue($form->isValid());
    }
}
