<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\Type\ServerSettings\PemTextareaCertificateType;
use App\Tests\Page\CertificatePage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PemTextareaCertificateTypeTest extends BaseTypeTest
{
    public function test_it_submit_the_form_with_invalid_certificate(): void
    {
        // Given I create a form and add a PemTextareaCertificateType field
        $form = $this->factory->create();
        $form->add('pem', PemTextareaCertificateType::class);

        // When I submit the form with invalid certificate string
        $form->submit(['pem' => 'invalid certificate']);

        // Then I see the form has errors
        $this->assertTrue($form->isSynchronized());
        $this->assertFalse($form->isValid());
        $this->assertSame('invalid certificate', $form->get('pem')->getData());
        $this->assertCount(1, $form->get('pem')->getErrors());
        $this->assertSame('validator_certificate_error_message', $form->get('pem')->getErrors()[0]->getMessage());
    }

    public function test_it_submit_the_form_with_valid_certificate(): void
    {
        // Given I create a form and add a PemTextareaCertificateType field
        $form = $this->factory->create();
        $form->add('pem', PemTextareaCertificateType::class);

        // When I submit the form with valid certificate string
        $form->submit(['pem' => CertificatePage::PEM]);

        // Then I see the form is valid
        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
        $this->assertSame(CertificatePage::PEM, $form->get('pem')->getData());
    }
}
