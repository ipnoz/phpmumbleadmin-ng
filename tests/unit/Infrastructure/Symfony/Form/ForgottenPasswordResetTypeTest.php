<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Symfony\Form;

use App\Infrastructure\Symfony\Form\ForgottenPasswordResetType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordResetTypeTest extends BaseTypeTest
{
    public function test_form_is_synchronized(): void
    {
        $form = $this->factory->create(ForgottenPasswordResetType::class);

        $form->submit([
            'password' => [
                'first' => 'one',
                'second' => 'Two'
            ]
        ]);

        $this->assertTrue($form->isSynchronized());
    }

    public function test_repeated_password(): void
    {
        $form = $this->factory->create(ForgottenPasswordResetType::class);

        $form->submit([
            'password' => [
                'first' => 'new_password',
                'second' => 'new_password'
            ]
        ]);

        $this->assertTrue($form->isValid());
    }

    public function test_form_require_repeated_password(): void
    {
        $form = $this->factory->create(ForgottenPasswordResetType::class);

        $form->submit([
            'password' => [
                'first' => 'new_password',
                'second' => 'not_repeated_password'
            ]
        ]);

        $this->assertFalse($form->isValid());
    }

    public function emptyPasswordProvider(): array
    {
        return [
            [['first' => null, 'second' => null]],
            [['first' => '', 'second' => '']],
        ];
    }

    /** @dataProvider emptyPasswordProvider */
    public function test_form_require_not_empty_password($data): void
    {
        $form = $this->factory->create(ForgottenPasswordResetType::class);

        $form->submit(['password' => $data]);

        $this->assertFalse($form->isValid());
    }
}
