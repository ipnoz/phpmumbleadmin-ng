<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Infrastructure\Murmur\Service;

use App\Infrastructure\Murmur\Service\MurmurConnectionUriService;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MurmurConnectionUriServiceTest extends Unit
{
    public function provideDataForConstructUri(): array
    {
        return [
            [
                '', 'testLogin', 'testPassword', 'testHost', '5042', 'testVersion',
                'mumble://testLogin:testPassword@testHost:5042/?version=testVersion'
            ],
            [
                null, 'testLogin', 'testPassword', 'testHost', '5042', 'testVersion',
                'mumble://testLogin:testPassword@testHost:5042/?version=testVersion'
            ],
            [
                'ParametredHost', 'testLogin', 'testPassword', 'testHost', '5042', 'testVersion',
                'mumble://testLogin:testPassword@ParametredHost:5042/?version=testVersion'
            ],
            [
                '', 'testLogin', null, 'testHost', '5042', null,
                'mumble://testLogin@testHost:5042/?version=1.2.0'
            ],
            [
                '', 'testLogin', '', 'testHost', 5042, null,
                'mumble://testLogin@testHost:5042/?version=1.2.0'
            ],
            [
                '', 'testLogin', 'testPassword', 'testHost', 5042, null,
                'mumble://testLogin:testPassword@testHost:5042/?version=1.2.0'
            ],
            [
                '', 'testLogin', 'testPassword', 'testHost', 5042, '',
                'mumble://testLogin:testPassword@testHost:5042/?version=1.2.0'
            ],
            [
                '', 'testLogin', 'testPassword', '127.0.0.1', 5042, '',
                'mumble://testLogin:testPassword@127.0.0.1:5042/?version=1.2.0'
            ],
            [
                '', 'testLogin', 'testPassword', '::1', 5042, '',
                'mumble://testLogin:testPassword@[::1]:5042/?version=1.2.0'
            ],
        ];
    }

    /**
     * @dataProvider provideDataForConstructUri
     */
    public function test_construct_uri_method($parametredHost, $login, $password, $host, $port, $version, $expectedUri): void
    {
        $parameterBag = new ParameterBag(['murmur.url_connection_host' => $parametredHost]);
        $service = new MurmurConnectionUriService($parameterBag);

        $uri = $service->constructUri($login, $password, $host, $port, $version);

        $this->assertSame($expectedUri, $uri);
    }
}
