<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Murmur\Helper;

use App\Domain\Murmur\Helper\LogsHelper;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogsHelperTest extends Unit
{
    private string $originalTimezone;

    protected function _before(): void
    {
        $this->originalTimezone = \date_default_timezone_get();
    }

    public function timezoneProvider(): array
    {
        return [
            'UTC'           => ['+0000', 0],
            'Europe/Paris'  => ['+0200', 7200],
            'Etc/GMT-6'     => ['+0600', 21600],
            'Etc/GMT+9'     => ['-0900', -32400]
        ];
    }

    /** @dataProvider timezoneProvider */
    public function test_timestamp_diff_workaround_method(string $timezoneDiff, int $expected): void
    {
        $result = LogsHelper::timestampDiffWorkaround($timezoneDiff);

        $this->assertSame($expected, $result);
    }

    public function dateProvider(): array
    {
        return [
            ['2021-01-01T00:00:00.000000Z', '2021-01-01T00:00:00.000000Z', false],
            ['2021-01-01T00:00:00.000000Z', '2021-01-01T00:00:01.000000Z', false],
            ['2021-01-01T23:59:59.999999Z', '2021-01-01T00:00:00.000000Z', false],
            ['2021-01-01T23:59:59.999999Z', '2021-01-02T00:00:00.000000Z', true],
            ['2021-01-03T23:59:59.999999Z', '2021-01-02T00:00:00.000000Z', true],

            ['2021-02-01T00:00:00.000000Z', '2021-01-01T00:00:00.000000Z', true],
            ['2021-01-01T00:00:00.000000Z', '2021-02-01T00:00:00.000000Z', true],

            ['2022-01-01T00:00:00.000000Z', '2021-01-01T00:00:00.000000Z', true],
            ['2021-01-01T00:00:00.000000Z', '2022-01-01T00:00:00.000000Z', true]
        ];
    }

    /** @dataProvider dateProvider */
    public function test_day_has_changed_method(string $lastTimestamp, string $timestamp, bool $expected): void
    {
        // Given
        \date_default_timezone_set('UTC');
        $lastTimestamp = new \DateTimeImmutable($lastTimestamp);
        $timestamp = new \DateTimeImmutable($timestamp);

        // When
        $result = LogsHelper::dayHasChanged($lastTimestamp->getTimestamp(), $timestamp->getTimestamp());

        // Then
        $this->assertSame($expected, $result);
    }

    public function logLenProvider(): array
    {
        return [
            [4, 2, 2],
            [125, 75, 50],
            [-1, 75, -1],
            [125, -1, -1],
        ];
    }

    /** @dataProvider logLenProvider */
    public function test_get_last_log_number_method(int $logLen, int $lastLogLen, int $expected): void
    {
        $result = LogsHelper::getLastLogEntry($logLen, $lastLogLen);

        $this->assertSame($expected, $result);
    }

    public function _after(): void
    {
        \date_default_timezone_set($this->originalTimezone);
    }
}
