<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Model;

use App\Domain\Model\Log;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LogTest extends Unit
{
    public function test_id_are_always_different_with_the_same_timestamp_but_different_position(): void
    {
        $log1 = new Log(0, 1531680600, 'text');
        $log2 = new Log(1, 1531680600, 'text');
        $log3 = new Log(2, 1531680600, 'text');
        $log4 = new Log(2, 1531680600, 'text');

        $this->assertNotSame($log1->id, $log2->id);
        $this->assertNotSame($log1->id, $log3->id);
        $this->assertNotSame($log2->id, $log3->id);
        $this->assertSame($log3->id, $log4->id);
    }

    public function test_id_are_always_different_with_the_different_timestamp_but_same_position(): void
    {
        $log1 = new Log(0, 1531680600, 'text');
        $log2 = new Log(0, 1531680601, 'text');

        $this->assertNotSame($log1->id, $log2->id);
    }

    public function test_date_is_empty(): void
    {
        $log = new Log(0, 1531680600, 'text');

        $this->assertSame('', $log->date);
    }

    public function test_time_is_empty(): void
    {
        $log = new Log(0, 1531680600, 'text');

        $this->assertSame('', $log->date);
    }
}
