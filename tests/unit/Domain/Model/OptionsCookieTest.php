<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Model;

use App\Domain\Model\OptionsCookie;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class OptionsCookieTest extends Unit
{
    public function test_getter_and_setter_of_locale(): void
    {
        $optionsCookie = new OptionsCookie();
        $this->assertNull($optionsCookie->getLocale());

        $optionsCookie->setLocale('test_locale');

        $this->assertSame('test_locale', $optionsCookie->getLocale());
    }

    public function test_serialize_method(): void
    {
        $optionsCookie = new OptionsCookie();
        $this->assertSame('[null]', $optionsCookie->serialize());

        $optionsCookie->setLocale('test_locale');
        $result = $optionsCookie->serialize();

        $this->assertSame('["test_locale"]', $result);
    }

    public function test_unserialize_method(): void
    {
        $optionsCookie = new OptionsCookie();

        $optionsCookie->unserialize('["test_locale"]');

        $this->assertSame('test_locale', $optionsCookie->getLocale());
    }
}
