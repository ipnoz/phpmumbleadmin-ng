<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Settings;

use App\Domain\Action\Server\Settings\SettingsViewModel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsViewModelTest extends Unit
{
    public function test_constructor(): void
    {
        $viewModel = new SettingsViewModel(42);

        $result = $viewModel->getServerId();

        $this->assertSame(42, $result);
    }

    public function test_getter_and_setter_certificate(): void
    {
        $viewModel = new SettingsViewModel(42);
        $this->assertSame([], $viewModel->getCertificate());

        $viewModel->setCertificate($certificate = ['certificate']);
        $result = $viewModel->getCertificate();

        $this->assertSame($certificate, $result);
    }
}
