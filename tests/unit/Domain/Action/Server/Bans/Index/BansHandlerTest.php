<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Bans\Index;

use App\Domain\Action\Server\Bans\Ban;
use App\Domain\Action\Server\Bans\Index\BansHandler;
use App\Domain\Action\Server\Bans\Index\BansQuery;
use App\Domain\Action\Server\Bans\Index\BansViewModel;
use App\Domain\Action\Server\Bans\TotalBansInfoPanel;
use App\Domain\Helper\IpHelper;
use App\Domain\Murmur\Model\Mock\BanMock;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class BansHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new BansHandler();

        $result = $handler->listenTo();

        $this->assertSame(BansQuery::class, $result);
    }

    private function mockServer(bool $isRunning = true): MockObject
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('isRunning')->willReturn($isRunning);
        $mockServer->method('getBans')->willReturn([
            new BanMock(IpHelper::stringToDecimalIPv4('127.0.0.1'), 128, 'test username', 'test hash', 'test reason', 666, 42),
            new BanMock(IpHelper::stringToDecimalIPv4('192.168.10.0'), 120, 'test username2', 'test hash2', 'test reason2', 999, 96),
            new BanMock(IpHelper::stringToDecimalIPv6('::1'), 120, 'test username3', 'test hash3', 'test reason3', 999, 96),
            new BanMock([])
        ]);

        return $mockServer;
    }

    public function test_handler_on_success(): void
    {
        // Given
        $model = new BansViewModel(42);
        $query = new BansQuery(42, $model);
        $query->prx = $this->mockServer();
        $handler = new BansHandler();

        // When
        $response = $handler->handle($query);

        // Then
        [$ban1, $ban2, $ban3, $ban4] = $bans = $model->getBans();
        $infoPanels = $model->getInfoPanel();

        $this->assertCount(4, $bans);
        $this->assertContainsOnlyInstancesOf(Ban::class, $bans);
        $this->assertCount(1, $infoPanels);
        $this->assertInstanceOf(TotalBansInfoPanel::class, $infoPanels[0]);
        $this->assertSame(0, $ban1->id);
        $this->assertSame('127.0.0.1', $ban1->ip);
        $this->assertSame(null, $ban1->mask);
        $this->assertSame('test username', $ban1->name);
        $this->assertSame('test hash', $ban1->hash);
        $this->assertSame('test reason', $ban1->reason);
        $this->assertSame(666, $ban1->start);
        $this->assertSame(42, $ban1->duration);
        $this->assertSame(1, $ban2->id);
        $this->assertSame('192.168.10.0', $ban2->ip);
        $this->assertSame(24, $ban2->mask);
        $this->assertSame(2, $ban3->id);
        $this->assertSame('::1', $ban3->ip);
        $this->assertSame(120, $ban3->mask);
        $this->assertSame(3, $ban4->id);
    }

    public function test_handler_when_server_is_not_running(): void
    {
        // Given
        $model = new BansViewModel(42);
        $query = new BansQuery(42, $model);
        $query->prx = $this->mockServer(false);
        $handler = new BansHandler();

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertCount(0, $model->getBans());
        $this->assertCount(0, $model->getInfoPanel());
    }
}
