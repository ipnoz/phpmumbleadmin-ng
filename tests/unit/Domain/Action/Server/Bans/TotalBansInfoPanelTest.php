<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Bans;

use App\Domain\Action\Server\Bans\TotalBansInfoPanel;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TotalBansInfoPanelTest extends Unit
{
    public function test_constructor(): void
    {
        $infoPanel = new TotalBansInfoPanel(42);

        $result = $infoPanel->getArguments();

        $this->assertSame(['%total%' => 42], $result);
    }

    public function test_get_test_method(): void
    {
        $infoPanel = new TotalBansInfoPanel(42);

        $result = $infoPanel->getText();

        $this->assertSame('bans_total', $result);
    }
}
