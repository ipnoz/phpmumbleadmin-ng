<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations;

use App\Domain\Action\Server\Registrations\Registration;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationTest extends Unit
{
    public function test_constructor(): void
    {
        $registration = new Registration(42, []);

        $this->assertSame(42, $registration->id);
        $this->assertSame('', $registration->username);
        $this->assertSame('', $registration->email);
        $this->assertSame('', $registration->description);
        $this->assertSame('', $registration->certificate);
        $this->assertSame('', $registration->lastActivity);
        $this->assertFalse($registration->status);
    }

    public function test_constructor_with_arguments(): void
    {
        $registration = new Registration(24, [
            'test username field',
            'test email field',
            'test description field',
            'test certificate field',
            'test password field',
            'test lastActivity field'
        ]);

        $this->assertSame(24, $registration->id);
        $this->assertSame('test username field', $registration->username);
        $this->assertSame('test email field', $registration->email);
        $this->assertSame('test description field', $registration->description);
        $this->assertSame('test certificate field', $registration->certificate);
        $this->assertSame('test lastActivity field', $registration->lastActivity);
    }
}
