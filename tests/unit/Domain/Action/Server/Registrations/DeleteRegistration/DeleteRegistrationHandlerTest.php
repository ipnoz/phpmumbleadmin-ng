<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\DeleteRegistration;

use App\Domain\Action\Server\Registrations\DeleteRegistration\DeleteRegistrationCommand;
use App\Domain\Action\Server\Registrations\DeleteRegistration\DeleteRegistrationHandler;
use App\Domain\Action\Server\Registrations\DeleteRegistration\RegistrationDeleted;
use App\Domain\Action\Server\Registrations\DeleteRegistration\SuperUserCannotBeDeleted;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteRegistrationHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new DeleteRegistrationHandler();

        $this->assertSame(DeleteRegistrationCommand::class, $handler->listenTo());
    }

    public function test_handler_method_return_success_event(): void
    {
        $command = new DeleteRegistrationCommand(1, 2);
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->expects($this->once())->method('unregisterUser')->with(2);
        $handler = new DeleteRegistrationHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(RegistrationDeleted::class));
        /** @var RegistrationDeleted $event */
        $event = $response->getEvent(RegistrationDeleted::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->registrationId);
    }

    public function test_handler_method_return_error_event_when_trying_to_delete_the_superuser(): void
    {
        $command = new DeleteRegistrationCommand(1, 0);
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->expects($this->never())->method('unregisterUser');
        $handler = new DeleteRegistrationHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(SuperUserCannotBeDeleted::class));
        /** @var SuperUserCannotBeDeleted $event */
        $event = $response->getEvent(SuperUserCannotBeDeleted::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(0, $event->registrationId);
    }
}
