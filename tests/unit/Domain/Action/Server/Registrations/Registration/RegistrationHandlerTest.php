<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Registration;

use App\Domain\Action\Server\Registrations\Registration;
use App\Domain\Action\Server\Registrations\Registration\AccessToRegistrationIsNotAllowed;
use App\Domain\Action\Server\Registrations\Registration\RegistrationDoNotExist;
use App\Domain\Action\Server\Registrations\Registration\RegistrationHandler;
use App\Domain\Action\Server\Registrations\Registration\RegistrationQuery;
use App\Domain\Action\Server\Registrations\Registration\RegistrationViewModel;
use App\Domain\Action\Server\ServerIsNotBooted;
use App\Domain\Murmur\Exception\Server\InvalidUserException;
use App\Domain\Murmur\Model\Mock\UserMock;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new RegistrationHandler($this->makeEmpty(SecurityServiceInterface::class));

        $result = $handler->listenTo();

        $this->assertSame(RegistrationQuery::class, $result);
    }

    private function mockSecurityService(bool $isGranted = true): SecurityServiceInterface
    {
        return $this->makeEmpty(SecurityServiceInterface::class, ['canAccessToRegistration' => $isGranted]);
    }

    private function mockServer(bool $isRunning = true): MockObject
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('isRunning')->willReturn($isRunning);
        $mockServer->method('getRegisteredUsers')->willReturn([
            [0 => 'username field', 1 => 'email field', 2 => 'description field', 3 => 'certificate field', 5 => 'lastActivity field'],
            [0 => 'username2 field', 1 => 'email field', 2 => 'description field', 3 => 'certificate field', 5 => 'lastActivity field'],
        ]);
        $mockServer->method('getTexture')->willReturn([42, 84]);
        $mockServer->method('getUsers')->willReturn([
            new UserMock(42, 1), new UserMock(96, 2),
        ]);
        return $mockServer;
    }

    public function test_handler_on_success(): void
    {
        // Given
        $model = new RegistrationViewModel(42);
        $query = new RegistrationQuery(42, 96, $model);
        $query->prx = $this->mockServer();
        $query->prx->method('getRegistration')->willReturn([]);
        $handler = new RegistrationHandler($this->mockSecurityService());

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertFalse($query->viewModel->getHasError());
        $this->assertSame(['key' => null, 'translationDomain' => null], $query->viewModel->getErrorMessage());
        $this->assertTrue($query->viewModel->getHasConnection());
        $this->assertCount(1, $query->viewModel->getInfoPanel());
        $this->assertInstanceOf(Registration::class, $query->viewModel->getRegistration());
        $this->assertSame('data:image/png;base64, KlQ=', $query->viewModel->getAvatar());
    }

    public function test_handler_when_server_is_not_running(): void
    {
        // Given
        $model = new RegistrationViewModel(42);
        $query = new RegistrationQuery(42, 96, $model);
        $query->prx = $this->mockServer(false);
        $handler = new RegistrationHandler($this->mockSecurityService());

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertTrue($response->hasEvent(ServerIsNotBooted::class));
        $this->assertCount(1, $response->getEvents());
    }

    public function test_handler_when_a_superuser_ru_requested_the_superuser_account(): void
    {
        // Given
        $model = new RegistrationViewModel(42);
        $query = new RegistrationQuery(42, 0, $model);
        $query->prx = $this->mockServer();
        $handler = new RegistrationHandler($this->mockSecurityService(false));

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(AccessToRegistrationIsNotAllowed::class));
    }

    public function test_handler_when_requested_a_non_existant_registration_id(): void
    {
        // Given
        $model = new RegistrationViewModel(42);
        $query = new RegistrationQuery(42, 666, $model);
        $query->prx = $this->mockServer();
        $query->prx->method('getRegistration')->willThrowException(new InvalidUserException());
        $handler = new RegistrationHandler($this->mockSecurityService());

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(RegistrationDoNotExist::class));
        /** @var RegistrationDoNotExist $event */
        $event = $response->getEvent(RegistrationDoNotExist::class);
        $this->assertSame(42, $event->serverId);
        $this->assertSame(666, $event->registrationId);
    }
}
