<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\Index;

use App\Domain\Action\Server\Registrations\Index\RegistrationsHandler;
use App\Domain\Action\Server\Registrations\Index\RegistrationsQuery;
use App\Domain\Action\Server\Registrations\Index\RegistrationsViewModel;
use App\Domain\Action\Server\Registrations\TotalRegistrationsInfoPanel;
use App\Domain\Murmur\Model\Mock\UserMock;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new RegistrationsHandler($this->makeEmpty(SecurityServiceInterface::class));

        $result = $handler->listenTo();

        $this->assertSame(RegistrationsQuery::class, $result);
    }

    private function mockServer(bool $isRunning = true): ServerInterface
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('isRunning')->willReturn($isRunning);
        $mockServer->method('getRegisteredUsers')->willReturn([0 => 'SuperUser', 1 => 'user2', 2 => 'user3']);
        $mockServer->method('getRegistration')->willReturnOnConsecutiveCalls(['SuperUser'], ['user2'], ['user3']);
        $mockServer->method('getUsers')->willReturn([new UserMock(42, 2)]);

        return $mockServer;
    }

    public function test_handler_on_success(): void
    {
        // Given
        $model = new RegistrationsViewModel(42);
        $query = new RegistrationsQuery(42, $model);
        $query->prx = $this->mockServer();
        $handler = new RegistrationsHandler($this->makeEmpty(SecurityServiceInterface::class, ['canAccessToRegistration' => true]));

        // When
        $response = $handler->handle($query);

        // Then
        $registrations = $model->getRegistrations();
        $infoPanel = $model->getInfoPanel();

        $this->assertCount(3, $registrations);
        $this->assertCount(1, $infoPanel);
        $this->assertInstanceOf(TotalRegistrationsInfoPanel::class, $infoPanel[0]);
        $this->assertSame(0, $registrations[0]->id);
        $this->assertSame('SuperUser', $registrations[0]->username);
        $this->assertFalse($registrations[0]->status);
        $this->assertSame(1, $registrations[1]->id);
        $this->assertSame('user2', $registrations[1]->username);
        $this->assertFalse($registrations[1]->status);
        $this->assertSame(2, $registrations[2]->id);
        $this->assertSame('user3', $registrations[2]->username);
        $this->assertTrue($registrations[2]->status);
    }

    public function test_handler_when_server_is_not_running(): void
    {
        // Given
        $model = new RegistrationsViewModel(42);
        $query = new RegistrationsQuery(42, $model);
        $query->prx = $this->mockServer(false);
        $handler = new RegistrationsHandler($this->makeEmpty(SecurityServiceInterface::class, ['canAccessToRegistration' => true]));

        // When
        $response = $handler->handle($query);

        // Then
        $this->assertCount(0, $model->getRegistrations());
        $this->assertCount(0, $model->getInfoPanel());
    }

    private function mockServer2(): ServerInterface
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('isRunning')->willReturn(true);
        $mockServer->method('getRegisteredUsers')->willReturn([0 => 'SuperUser', 1 => 'user2', 2 => 'user3']);
        $mockServer->method('getRegistration')->willReturnOnConsecutiveCalls(['user2'], ['user3']);
        $mockServer->method('getUsers')->willReturn([new UserMock(42, 2)]);

        return $mockServer;
    }

    public function test_handler_with_superuser_ru_is_granted(): void
    {
        // Given
        $model = new RegistrationsViewModel(42);
        $query = new RegistrationsQuery(42, $model);
        $query->prx = $this->mockServer2();
        $handler = new RegistrationsHandler($this->makeEmpty(SecurityServiceInterface::class, ['canAccessToRegistration' => false]));

        // When
        $handler->handle($query);

        // Then
        $registrations = $model->getRegistrations();
        $infoPanel = $model->getInfoPanel();

        $this->assertCount(2, $registrations);
        $this->assertCount(1, $infoPanel);
        $this->assertInstanceOf(TotalRegistrationsInfoPanel::class, $infoPanel[0]);
        $this->assertSame(1, $registrations[0]->id);
        $this->assertSame('user2', $registrations[0]->username);
        $this->assertFalse($registrations[0]->status);
        $this->assertSame(2, $registrations[1]->id);
        $this->assertSame('user3', $registrations[1]->username);
        $this->assertTrue($registrations[1]->status);
    }

    private function mockServer3(): ServerInterface
    {
        $mockServer = $this->createMock(ServerInterface::class);
        $mockServer->method('isRunning')->willReturn(true);
        $mockServer->method('getRegisteredUsers')->willReturn([0 => 'Test_custom_superuser_name']);
        $mockServer->method('getRegistration')->willReturnOnConsecutiveCalls(['Test_custom_superuser_name']);
        $mockServer->method('getUsers')->willReturn([]);

        return $mockServer;
    }

    public function test_handler_with_custom_superuser_name(): void
    {
        // Given
        $model = new RegistrationsViewModel(42);
        $query = new RegistrationsQuery(42, $model);
        $query->prx = $this->mockServer3();
        $handler = new RegistrationsHandler($this->makeEmpty(SecurityServiceInterface::class, ['canAccessToRegistration' => true]));

        // When
        $handler->handle($query);

        // Then
        $registrations = $model->getRegistrations();
        $infoPanel = $model->getInfoPanel();

        $this->assertCount(1, $registrations);
        $this->assertCount(1, $infoPanel);
        $this->assertInstanceOf(TotalRegistrationsInfoPanel::class, $infoPanel[0]);
        $this->assertSame(0, $registrations[0]->id);
        $this->assertSame('Test_custom_superuser_name (SuperUser)', $registrations[0]->username);
        $this->assertFalse($registrations[0]->status);
    }
}
