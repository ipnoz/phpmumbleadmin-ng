<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Action\Server\Registrations\EditRegistration\UsernameAlreadyExist;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UsernameAlreadyExistTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new UsernameAlreadyExist(42, 'existing username');

        $this->assertSame(42, $command->serverId);
        $this->assertSame('existing username', $command->username);
    }

    public function test_get_key_method(): void
    {
        $event = new UsernameAlreadyExist(42, 'existing username');

        $this->assertSame(UsernameAlreadyExist::KEY, $event->getKey());
    }
}
