<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Action\Server\Registrations\EditRegistration\EditRegistrationCommand;
use App\Domain\Action\Server\Registrations\EditRegistration\EditRegistrationHandler;
use App\Domain\Action\Server\Registrations\EditRegistration\InvalidCharacterForUsername;
use App\Domain\Action\Server\Registrations\EditRegistration\RegistrationEdited;
use App\Domain\Action\Server\Registrations\EditRegistration\ServerDontAllowHtmlTags;
use App\Domain\Murmur\Model\ServerInterface;
use App\Domain\Service\SecurityServiceInterface;
use Codeception\Test\Unit;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EditRegistrationHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class));

        $this->assertSame(EditRegistrationCommand::class, $handler->listenTo());
    }

    private function createServerMock($description): MockObject
    {
        $prx = $this->createMock(ServerInterface::class);
        $prx->method('validateUserChars')->willReturn(true);
        $prx->method('checkIsAllowHtmlOrStripTags')->willReturnCallback(
            function ($string, &$stripped) use ($description) {
                $stripped = false;
                return $description;
            });
        return $prx;
    }

    public function editRegistrationWithSuccessProvider(): array
    {
        return [
            [[], 'username_mod', null, null, null, ['username_mod']],
            [[], 'username_mod', 'email_mod', null, null, ['username_mod', 'email_mod']],
            [[], 'username_mod', 'email_mod', 'description_mod', null, ['username_mod', 'email_mod', 'description_mod']],
            [[], 'username_mod', 'email_mod', 'description_mod', 'password_mod', ['username_mod', 'email_mod', 'description_mod', 4 => 'password_mod']],
            [['username'], 'username_mod', null, null, null, ['username_mod']],
            [['username', 'email'], 'username_mod', 'email_mod', null, null, ['username_mod', 'email_mod']],
            [['username', 'email', 'description'], 'username_mod', 'email_mod', 'description_mod', null, ['username_mod', 'email_mod', 'description_mod']],
            [['username', 'email', 'description'], 'username_mod', 'email_mod', 'description_mod', 'password_mod', ['username_mod', 'email_mod', 'description_mod', 4 => 'password_mod']],
        ];
    }

    /**
     * @dataProvider editRegistrationWithSuccessProvider
     */
    public function test_handler_method_return_success_event($originalData, $username, $email, $description, $password, $expected): void
    {
        // Given
        $command = new EditRegistrationCommand(24, 42, $originalData, $username, $email, $description, $password);
        $command->prx = $this->createServerMock($description);
        $command->prx->expects($this->once())->method('updateRegistration')->withConsecutive([42, $expected]);
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class, [
            'canModifyRegistrationUsername' => true,
            'canModifyRegistrationPassword' => true,
        ]));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(RegistrationEdited::class));
        /** @var RegistrationEdited $event */
        $event = $response->getEvent(RegistrationEdited::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->registrationId);
    }

    public function nothingToEditRegistrationProvider(): array
    {
        $fields = [
            [['username'], 'username', null, null, null],
            [['', 'email'], '', 'email', null, null],
            [['', '', 'description'], null, null, 'description', null],
            [['username', 'email', 'description'], 'username', 'email', 'description', null]
        ];
        $empty = [
            [[], '', '', '', ''],
            [[], null, null, null, null],
            [[''], '', '', '', ''],
            [[''], null, null, null, null],
            [[null], '', '', '', ''],
            [[null], null, null, null, null],
            [['', ''], '', '', '', ''],
            [['', ''], null, null, null, null],
            [[null, null], '', '', '', ''],
            [[null, null], null, null, null, null],
            [['', '', ''], '', '', '', ''],
            [['', '', ''], null, null, null, null],
            [[null, null, null], '', '', '', ''],
            [[null, null, null], null, null, null, null],
        ];

        return \array_merge($fields, $empty);
    }

    /**
     * @dataProvider nothingToEditRegistrationProvider
     */
    public function test_handler_method_when_nothing_to_update($originalData, $username, $email, $description, $password): void
    {
        // Given
        $command = new EditRegistrationCommand(24, 42, $originalData, $username, $email, $description, $password);
        $command->prx = $this->createServerMock($description);
        $command->prx->expects($this->never())->method('updateRegistration');
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class, [
            'canModifyRegistrationUsername' => true,
            'canModifyRegistrationPassword' => true,
        ]));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(0, $response->getEvents());
    }

    public function test_handler_method_return_error_event_when_editing_username_with_invalid_characters(): void
    {
        // Given
        $command = new EditRegistrationCommand(24, 42, [], 'username with invalid characters', 'email', 'description', 'password');
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->method('validateUserChars')->willReturn(false);
        $command->prx->expects($this->never())->method('updateRegistration');
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class, [
            'canModifyRegistrationUsername' => true,
            'canModifyRegistrationPassword' => true,
        ]));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(InvalidCharacterForUsername::class));
        /** @var InvalidCharacterForUsername $event */
        $event = $response->getEvent(InvalidCharacterForUsername::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->registrationId);
        $this->assertSame('username with invalid characters', $event->username);
        $this->assertSame('validators', $event->getTranslationDomain());
    }

    public function test_handler_method_return_error_event_when_editing_description_with_html_and_server_dont_allow_it(): void
    {
        // Given
        $command = new EditRegistrationCommand(24, 42, [], 'username', 'email', 'description with html', 'password');
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->method('validateUserChars')->willReturn(true);
        $command->prx->method('checkIsAllowHtmlOrStripTags')->willReturnCallback(
            function ($string, &$stripped) {
                $stripped = true;
                return 'description with html';
            });
        $command->prx->expects($this->never())->method('updateRegistration');
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class, [
            'canModifyRegistrationUsername' => true,
            'canModifyRegistrationPassword' => true,
        ]));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ServerDontAllowHtmlTags::class));
        /** @var ServerDontAllowHtmlTags $event */
        $event = $response->getEvent(ServerDontAllowHtmlTags::class);
        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->registrationId);
        $this->assertSame('description with html', $event->description);
        $this->assertSame('validators', $event->getTranslationDomain());
    }

    public function test_handler_method_cant_edit_fields_if_not_granted(): void
    {
        // Given
        $command = new EditRegistrationCommand(24, 42, [], 'username', 'email', 'description', 'password');
        $command->prx = $this->createServerMock('description');
        $command->prx->expects($this->once())->method('updateRegistration')->withConsecutive([42, [1 => 'email', 2 => 'description']]);
        $handler = new EditRegistrationHandler($this->makeEmpty(SecurityServiceInterface::class, [
            'canModifyRegistrationUsername' => false,
            'canModifyRegistrationPassword' => false,
        ]));

        // When
        $response = $handler->handle($command);

        // Then
        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(RegistrationEdited::class));
    }
}
