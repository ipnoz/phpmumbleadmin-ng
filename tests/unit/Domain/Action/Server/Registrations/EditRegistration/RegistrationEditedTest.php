<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Registrations\EditRegistration;

use App\Domain\Action\Server\Registrations\EditRegistration\RegistrationEdited;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationEditedTest extends Unit
{
    public function test_constructor(): void
    {
        $command = new RegistrationEdited(42, 96);

        $this->assertSame(42, $command->serverId);
        $this->assertSame(96, $command->registrationId);
    }

    public function test_get_key_method(): void
    {
        $event = new RegistrationEdited(42, 96);

        $this->assertSame(RegistrationEdited::KEY, $event->getKey());
    }
}
