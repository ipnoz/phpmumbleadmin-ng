<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\UnlinkChannel;

use App\Domain\Action\Server\Channels\UnlinkChannel\CannotUnlinkSelf;
use App\Domain\Action\Server\Channels\UnlinkChannel\ChannelIsNotLinked;
use App\Domain\Action\Server\Channels\UnlinkChannel\ChannelUnlinked;
use App\Domain\Action\Server\Channels\UnlinkChannel\UnlinkChannelCommand;
use App\Domain\Action\Server\Channels\UnlinkChannel\UnlinkChannelHandler;
use App\Domain\Murmur\Model\Mock\ChannelMock;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UnlinkChannelHandlerTest extends Unit
{
    public function test_listen_to_method(): void
    {
        $handler = new UnlinkChannelHandler();

        $this->assertSame(UnlinkChannelCommand::class, $handler->listenTo());
    }

    private function createPrxMock(): ServerInterface
    {
        $channelMock = new ChannelMock();
        $channelMock->links = [24, 42];

        $expects = new ChannelMock();
        $expects->links = [24];

        $server = $this->createMock(ServerInterface::class);
        $server->method('getChannelState')->willReturn($channelMock);
        $server->expects($this->once())->method('setChannelState')->with($expects);

        return $server;
    }

    public function test_handler_method_return_success_event(): void
    {
        $command = new UnlinkChannelCommand(1);
        $command->channelId = 2;
        $command->unlinkId = 42;
        $command->prx = $this->createPrxMock();
        $handler = new UnlinkChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelUnlinked::class));
        /** @var ChannelUnlinked $event */
        $event = $response->getEvent(ChannelUnlinked::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->channelId);
        $this->assertSame(42, $event->unlinkId);
    }

    public function test_handler_method_when_trying_to_unlink_self_channel(): void
    {
        $command = new UnlinkChannelCommand(1);
        $command->channelId = 2;
        $command->unlinkId = 2;
        $command->prx = $this->createMock(ServerInterface::class);
        $command->prx->expects($this->never())->method('setChannelState');
        $handler = new UnlinkChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(CannotUnlinkSelf::class));
        /** @var CannotUnlinkSelf $event */
        $event = $response->getEvent(CannotUnlinkSelf::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->channelId);
        $this->assertSame(2, $event->unlinkId);
    }

    private function createPrxMock2(): ServerInterface
    {
        $channelMock = new ChannelMock();
        $channelMock->links = [24, 42];

        $server = $this->createMock(ServerInterface::class);
        $server->method('getChannelState')->willReturn($channelMock);
        $server->expects($this->never())->method('setChannelState');

        return $server;
    }

    public function test_handler_method_when_trying_to_unlink_not_linked_channel(): void
    {
        $command = new UnlinkChannelCommand(1);
        $command->channelId = 2;
        $command->unlinkId = 96;
        $command->prx = $this->createPrxMock2();
        $handler = new UnlinkChannelHandler();

        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(ChannelIsNotLinked::class));
        /** @var ChannelIsNotLinked $event */
        $event = $response->getEvent(ChannelIsNotLinked::class);
        $this->assertSame(1, $event->serverId);
        $this->assertSame(2, $event->channelId);
        $this->assertSame(96, $event->unlinkId);
    }
}
