<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Action\Server\Channels\SendMessageToUser\MessageSentToUser;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MessageSentToUserTest extends Unit
{
    public function test_constructor(): void
    {
        $event = new MessageSentToUser(42, 96, true);

        $this->assertSame(42, $event->serverId);
        $this->assertSame(96, $event->userSessionId);
        $this->assertTrue($event->stripped);
    }

    public function test_get_key_method(): void
    {
        $event = new MessageSentToUser(42, 96, true);

        $this->assertSame(MessageSentToUser::KEY, $event->getKey());
    }
}
