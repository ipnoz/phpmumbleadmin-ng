<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\SendMessageToUser;

use App\Domain\Action\Server\Channels\SendMessageToUser\EmptyMessage;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EmptyMessageTest extends Unit
{
    public function test_constructor(): void
    {
        $event = new EmptyMessage(42, 96);

        $this->assertSame(42, $event->serverId);
        $this->assertSame(96, $event->userSessionId);
    }

    public function test_get_key_method(): void
    {
        $event = new EmptyMessage(42, 96);

        $this->assertSame(EmptyMessage::KEY, $event->getKey());
    }
}
