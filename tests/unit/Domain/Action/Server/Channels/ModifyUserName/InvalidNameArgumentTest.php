<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\ModifyUserName;

use App\Domain\Action\Server\Channels\ModifyUserName\InvalidNameArgument;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class InvalidNameArgumentTest extends Unit
{
    public function test_constructor(): void
    {
        $event = new InvalidNameArgument(24, 42, 'new name field');

        $this->assertSame(24, $event->serverId);
        $this->assertSame(42, $event->userSessionId);
        $this->assertSame('new name field', $event->newName);
    }

    public function test_get_key_method(): void
    {
        $event = new InvalidNameArgument(24, 42, 'new name field');

        $this->assertSame(InvalidNameArgument::KEY, $event->getKey());
    }
}
