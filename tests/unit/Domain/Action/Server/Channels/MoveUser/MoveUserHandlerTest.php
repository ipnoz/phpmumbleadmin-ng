<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Action\Server\Channels\MoveUser;

use App\Domain\Action\Server\Channels\MoveUser;
use App\Domain\Murmur\Model\ServerInterface;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class MoveUserHandlerTest extends Unit
{
    private function createMockedServerForGetState(): object
    {
        return $this->makeEmpty(ServerInterface::class, [
            'getState' => function () {
                $userState = new \stdClass();
                $userState->channel = 96;
                return $userState;
            }
        ]);
    }

    public function test_listen_to_method(): void
    {
        $handler = new MoveUser\MoveUserHandler();

        $this->assertSame(MoveUser\MoveUserCommand::class, $handler->listenTo());
    }

    public function test_move_user_return_success_event(): void
    {
        $command = new MoveUser\MoveUserCommand(-1, 42, -1);
        $command->prx = $this->createMockedServerForGetState();

        $handler = new MoveUser\MoveUserHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveUser\UserMoved::class));
    }

    public function test_move_user_in_its_current_channel_return_error_event(): void
    {
        $command = new MoveUser\MoveUserCommand(-1, 42, 96);
        $command->prx = $this->createMockedServerForGetState();

        $handler = new MoveUser\MoveUserHandler();
        $response = $handler->handle($command);

        $this->assertCount(1, $response->getEvents());
        $this->assertTrue($response->hasEvent(MoveUser\UserAlreadyInChannel::class));
    }
}
