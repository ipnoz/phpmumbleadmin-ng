<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Manager;

use App\Domain\Manager\CertificateManager;
use App\Tests\Page\CertificatePage;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateManagerTest extends Unit
{
    public function test_class_cant_change_the_pem_with_constructor(): void
    {
        // Given I create the class with a valid PEM
        $manager = new CertificateManager(CertificatePage::PEM);

        // When I call the constructor with another string as PEM, I get
        // BadMethodCallException
        $this->expectException(\BadMethodCallException::class);
        $manager->__construct('invalid_pem');
    }

    public function test_parse_method_with_empty_pem(): void
    {
        $manager = new CertificateManager('');

        $result = $manager->parse();

        $this->assertSame([], $result);
    }

    public function test_parse_method(): void
    {
        $manager = new CertificateManager(CertificatePage::PEM);

        $result = $manager->parse();

        $this->assertEquals(CertificatePage::output(), $result);
    }

    public function test_parse_method_with_invalid_certificate(): void
    {
        $manager = new CertificateManager('invalid certificate string');

        $result = $manager->parse();

        $this->assertSame(['error' => 'invalid certificate'], $result);
    }

    public function test_get_private_key_method(): void
    {
        $manager = new CertificateManager(CertificatePage::PEM);

        $result = $manager->getPrivateKey();

        $this->assertSame(CertificatePage::PRIVATE_KEY.PHP_EOL, $result);

        // Test the cached result
        $result = $manager->getPrivateKey();

        $this->assertSame(CertificatePage::PRIVATE_KEY.PHP_EOL, $result);
    }

    public function test_get_private_key_method_with_invalid_certificate(): void
    {
        $manager = new CertificateManager('invalid_private_key');

        $result = $manager->getPrivateKey();

        $this->assertSame('', $result);

        // Test the cached result
        $result = $manager->getPrivateKey();

        $this->assertSame('', $result);
    }

    public function test_get_certificate_method(): void
    {
        $manager = new CertificateManager(CertificatePage::PEM);

        $result = $manager->getCertificate();

        $this->assertSame(CertificatePage::CERTIFICATE.PHP_EOL, $result);

        // Test the cached result
        $result = $manager->getCertificate();

        $this->assertSame(CertificatePage::CERTIFICATE.PHP_EOL, $result);
    }

    public function test_get_certificate_method_with_invalid_certificate(): void
    {
        $manager = new CertificateManager('invalid_certificate');

        $result = $manager->getCertificate();

        $this->assertSame('', $result);

        // Test the cached result
        $result = $manager->getCertificate();

        $this->assertSame('', $result);
    }

    public function test_constructor_add_missing_last_php_eol(): void
    {
        $manager = new CertificateManager(CertificatePage::PRIVATE_KEY.PHP_EOL.CertificatePage::CERTIFICATE);

        $result = $manager->getCertificate();

        $this->assertSame(CertificatePage::CERTIFICATE.PHP_EOL, $result);
    }

    public function test_constructor_add_missing_last_php_eol_2(): void
    {
        $manager = new CertificateManager(CertificatePage::CERTIFICATE.PHP_EOL.CertificatePage::PRIVATE_KEY);

        $result = $manager->getPrivateKey();

        $this->assertSame(CertificatePage::PRIVATE_KEY.PHP_EOL, $result);
    }

    public function test_constructor_dont_add_last_php_eol_if_one_already_set(): void
    {
        $manager = new CertificateManager(CertificatePage::CERTIFICATE.PHP_EOL.CertificatePage::PRIVATE_KEY.PHP_EOL);

        $result = $manager->getPrivateKey();

        $this->assertSame(CertificatePage::PRIVATE_KEY.PHP_EOL, $result);
    }

    public function test_verify_certificate_with_private_key_method(): void
    {
        $manager = new CertificateManager(CertificatePage::PEM);

        $result = $manager->verifyCertificateWithPrivateKey();

        $this->assertTrue($result);
    }

    public function invalidCertificateProvider(): array
    {
        return [
            'invalid certificate' => ['certificate' => 'invalid certificate'],
            'Empty string' => ['certificate' => ''],
            'Empty string with multiple spaces' => ['certificate' => '  '],
            'CertificatePage::CERTIFICATE' => ['certificate' => CertificatePage::CERTIFICATE],
            'CertificatePage::PRIVATE_KEY' => ['certificate' => CertificatePage::PRIVATE_KEY],
            'Certificate is not generated with the private key' => ['certificate' => CertificatePage::PRIVATE_KEY.PHP_EOL.CertificatePage::INVALID_CERTIFICATE],
        ];
    }

    /** @dataProvider invalidCertificateProvider */
    public function test_certificate_is_valid_method_with_invalid_certificate($certificate): void
    {
        $manager = new CertificateManager($certificate);

        $result = $manager->verifyCertificateWithPrivateKey();

        $this->assertFalse($result);
    }
}
