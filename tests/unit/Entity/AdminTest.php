<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Domain\Service\SecurityServiceInterface;
use App\Entity\AdminEntity;
use Codeception\Test\Unit;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminTest extends Unit
{
    public function test_it_set_only_available_admin_roles(): void
    {
        $admin = new AdminEntity();

        $admin->setRoles([SecurityServiceInterface::ROLE_ROOT_ADMIN, SecurityServiceInterface::ROLE_MUMBLE_USER, 'invalid role']);

        $this->assertContains(SecurityServiceInterface::ROLE_ROOT_ADMIN, $admin->getRoles());
        $this->assertNotContains(SecurityServiceInterface::ROLE_MUMBLE_USER, $admin->getRoles());
        $this->assertNotContains('invalid role', $admin->getRoles());
    }
}
