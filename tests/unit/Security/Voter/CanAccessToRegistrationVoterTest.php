<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Security\Voter;

use App\Domain\Service\SecurityServiceInterface;
use App\Infrastructure\Symfony\Security\Voter\CanAccessToRegistrationVoter;
use Codeception\Test\Unit;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanAccessToRegistrationVoterTest extends Unit
{
    public function validVoterProvider(): array
    {
        return [
            [0, true, VoterInterface::ACCESS_GRANTED],
            [1, true, VoterInterface::ACCESS_GRANTED],
            [0, false, VoterInterface::ACCESS_DENIED],
            [1, false, VoterInterface::ACCESS_GRANTED]
        ];
    }

    /**
     * @dataProvider validVoterProvider
     */
    public function test_with_valid_voter($subject, $granted, $expected): void
    {
        $voter = new CanAccessToRegistrationVoter($this->makeEmpty(SecurityServiceInterface::class, ['isGrantedSuperUser' => $granted]));

        $result = $voter->vote($this->makeEmpty(TokenInterface::class), $subject, [CanAccessToRegistrationVoter::KEY]);

        $this->assertSame($expected, $result);
    }

    public function invalidVoterProvider(): array
    {
        return [
            [1, 'not_supported_attribute_key'],
            ['not_supported_subject', CanAccessToRegistrationVoter::KEY],
            [null, CanAccessToRegistrationVoter::KEY]
        ];
    }

    /**
     * @dataProvider invalidVoterProvider
     */
    public function test_with_invalid_voter($subject, $attribute): void
    {
        $voter = new CanAccessToRegistrationVoter($this->makeEmpty(SecurityServiceInterface::class, ['isGrantedSuperUser' => true]));

        $result = $voter->vote($this->makeEmpty(TokenInterface::class), $subject, [$attribute]);

        $this->assertSame(VoterInterface::ACCESS_ABSTAIN, $result);
    }
}
