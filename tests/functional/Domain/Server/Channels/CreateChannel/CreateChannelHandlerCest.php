<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Domain\Server\Channels\CreateChannel;

use App\Domain\Action\Server\Channels\ChannelNameIsInvalid;
use App\Domain\Action\Server\Channels\CreateChannel\ChannelCreated;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelCommand;
use App\Domain\Action\Server\Channels\CreateChannel\CreateChannelHandler;
use App\Domain\Action\Server\Channels\CreateChannel\ParentChannelIsInvalid;
use App\Domain\Action\Server\Channels\NestingLimitReached;
use App\Domain\Murmur\Model\ServerInterface;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateChannelHandlerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function test_listen_to_method(FunctionalTester $I): void
    {
        $handler = new CreateChannelHandler();

        $I->assertSame(CreateChannelCommand::class, $handler->listenTo());
    }

    public function it_get_positive_event__when_creating_a_channel(FunctionalTester $I): void
    {
        // Given
        $handler = new CreateChannelHandler();
        $command = new CreateChannelCommand(ServerPage::TEST_SERVER_ID, 'NewChannel', 0);
        $command->prx = $I->getTheTestServer();

        // When I call the handle method
        $response = $handler->handle($command);

        // Then I see I get positive event
        $I->assertCount(1, $response->getEvents());
        $I->assertTrue($response->hasEvent(ChannelCreated::class));
    }

    public function it_get_negative_event_on_invalid_parent_channel_id(FunctionalTester $I): void
    {
        // Given
        $handler = new CreateChannelHandler();
        $command = new CreateChannelCommand(ServerPage::TEST_SERVER_ID, 'NewChannel', 9999999);
        $command->prx = $I->getTheTestServer();

        // When I call the handle method
        $response = $handler->handle($command);

        // Then I see I get negative event
        $I->assertCount(1, $response->getEvents());
        $I->assertTrue($response->hasEvent(ParentChannelIsInvalid::class));
    }

    public function it_get_negative_event_on_invalid_channel_characters(FunctionalTester $I): void
    {
        // Given
        $handler = new CreateChannelHandler();
        $command = new CreateChannelCommand(ServerPage::TEST_SERVER_ID, 'invalid°characters', 0);
        $command->prx = $I->getTheTestServer();

        // When I call the handle method
        $response = $handler->handle($command);

        // Then I see I get negative event
        $I->assertCount(1, $response->getEvents());
        $I->assertTrue($response->hasEvent(ChannelNameIsInvalid::class));
    }

    private function createConditionsForNestingLimitReachedError(ServerInterface $server): void
    {
        $server->setConf('channelnestinglimit', '3');

        $channels = $server->getChannels();
        foreach ($channels as $channel) {
            try {
                $server->removeChannel($channel->id);
            } catch (\Exception $e) {} // No Op
        }
        $server->addChannel('channel', 0);
        $server->addChannel('channel', 1);
        $server->addChannel('channel', 2);
    }

    public function it_get_negative_event_on_nesting_limit_reached(FunctionalTester $I): void
    {
        // Given
        $handler = new CreateChannelHandler();
        $command = new CreateChannelCommand(ServerPage::TEST_SERVER_ID, 'NewChannel', 3);
        $command->prx = $I->getTheTestServer();
        $this->createConditionsForNestingLimitReachedError($command->prx);

        // When I call the handle method
        $response = $handler->handle($command);

        // Then I see I get negative event
        $I->assertCount(1, $response->getEvents());
        $I->assertTrue($response->hasEvent(NestingLimitReached::class));
    }
}
