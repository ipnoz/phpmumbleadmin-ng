<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Administration\AdminLogs;

use App\Tests\FunctionalTester;
use App\Tests\Page\AdminLogsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminLogsControllerCest
{
    public function it_query_the_admin_logs_page(FunctionalTester $I): void
    {
        // Given I have no log in repository, and I'm logged as SuperAdmin
        $I->amLoggedAsSuperAdmin();

        // When I get the admin logs page
        $I->amOnPage(Page::LOCATION);

        // Then I get a 200 HTTP code
        $I->seeCurrentUrlMatches('#'.Page::LOCATION.'$#');
        $I->seeResponseCodeIs(200);
        $I->seeElement(Page::CONTAINER);
    }

    public function it_send_ajax_get_request_to_get_the_latest_logs(FunctionalTester $I): void
    {
        // Given I have 2 logs in repository, and I'm logged as SuperAdmin
        $I->addAdminLogInRepository(['message' => 'test admin log 1', 'timestamp' => 42424242]);
        $I->addAdminLogInRepository(['message' => 'test admin log 2', 'timestamp' => 84848484]);
        $I->addAdminLogInRepository(['message' => 'test admin log 3', 'timestamp' => 84848490]);
        $I->amLoggedAsSuperAdmin();

        // When I send a AJAX get request to the latest admin logs page, with lastLenLog of 2
        $I->sendAjaxGetRequest(Page::LOCATION.'/latest/2');

        // Then I see I get a 200 HTTP code, and the last log in Json
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseEquals(\json_encode([
            'logs' => [
                0 => [
                    'position' => 0,
                    'id' => '84848490_0',
                    'timestamp' => 84848490,
                    'text' => 'test admin log 3',
                    'date' => '9 septembre 1972',
                    'time' => '02:01:30'
                ]
            ]
        ]));
    }
}
