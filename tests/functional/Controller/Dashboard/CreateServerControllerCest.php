<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Dashboard;

use App\Domain\Action\Dashboard\CreateServer\ServerCreated;
use App\Infrastructure\Symfony\Form\CreateServerType;
use App\Tests\FunctionalTester;
use App\Tests\Page\DashboardPage as Dashboard;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreateServerControllerCest
{
    public function it_send_a_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send empty form data
        $I->sendPOST(Dashboard::CREATE_SERVER_CMD, [
            CreateServerType::BLOCK_PREFIX => [],
        ]);

        // Then I see I get the CSRF error message
        $I->seeResponseCodeIsFormError();
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form.');
    }

    public function it_send_a_post_request_with_valid_data(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I send post with valid data
        $I->sendPostWithCsrf(Dashboard::CREATE_SERVER_CMD, CreateServerType::BLOCK_PREFIX, []);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource(ServerCreated::KEY);
    }
}
