<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Security;

use App\Tests\FunctionalTester;
use App\Tests\Page\ForgottenPasswordPage as Page;
use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ForgottenPasswordResetControllerCest
{
    public function it_send_a_get_request_with_invalid_hash(FunctionalTester $I): void
    {
        // When I want to get the reset page
        $I->amOnPage(Page::LOCATION_RESET.'/invalid_hash');

        // Then
        $I->seeResponseCodeIs(404);
    }

    public function it_send_a_get_request_when_logged(FunctionalTester $I): void
    {
        // Given I'm logged
        $I->amLoggedAsSuperAdmin();

        // When I want to get the reset page
        $I->amOnPage(Page::LOCATION_RESET.'/dummy_hash');

        // Then
        $I->seeResponseCodeIs(403);
    }

    public function it_send_a_post_request_without_csrf_token(FunctionalTester $I): void
    {
        // Given I have a Forgotten Password
        $admin = $I->addAdminUserInRepository();
        $forgottenPassword = $I->addForgottenPasswordInRepository(['admin' => $admin]);

        // When
        $I->sendPost(Page::LOCATION_RESET.'/'.$forgottenPassword->getHash(), [
            Page::FORM_RESET_BLOCK_PREFIX => []
        ]);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeInSource('The CSRF token is invalid. Please try to resubmit the form');
    }

    public function it_send_a_post_request_with_valid_data_and_login_with_the_new_password(FunctionalTester $I): void
    {
        // Given I have a Forgotten Password
        $admin = $I->addAdminUserInRepository(['password' => 'lost_password']);
        $forgottenPassword = $I->addForgottenPasswordInRepository(['admin' => $admin]);

        // When I submit the form
        $I->sendPostWithCsrf(Page::LOCATION_RESET.'/'.$forgottenPassword->getHash(), Page::FORM_RESET_BLOCK_PREFIX, [
            'password' => [
                'first' => 'newPassword',
                'second' => 'newPassword',
            ],
        ]);

        // Then I see the message that the password has been reset
        $I->seeResponseCodeIs(200);
        $I->see('password_has_been_reset');

        // I can log in with the new password
        $I->amOnPage(LoginPage::LOCATION);
        $I->logIn($admin->getUserIdentifier(), 'newPassword');
        $I->seeAmLoggedAs($admin->getUserIdentifier());
    }

    public function it_send_a_post_request_with_valid_data_two_times(FunctionalTester $I): void
    {
        // Given I have a Forgotten Password
        $admin = $I->addAdminUserInRepository(['password' => 'lost_password']);
        $I->addForgottenPasswordInRepository(['admin' => $admin, 'hash' => 'same_hash']);

        // When I submit the form
        $I->sendPostWithCsrf(Page::LOCATION_RESET.'/same_hash', Page::FORM_RESET_BLOCK_PREFIX, [
            'password' => [
                'first' => 'newPassword',
                'second' => 'newPassword',
            ],
        ]);

        // Then I see the message that the password has been reset
        $I->see('password_has_been_reset');

        // When I submit the form with the same hash
        $I->sendPostWithCsrf(Page::LOCATION_RESET.'/same_hash', Page::FORM_RESET_BLOCK_PREFIX, [
            'password' => [
                'first' => 'newPassword',
                'second' => 'newPassword',
            ],
        ]);

        // Then I see a page not found error, because the Forgotten Password doesn't exist anymore
        $I->seeResponseCodeIs(404);
    }

    /*
     * The ForgottenPasswordService can remove the ForgottenPassword entity at any
     * time.
     * Because of that, check that the wrong repeated password scenario always work...
     */
    public function it_submit_a_wrong_repeated_password(FunctionalTester $I): void
    {
        // Given I have a Forgotten Password
        $admin = $I->addAdminUserInRepository(['password' => 'lost_password']);
        $I->addForgottenPasswordInRepository(['admin' => $admin, 'hash' => 'dummy_hash']);

        // When I submit the form with a wrong repeated password
        $I->amOnPage(Page::LOCATION_RESET.'/dummy_hash');
        $I->fillField(Page::FORM_RESET_BLOCK_PREFIX.'[password][first]', 'newPassword');
        $I->fillField(Page::FORM_RESET_BLOCK_PREFIX.'[password][second]', 'wrongRepeatedPassword');
        $I->click('submit', Page::FORM_RESET_ELEMENT);

        // Then I see the invalid form message
        $I->seeFormError('validator_password_not_identical');

        // When I submit the form with the same hash
        $I->amOnPage(Page::LOCATION_RESET.'/dummy_hash');
        $I->fillField(Page::FORM_RESET_BLOCK_PREFIX.'[password][first]', 'newPassword');
        $I->fillField(Page::FORM_RESET_BLOCK_PREFIX.'[password][second]', 'newPassword');
        $I->click('submit', Page::FORM_RESET_ELEMENT);

        // Then I see the message that the password has been reset
        $I->seeResponseCodeIs(200);
        $I->seeSuccessFlashMessage('password_has_been_reset');
    }

    public function it_send_post_request_with_expired_validity_two_times(FunctionalTester $I): void
    {
        // Given I have an expired Forgotten Password
        $admin = $I->addAdminUserInRepository();
        $I->addForgottenPasswordInRepository(['admin' => $admin, 'hash' => 'dummy_hash', 'valid_until' => '-1hour']);

        // When I submit the form
        $I->sendPostWithCsrf(Page::LOCATION_RESET.'/dummy_hash', Page::FORM_RESET_BLOCK_PREFIX, [
            'password' => [
                'first' => 'newPassword',
                'second' => 'newPassword',
            ],
        ]);

        // Then I see the expired message
        $I->see('hash_has_expired');

        // When I submit the form a second time
        $I->sendPostWithCsrf(Page::LOCATION_RESET.'/dummy_hash', Page::FORM_RESET_BLOCK_PREFIX, [
            'password' => [
                'first' => 'newPassword',
                'second' => 'newPassword',
            ],
        ]);

        // Then I see the page not found error, because the ForgottenPassword has been deleted
        $I->seeResponseCodeIs(404);
    }
}
