<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Settings;

use App\Tests\FunctionalTester;
use App\Tests\Page\CertificatePage;
use App\Tests\Page\ServerSettingsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ResetCertificateControllerCest
{
    public function it_send_a_request_to_reset_certificate(FunctionalTester $I): void
    {
        // Given I have a server with a valid certificate
        $server = $I->getTheTestServer();
        $server->setConf('key', CertificatePage::PRIVATE_KEY);
        $server->setConf('certificate', CertificatePage::CERTIFICATE);
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When I query the reset certificate page
        $I->amOnPage(Page::LOCATION.'/cmd/reset_certificate');

        // Then I see I've been redirect to the server settings page
        $I->seeResponseIsRedirectionTo(Page::LOCATION.'#tab-certificate');
        // And I see the certificate has been reset
        $I->assertSame('', $server->getConf('key'));
        $I->assertSame('', $server->getConf('certificate'));
    }
}
