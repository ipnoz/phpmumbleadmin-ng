<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Settings;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerSettingsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class SettingsControllerCest
{
    public function it_send_a_request_to_the_settings_page_when_the_server_is_online(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement('#MumbleSettingsContainer');
        $I->seeNumberOfElements('#MumbleSettingsTabs .nav-item', 4);
        foreach (['server', 'administration', 'welcome_text', 'certificate'] as $text) {
            $I->see($text, '#MumbleSettingsTabs .nav-item');
        }
    }

    public function it_send_a_request_to_the_settings_page_with_the_super_user(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperUser();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement('#MumbleSettingsContainer');
        $I->seeNumberOfElements('#MumbleSettingsTabs .nav-item', 3);
        $I->dontSee('administration', '#MumbleSettingsTabs .nav-item');
    }

    public function it_submit_form_when_the_server_is_offline(FunctionalTester $I): void
    {
        // Given
        $I->resetAllServers(false);
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Page::CONTAINER);
        $I->seeNumberOfElements(Page::CONTAINER.' .nav-item', 4);
        foreach (['server', 'administration', 'welcome_text', 'certificate'] as $text) {
            $I->see($text, '#MumbleSettingsTabs .nav-item');
        }

        // Given
        $I->dontSee('test registername field', Page::OUTPUT_ELEMENT);

        // When
        $I->fillField(Page::FORM.' [name="app_settings_page[registername]"]', 'test registername field');
        $I->click('Submit');

        // Then
        $I->see('test registername field', Page::OUTPUT_ELEMENT);
    }

    public function it_get_form_error_with_registername_characters(FunctionalTester $I): void
    {
        // Given I have a server which only accept any word character for channel names
        $I->resetAllServers();
        $I->amLoggedAsSuperAdmin();
        $server = $I->getTheTestServer();
        $server->setConf('channelname', '[\w]+');

        // When I submit registername field with space characters
        $I->amOnPage(Page::LOCATION);
        $I->fillField(Page::FORM.' [name="app_settings_page[registername]"]', 'invalid space character');
        $I->click('Submit');

        // Then I see I got form error
        $I->seeFormErrorOn('validator_characters_error_message', Page::FORM);
    }
}
