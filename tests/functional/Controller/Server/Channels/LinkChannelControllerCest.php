<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\LinkChannel\CannotLinkSelf;
use App\Infrastructure\Symfony\Form\LinkChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LinkChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/link_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_post_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxPostRequest(self::CONTROLLER_URL);

        // Then
        $I->seeResponseCodeIsFormError();
    }

    public function it_link_root_channel_with_itself(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I try to link the root channel to itself
        $I->sendPostWithCsrf(self::CONTROLLER_URL, LinkChannelType::BLOCK_PREFIX, [
            'channelId' => 0,
            'withChannelId' => 0,
        ]);

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeResponseContainsJson([CannotLinkSelf::KEY]);
    }
}
