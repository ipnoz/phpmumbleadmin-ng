<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ChannelsControllerCest
{
    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_request_to_the_channels_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement('#MumbleViewerContainer');
        $I->seeElement('#MumbleViewerPanel');
        $I->seeElement('.MumbleViewer');
    }

    public function it_send_a_xml_request_to_the_channels_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxGetRequest(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeResponseIsJson();
    }
}
