<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Domain\Action\Server\Channels\UnlinkChannel\CannotUnlinkSelf;
use App\Infrastructure\Symfony\Form\UnlinkChannelType;
use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UnlinkChannelControllerCest
{
    private const CONTROLLER_URL = Page::LOCATION.'/cmd/unlink_channel';

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
    }

    public function it_send_a_post_request_without_form_fields(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxPostRequest(self::CONTROLLER_URL);

        // Then
        $I->seeResponseCodeIsFormError();
    }

    public function it_unlink_root_channel_with_itself(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When I try to link the root channel to itself
        $I->sendPostWithCsrf(self::CONTROLLER_URL, UnlinkChannelType::BLOCK_PREFIX, [
            'channelId' => 0,
            'unlinkId' => 0,
        ]);

        // Then
        $I->seeResponseCodeIs(403);
        $I->seeResponseContainsJson([CannotUnlinkSelf::KEY]);
    }
}
