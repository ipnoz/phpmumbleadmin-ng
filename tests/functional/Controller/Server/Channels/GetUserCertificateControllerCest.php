<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Channels;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class GetUserCertificateControllerCest
{
    private int $sessionIdUser1;
    private int $sessionIdUser2WithCertificate;

    public function _before(FunctionalTester $I): void
    {
        $I->haveTheTestServerOnline();
        $I->haveOneMumbleClient();
        $I->haveOneMumbleClient(Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME);

        // Get the session id of connected users
        $server = $I->getTheTestServer();
        $users = $server->getUsers();
        foreach ($users as $user) {
            if ($user->name === Page::NODEJS_CLIENT1_NAME) {
                $this->sessionIdUser1 = (int) $user->session;
            }
            if ($user->name === Page::NODEJS_CLIENT_WITH_CERTIFICATE_NAME) {
                $this->sessionIdUser2WithCertificate = (int) $user->session;
            }
        }
    }

    public function it_send_ajax_request_to_controller_for_user_without_certificate(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxGetRequest(Page::LOCATION.'/user_certificate/'.$this->sessionIdUser1);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('no_certificate');
    }

    public function it_send_ajax_request_to_controller_for_user_with_certificate(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->sendAjaxGetRequest(Page::LOCATION.'/user_certificate/'.$this->sessionIdUser2WithCertificate);

        // Then
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('Internet Widgits Pty Ltd');
        $I->seeResponseContains('82:75:A4:5E:F3:23:C5:A9:50:45:03:D7:E5:73:1C:F0:7A:B3:00:71');
    }
}
