<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Registrations;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerPage;
use App\Tests\Page\ServerRegistrationsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DeleteRegistrationControllerCest
{
    public function it_send_a_get_request_to_delete_a_registration_controller(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();
        $I->stopFollowingRedirects();

        // When
        $I->amOnPage(ServerPage::LOCATION.'/cmd/registration/delete/1');

        // Then
        $I->seeResponseCodeIsRedirection();
        $I->seeLink(Page::LOCATION);
    }
}
