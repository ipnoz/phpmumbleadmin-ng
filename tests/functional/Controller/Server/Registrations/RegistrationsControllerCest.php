<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Controller\Server\Registrations;

use App\Tests\FunctionalTester;
use App\Tests\Page\ServerRegistrationsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RegistrationsControllerCest
{
    public function it_send_a_request_to_the_registrations_page(FunctionalTester $I): void
    {
        // Given
        $I->haveTheTestServerOnline();
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage(Page::LOCATION);

        // Then
        $I->seeResponseCodeIsSuccessful();
        $I->seeElement(Page::CONTAINER);
    }
}
