<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Page\TwigBundle\Exception;

use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Error403Cest
{
    public function it_display_the_403_error_twig_preview_page(FunctionalTester $I): void
    {
        // Given
        $I->amLoggedAsSuperAdmin();

        // When
        $I->amOnPage('_error/403');

        // Then
        $I->seeElement('.container-error');
        $I->seeElement('.app-container');
        $I->see('an_error_occured');
        $I->seetNotTranslatedText('the_server_returned_error', ['%error_server_message%' => '403 Forbidden']);
        $I->see('you_are_not_authorized_to_access_this_resource');
        $I->seeLink('return_to_the_homepage');
    }
}
