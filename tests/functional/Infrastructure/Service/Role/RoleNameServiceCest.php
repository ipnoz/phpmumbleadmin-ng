<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service\Role;

use App\Domain\Service\SecurityServiceInterface as Security;
use App\Entity\AdminEntity;
use App\Infrastructure\Service\Role\RoleNameService;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class RoleNameServiceCest
{
    private RoleNameService $service;

    public function _before(FunctionalTester $I): void
    {
        if (! isset($this->service)) {
            $this->service = $I->grabService(RoleNameService::class);
        }
    }

    protected function rolesProvider(): array
    {
        return [
            ['roles' => [Security::ROLE_SUPER_ADMIN], 'expected' => RoleNameService::SUPER_ADMIN],
            // Reverse roles must return the best role name
            ['roles' => [Security::ROLE_MUMBLE_USER, Security::ROLE_SUPER_ADMIN], 'expected' => RoleNameService::SUPER_ADMIN],
            // Multiple roles, before and after the best role, must return the best role
            ['roles' => ['ROLE_USER', Security::ROLE_SUPER_USER, Security::ROLE_SUPER_ADMIN, Security::ROLE_MUMBLE_USER, 'ROLE_NOT_CONFIGURED'], 'expected' => RoleNameService::SUPER_ADMIN],
            // Invalid/empty roles
            ['roles' => [], 'expected' => RoleNameService::INVALID],
            ['roles' => [''], 'expected' => RoleNameService::INVALID],
            ['roles' => ['ROLE_NOT_CONFIGURED'], 'expected' => RoleNameService::INVALID],
            ['roles' => ['invalid string'], 'expected' => RoleNameService::INVALID]
        ];
    }

    /** @dataProvider rolesProvider */
    public function it_find_the_right_role_name(FunctionalTester $I, Example $data): void
    {
        // Given we created a user with roles
        $user = new AdminEntity();
        $user->setRoles($data['roles']);

        // When we call the find method
        $result = $this->service->find($user);

        // Then we expect the right role name string
        $I->assertSame($data['expected'], $result);
    }
}
