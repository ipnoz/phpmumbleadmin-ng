<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Service\Logger;

use App\Infrastructure\Service\Logger\LoggerService;
use App\Tests\FunctionalTester;
use Codeception\Example;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Test that the LoggerService add messages to Monolog/Logger correctly
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class LoggerServiceCest
{
    private LoggerService $service;

    /** @var Logger $monolog */
    private LoggerInterface $monolog;

    public function _before(FunctionalTester $I): void
    {
        $this->service = $I->grabService(LoggerService::class);

        $this->monolog = $I->grabService(LoggerInterface::class);
        if (! $this->monolog instanceof Logger) {
            $I->fail(Logger::class.' is required as alias of '.LoggerInterface::class);
        }

        // Enable TestHandler on Monolog
        $this->monolog->setHandlers([new TestHandler()]);
    }

    public function it_test_log_method(FunctionalTester $I): void
    {
        // When I use LoggerService::log method to add a message with the warning level
        $this->service->log(-1, Logger::WARNING, 'server_started', ['%id%' => 42]);

        // Then I see Monolog has a warning level message to handle, translated with context
        $handler = $this->monolog->getHandlers()[0];
        /** @var TestHandler $handler */
        $formattedMessage = $handler->getRecords()[0]['formatted'];
        $I->assertStringContainsString('app.WARNING: server_started {"%id%":42}', $formattedMessage);
    }

    private function levelProvider(): array
    {
        return [
            ['level' => 'emergency'],
            ['level' => 'alert'],
            ['level' => 'critical'],
            ['level' => 'error'],
            ['level' => 'warning'],
            ['level' => 'notice'],
            ['level' => 'info'],
            ['level' => 'debug'],
        ];
    }

    /** @dataProvider levelProvider */
    public function it_test_methods(FunctionalTester $I, Example $data): void
    {
        // Given the level of context
        $level = $data['level'];

        // When I use the level method to add a message with the LoggerService
        $this->service->$level(-1, 'server_started', ['%id%' => 42]);

        // Then I see Monolog has a message to handle for the right level, translated with context
        $handler = $this->monolog->getHandlers()[0];
        $formattedMessage = $handler->getRecords()[0]['formatted'];
        $I->assertStringContainsStringIgnoringCase('app.'.$level.': server_started {"%id%":42}', $formattedMessage);
    }
}
