<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Murmur\Connection;

use App\Infrastructure\Murmur\Connection\ConnectionHandler;
use App\Tests\FunctionalTester;

/**
 * Test ConnectionHandler as Symfony service
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConnectionHandlerCest
{
    // The Symfony service have to be shared to avoid executing multiple connection
    public function it_get_the_shared_instance_of_connection_handler(FunctionalTester $I): void
    {
        $RPC1 = $I->grabService(ConnectionHandler::class);
        $RPC2 = $I->grabService(ConnectionHandler::class);

        $I->assertSame($RPC1, $RPC2);
    }
}
