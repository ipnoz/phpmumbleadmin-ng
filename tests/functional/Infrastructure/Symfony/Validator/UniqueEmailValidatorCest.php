<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Validator;

use App\Infrastructure\Symfony\Validator\Constraints\UniqueEmail;
use App\Tests\FunctionalTester;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UniqueEmailValidatorCest
{
    private ValidatorInterface $validator;

    public function _before(FunctionalTester $I): void
    {
        $this->validator = $I->grabService(ValidatorInterface::class);
    }

    public function it_validate_with_success_an_unexistant_email(FunctionalTester $I)
    {
        $violations = $this->validator->validate('unexistant_email@test.tld', new UniqueEmail());

        $I->assertSame(0, $violations->count());
    }

    public function it_validate_with_success_an_existant_email(FunctionalTester $I)
    {
        $I->addAdminUserInRepository(['email' => 'valid_email@test.tld']);

        $violations = $this->validator->validate('valid_email@test.tld', new UniqueEmail());

        $I->assertSame(1, $violations->count());
    }
}
