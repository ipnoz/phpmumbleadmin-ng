<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Validator;

use App\Tests\FunctionalTester;
use App\Tests\Page\CertificatePage;
use App\Infrastructure\Symfony\Validator\Constraints\Certificate;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CertificateValidatorCest
{
    private ValidatorInterface $validator;

    public function _before(FunctionalTester $I): void
    {
        $this->validator = $I->grabService(ValidatorInterface::class);
    }

    public function it_validate_with_success_a_valid_certificate(FunctionalTester $I)
    {
        $violations = $this->validator->validate(CertificatePage::PEM, new Certificate());

        $I->assertSame(0, $violations->count());
    }

    public function it_does_not_validate_an_invalid_certificate(FunctionalTester $I)
    {
        $violations = $this->validator->validate('invalid certificate', new Certificate());

        $I->assertSame(1, $violations->count());
        $I->assertInstanceOf(Certificate::class, $violations[0]->getConstraint());
        $I->assertSame('validator_certificate_error_message', $violations[0]->getMessage());
    }
}
