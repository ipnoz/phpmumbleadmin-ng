<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Command\Dev;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class LoadFixturesCommandInProdTest extends KernelTestCase
{
    public function test_it_run_command_app_load_fixtures_with_prod_environment(): void
    {
        // Given I'm on prod environment
        $kernel = self::bootKernel(['environment' => 'prod']);
        $application = new Application($kernel);
        $command = $application->find('app:load-fixtures');
        $commandTester = new CommandTester($command);

        // When I execute the command
        $commandTester->execute([]);

        // Then
        $exitCode = $commandTester->getStatusCode();
        $output = $commandTester->getDisplay();

        // I see the command return the error code and message
        $this->assertSame(Command::INVALID, $exitCode);
        $this->assertStringContainsString('Prod environment is not allowed to run this command', $output);
        $this->assertStringNotContainsString('Fast truncate workaround', $output);
        $this->assertStringNotContainsString('Truncate table admin', $output);
        $this->assertStringNotContainsString('loading', $output);
    }
}
