<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Symfony\Security\Voter;

use App\Domain\Service\SecurityServiceInterface as Security;
use App\Infrastructure\Symfony\Security\Voter\CanEditRoleVoter;
use App\Tests\FunctionalTester;
use Codeception\Example;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CanEditRoleVoterCest
{
    private Security $securityService;

    public function _before(FunctionalTester $I): void
    {
        $this->securityService = $I->grabService(Security::class);
    }

    protected function roleProvider(): array
    {
        return [
            ['loggedAs' => 'SuperAdmin', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'role' => Security::ROLE_ROOT_ADMIN, 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'role' => Security::ROLE_SUPER_USER, 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => true],

            ['loggedAs' => 'RootAdmin', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'RootAdmin', 'role' => Security::ROLE_ROOT_ADMIN, 'expected' => false],
            ['loggedAs' => 'RootAdmin', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => true],

            ['loggedAs' => 'Admin', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'Admin', 'role' => Security::ROLE_ADMIN, 'expected' => false],
            ['loggedAs' => 'Admin', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => true],

            ['loggedAs' => 'SuperUser', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'SuperUser', 'role' => Security::ROLE_SUPER_USER, 'expected' => false],
            ['loggedAs' => 'SuperUser', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => true],

            ['loggedAs' => 'SuperUserRu', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'SuperUserRu', 'role' => Security::ROLE_SUPER_USER_RU, 'expected' => false],
            ['loggedAs' => 'SuperUserRu', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => true],

            ['loggedAs' => 'MumbleUser', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'MumbleUser', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => false],

            ['loggedAs' => 'Anonymous', 'role' => Security::ROLE_SUPER_ADMIN, 'expected' => false],
            ['loggedAs' => 'Anonymous', 'role' => Security::ROLE_ADMIN, 'expected' => false],
            ['loggedAs' => 'Anonymous', 'role' => Security::ROLE_MUMBLE_USER, 'expected' => false],
        ];
    }

    /** @dataProvider roleProvider */
    public function it_test_if_a_logged_user_can_modify_a_role(FunctionalTester $I, Example $data): void
    {
        // Given I'm logged as
        $amLoggedAsMethod = 'amLoggedAs'.$data['loggedAs'];
        $I->$amLoggedAsMethod();

        // When I check if the logged user is granted to edit a role
        $result = $this->securityService->isGranted(CanEditRoleVoter::KEY, $data['role']);

        // Then I expect the right boolean
        $I->assertSame($data['expected'], $result);
    }

    protected function userProvider(): array
    {
        return [
            ['loggedAs' => 'SuperAdmin', 'user' => 'SuperAdmin', 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'user' => 'RootAdmin', 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'user' => 'SuperUser', 'expected' => true],
            ['loggedAs' => 'SuperAdmin', 'user' => 'MumbleUser', 'expected' => true],

            ['loggedAs' => 'RootAdmin', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'RootAdmin', 'user' => 'RootAdmin', 'expected' => false],
            ['loggedAs' => 'RootAdmin', 'user' => 'MumbleUser', 'expected' => true],

            ['loggedAs' => 'Admin', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'Admin', 'user' => 'Admin', 'expected' => false],
            ['loggedAs' => 'Admin', 'user' => 'MumbleUser', 'expected' => true],

            ['loggedAs' => 'SuperUser', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'SuperUser', 'user' => 'SuperUser', 'expected' => false],
            ['loggedAs' => 'SuperUser', 'user' => 'MumbleUser', 'expected' => true],

            ['loggedAs' => 'SuperUserRu', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'SuperUserRu', 'user' => 'SuperUser_Ru', 'expected' => false],
            ['loggedAs' => 'SuperUserRu', 'user' => 'MumbleUser', 'expected' => true],

            ['loggedAs' => 'MumbleUser', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'MumbleUser', 'user' => 'MumbleUser', 'expected' => false],

            ['loggedAs' => 'Anonymous', 'user' => 'SuperAdmin', 'expected' => false],
            ['loggedAs' => 'Anonymous', 'user' => 'SuperUser', 'expected' => false],
            ['loggedAs' => 'Anonymous', 'user' => 'MumbleUser', 'expected' => false],
        ];
    }

    /** @dataProvider userProvider */
    public function it_test_if_a_logged_user_can_modify_an_user(FunctionalTester $I, Example $data): void
    {
        // Given I'm logged as
        $amLoggedAsMethod = 'amLoggedAs'.$data['loggedAs'];
        $I->$amLoggedAsMethod();
        // And given I have the entity of user
        $createMethod = 'create'.$data['user'];
        $user = $I->$createMethod();

        // When I check if the logged user is granted to edit the user
        $result = $this->securityService->isGranted(CanEditRoleVoter::KEY, $user);

        // Then I expect the right boolean
        $I->assertSame($data['expected'], $result);
    }
}
