<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ServerChannelsPage extends ServerPage
{
    public const PATH = parent::PATH;
    public const LOCATION = parent::LOCATION;

    public const VIEWER = '#MumbleViewerContainer';
    public const VIEWER_PANEL = self::VIEWER.' #MumbleViewerPanel';
    public const VIEWER_MENU = self::VIEWER_PANEL.' #MumbleViewerActionMenu';
    public const VIEWER_CHANNELS = self::VIEWER.' .MumbleViewer';

    public const NODEJS_CLIENT1_NAME = 'node-js-client-001.js';
    public const NODEJS_CLIENT2_NAME = 'node-js-client-002.js';

    public const NODEJS_CLIENT_WITH_CERTIFICATE_NAME = self::NODEJS_CLIENT2_NAME;

    public const JS_CONFIRM_MODAL = '.jconfirm';
}
