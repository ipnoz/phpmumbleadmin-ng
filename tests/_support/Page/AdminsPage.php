<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

use App\Infrastructure\Symfony\Form\CreateAdminType;
use App\Infrastructure\Symfony\Form\EditAdminType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class AdminsPage extends AdministrationPage
{
    public const LOCATION = parent::LOCATION.'/admins';
    public const LOCATION_CREATE = self::LOCATION.'/create';
    public const LOCATION_EDIT = self::LOCATION.'/edit';
    public const LOCATION_DELETE = self::LOCATION.'/delete';

    public const CONTAINER = '#AdminsContainer';
    public const TABLE_ELEMENT = self::CONTAINER.' #table-admin-users';
    public const TABLE_CELL_ID = 1;
    public const TABLE_CELL_ROLE = 2;
    public const TABLE_CELL_LOGIN = 3;

    public const MODAL_CREATE = '#createAdminModal';
    public const CREATE_FORM_PREFIX = CreateAdminType::BLOCK_PREFIX;

    public const EDIT_FORM_PREFIX = EditAdminType::BLOCK_PREFIX;
    public const EDIT_FORM = 'form[name="'.self::EDIT_FORM_PREFIX.'"]';
}
