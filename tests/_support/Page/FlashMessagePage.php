<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class FlashMessagePage extends Page
{
    public const APP = '.app-flash';
    public const JS = '.app-flash-js';

    public const SUCCESS = self::APP.'.alert-success';
    public const ERROR = self::APP.'.alert-danger';
}
