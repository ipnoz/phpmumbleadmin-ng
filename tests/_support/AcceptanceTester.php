<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * Define custom actions here
     */
    use Step\Acceptance\DatabaseTesterActions;
    use Step\Acceptance\DropdownTesterActions;
    use Step\Acceptance\FormAjaxActions;
    use Step\Acceptance\JsEditorTesterActions;
    use Step\Acceptance\JsFlashMessagesActions;
    use Step\Acceptance\LocalesTesterActions;
    use Step\Acceptance\LoginTesterActions;
    use Step\Acceptance\LogsTesterActions;
    use Step\Acceptance\ModalTesterActions;
    use Step\Acceptance\ServerSettingsTesterActions;
    use Step\Acceptance\SfToolbarTesterActions;
    use Step\Acceptance\ViewerTesterActions;
    use Step\Common\CertificateTesterActions;
    use Step\Common\CreateEntities;
    use Step\Common\FileTesterActions;
    use Step\Common\FlashMessagesActions;
    use Step\Common\FormActions;
    use Step\Common\LoginTesterActions;
    use Step\Common\MenuTesterActions;
    use Step\Common\MumbleUserTesterActions;
    use Step\Common\MurmurActions;
    use Step\Common\OptionsCookieTesterActions;
    use Step\Common\ServerViewTesterActions;
    use Step\Common\TableActions;
    use Step\Common\TranslatorTesterActions;
    use Step\Common\ViewerTesterActions;
}
