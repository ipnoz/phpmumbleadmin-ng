<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

use App\Tests\Page\DashboardPage;
use App\Tests\Page\ModalPage;

const MODAL_FADE_EFFECT_IN_MILLISECOND = 200_000;

/**
 * Tester actions for the Viewer using the Webdriver module
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ModalTesterActions
{
    // The fade animation of the bootstrap modal block the bootstrap dismiss JS event
    public function waitForModalFadeEffect(string $element): void
    {
        $this->waitForElementVisible($element);
        // Wait for the end of the fade effect
        \usleep(MODAL_FADE_EFFECT_IN_MILLISECOND);
    }

    // The fade animation of the bootstrap modal block the bootstrap dismiss JS event
    public function waitForModalClosureWithFadeEffect(string $element): void
    {
        $this->waitForElementNotVisible($element);
        // Wait for the end of the fade effect
        \usleep(MODAL_FADE_EFFECT_IN_MILLISECOND);
    }

    public function confirmToDeleteOnTheModal(): void
    {
        $this->waitForElementVisible(ModalPage::DELETE_ID);
        $this->click('#deleteModal #confirm-delete-button');
    }

    public function waitForTheLoadingAnimation(): void
    {
        $this->waitForElement('.loading-animation');
    }

    public function waitForTheLoadingAnimationToEnd(): void
    {
        $this->waitForTheLoadingAnimation();
        $this->waitForElementNotVisible('.loading-animation');
    }

    public function openTheActionMenuOnTheDashboard(int $line): void
    {
        $this->click(DashboardPage::TABLE_ID.' tbody tr:nth-child('.$line.') td.icon .fa-ellipsis-h');
        $this->waitForElementVisible(DashboardPage::TABLE_ID.' tbody tr:nth-child('.$line.') td.icon .dropdown-menu');
    }

    public function seeActionOnTheActionMenuOfTheDashboard(string $action, string $icon, int $line): void
    {
        $this->see($action, DashboardPage::TABLE_ID.' tbody tr:nth-child('.$line.') td.icon .dropdown-menu');
        $this->seeElement(DashboardPage::TABLE_ID.' tbody tr:nth-child('.$line.') td.icon .dropdown-menu '.$icon);
    }
}
