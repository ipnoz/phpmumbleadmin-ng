<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

use App\Tests\Page\LoginPage;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LoginTesterActions
{
    public function amLoggedAs(string $login, string $password): void
    {
        $sessionSnapshot = 'login-'.$login;

        $I = $this;

        // if snapshot exists - skipping login
        if ($I->loadSessionSnapshot($sessionSnapshot)) {
            return;
        }

        $this->amOnPage(LoginPage::LOCATION);
        $I->seeCurrentUrlMatches('#'.LoginPage::LOCATION.'$#');

        $this->logIn($login, $password);

        $I->saveSessionSnapshot($sessionSnapshot);
    }

    public function amLoggedAsSuperAdmin(): void
    {
        $this->amLoggedAs(LoginPage::SUPER_ADMIN_USERNAME, LoginPage::SUPER_ADMIN_PASSWORD);
    }

    public function deleteSessionSnapshotForSuperAdmin(): void
    {
        $this->deleteSessionSnapshot('login-'.LoginPage::SUPER_ADMIN_USERNAME);
    }

    public function amNotLogged(): void
    {
        $this->deleteSessionSnapshotForSuperAdmin();
    }
}
