<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Acceptance;

/**
 * Tester actions for the Viewer using the Webdriver module
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait LocalesTesterActions
{
    public function seeTheLocaleIsSelectedInTheMenu(string $locale): void
    {
        // I see the flag of the selected locale on the dropdown button
        $this->seeElement('#locales-menu-button img#flag-id-'.$locale);
        // I see that the selected locale is the first locale on the menu
        $this->seeElementInDOM('ul[aria-labelledby="locales-menu-button"] li:nth-child(1)#locale-id-'.$locale);
        // I see the checked icon on the first locale
        $this->seeElementInDOM('ul[aria-labelledby="locales-menu-button"] li:nth-child(1) i.fa-check');
        // I see the locale locale don't have href element
        $this->dontSeeElementInDOM('ul[aria-labelledby="locales-menu-button"] li:nth-child(1) a');
    }

    public function seeTheLocaleIsSelectedInDOM(string $locale): void
    {
        $this->seeElementInDOM('html[lang="'.$locale.'"]');
    }

    public function openTheLocalesMenu(): void
    {
        $this->click('#locales-menu-button');
        $this->waitForElementVisible('ul[aria-labelledby="locales-menu-button"]');
    }

    public function clickOnTheLocaleIntoTheMenu(string $locale): void
    {
        $this->click('ul[aria-labelledby="locales-menu-button"] #locale-id-'.$locale);
    }

    public function selectTheLocaleOnTheMenu(string $locale): void
    {
        $this->openTheLocalesMenu();
        $this->clickOnTheLocaleIntoTheMenu($locale);
    }
}
