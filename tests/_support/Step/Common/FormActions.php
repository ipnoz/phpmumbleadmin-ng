<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\FormPage as Form;

/**
 * Action on the murmur server for tests
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait FormActions
{
    public function seeFormError(string $message): void
    {
        $this->see($message, 'form '.Form::ERROR_ELEMENT);
    }

    public function dontSeeFormError(string $message): void
    {
        $this->dontSee($message, 'form '.Form::ERROR_ELEMENT);
    }

    public function seeFormErrorOn(string $message, string $element): void
    {
        $this->see($message, $element.' '.Form::ERROR_ELEMENT);
    }

    public function dontSeeFormErrorOn(string $message, string $element): void
    {
        $this->dontSee($message, $element.' '.Form::ERROR_ELEMENT);
    }

    public function seeFieldIsRequired(string $field): void
    {
        $this->seeElement(['name' => $field, 'required' => true]);
    }

    public function seeFieldIsRequiredAndFillField(string $field, $value): void
    {
        $this->seeFieldIsRequired($field);
        $this->fillField($field, $value);
    }

    public function seeTooltip(string $text): void
    {
        $this->see($text, '.popover');
    }

    public function dontSeeTooltip(string $text): void
    {
        $this->dontSee($text, '.popover');
    }

    // Wait for the tooltip animation
    public function waitForTooltipAnimation(): void
    {
        \usleep(50_000);
    }

    public function seeTooltipFormErrorOn(string $text): void
    {
        $this->see($text, '[data-bs-tooltip="form-error"]');
    }

    public function dontSeeTooltipFormErrorOn(string $text): void
    {
        $this->dontSee($text, '[data-bs-tooltip="form-error"]');
    }
}
