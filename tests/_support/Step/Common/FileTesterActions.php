<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait FileTesterActions
{
    private function rrmdir(string $dir): void
    {
        if (! \is_dir($dir)) {
            return;
        }

        $objects = \scandir($dir);

        foreach ($objects as $object) {
            if ('.' !== $object && '..' !== $object) {
                if ('dir' === \filetype($dir.'/'.$object)) {
                    $this->rrmdir($dir.'/'.$object);
                } else {
                   \unlink($dir.'/'.$object);
                }
            }
        }

        \reset($objects);
        \rmdir($dir);
    }

    public function deleteTheSfCachePool(): void
    {
        $this->rrmdir(__DIR__.'/../../../../var/cache/test/pools');
    }
}
