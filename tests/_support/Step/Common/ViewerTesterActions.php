<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\ServerChannelsPage as Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ViewerTesterActions
{
    public function seeTheChannelInTheViewer(string $channelName): void
    {
        $this->see($channelName, Page::VIEWER_CHANNELS.' .channel');
    }

    public function dontSeeTheChannelInTheViewer(string $channelName): void
    {
        $this->dontSee($channelName, Page::VIEWER_CHANNELS.' .channel');
    }

    public function seeTheChannelInTheViewerIsSelected(string $channelName): void
    {
        $this->see($channelName, Page::VIEWER_CHANNELS.' .channel.selected');
    }

    public function seeTheMumbleUserInTheViewer(string $userName): void
    {
        $this->see($userName, Page::VIEWER_CHANNELS.' .user');
    }

    public function dontSeeTheMumbleUserInTheViewer(string $userName): void
    {
        $this->dontSee($userName, Page::VIEWER_CHANNELS.' .user');
    }

    public function selectMumbleClientInTheViewer(string $name = Page::NODEJS_CLIENT1_NAME): void
    {
        $this->click('//div[@class="MumbleViewer"]//div[contains(@class, "user")]//span[@class="name"][text()="'.$name.'"]');
    }

    public function selectChannelInTheViewer(string $name): void
    {
        $this->click('//div[@class="MumbleViewer"]//div[@class="channel"]//span[@class="name"][text()="'.$name.'"]');
    }

    public function clickTheSubmitButtonOfTheModalForm(string $modal): void
    {
        $this->click($modal.' form button[type="submit"]');
    }

    public function seeTheActionModeIsEnableOnTheViewer(): void
    {
        $this->seeElement(Page::VIEWER_CHANNELS);
        $this->seeElement(Page::VIEWER_CHANNELS.'.actionMode');
    }

    public function dontSeeTheActionModeIsEnableOnTheViewer(): void
    {
        $this->seeElement(Page::VIEWER_CHANNELS);
        $this->dontSeeElement(Page::VIEWER_CHANNELS.'.actionMode');
    }
}
