<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Common;

use App\Tests\Page\ServerChannelsPage;

const PATH_NODEJS_USERS = 'tests/_data/NodeJsMumbleUsers';

/**
 * Connect a fake Mumble clients with node.js
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait MumbleUserTesterActions
{
    /*
     * Check if the Node.js mumble client is up
     */
    private function nodeJsUserIsUp(string $name): bool
    {
        $output = \exec('ps aux | grep "node '.PATH_NODEJS_USERS.'/'.$name.'" | grep -v "grep"');
        return (false !== \strpos($output, 'node '.PATH_NODEJS_USERS.'/'.$name));
    }

    /**
     * Connect a nodeJs Mumble user to the test murmur.
     */
    public function haveOneMumbleClient(string $name = ServerChannelsPage::NODEJS_CLIENT1_NAME): void
    {
        $mumbleClientIsUp = $this->nodeJsUserIsUp($name);

        // Running the Node.js Mumble client.
        // Even if the client is up, run the command to reposition the client
        // to the initial on the Murmur tree.
        \exec('node '.PATH_NODEJS_USERS.'/'.$name.' > /dev/null 2>&1 &');

        // Let time to the Node.js client to connect if it wasn't online,
        // otherwise the test will fail...
        if (! $mumbleClientIsUp) {
            \sleep(1);
        }

        // If the Node.js client is still not connected, stop the test
        if (! $this->nodeJsUserIsUp($name)) {
            $this->fail('the Node.js Mumble client has failed to start');
        }
    }
}
