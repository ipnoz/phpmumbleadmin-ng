<?php

/*
 * This file is part of the PhpMumbleAdmin.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace App\Tests\Step\Functional;

use App\Entity;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait AddEntitiesInRepositoriesTesterActions
{
    public function addSuperAdminUserInRepository(array $data = []): Entity\AdminEntity
    {
        $id = $this->haveInRepository($this->createSuperAdmin($data));
        return $this->grabEntityFromRepository(Entity\AdminEntity::class, ['id' => $id]);
    }

    public function addRootAdminUserInRepository(array $data = []): Entity\AdminEntity
    {
        $id = $this->haveInRepository($this->createRootAdmin($data));
        return $this->grabEntityFromRepository(Entity\AdminEntity::class, ['id' => $id]);
    }

    public function addAdminUserInRepository(array $data = []): Entity\AdminEntity
    {
        $id = $this->haveInRepository($this->createAdmin($data));
        return $this->grabEntityFromRepository(Entity\AdminEntity::class, ['id' => $id]);
    }

    public function addForgottenPasswordInRepository(array $data = []): Entity\ForgottenPassword
    {
        $id = $this->haveInRepository($this->createForgottenPassword($data));
        return $this->grabEntityFromRepository(Entity\ForgottenPassword::class, ['id' => $id]);
    }

    public function addAdminLogInRepository(array $data = []): Entity\AdminLogEntity
    {
        $id = $this->haveInRepository($this->createAdminLog($data));
        return $this->grabEntityFromRepository(Entity\AdminLogEntity::class, ['id' => $id]);
    }
}
