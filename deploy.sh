#!/bin/bash

cd $(dirname "$0") || exit 1

source "./.env.deploy.local" || exit 1
source "./deploy.inc.sh" || exit 1

INITIAL_BRANCH=$(git rev-parse --abbrev-ref HEAD)

announce_command 'git checkout master'
git checkout master || shutdown 1

announce_command 'git merge dev'
git merge dev master -m "Merge branch 'dev'" || shutdown 1

announce_command 'git push'
git push || shutdown 1

announce_ssh_connection $APP_DEPLOY_USER@$APP_DEPLOY_HOST"/"$APP_DEPLOY_PATH

ssh -t $APP_DEPLOY_USER@$APP_DEPLOY_HOST "

    cd $APP_DEPLOY_PATH || exit 1

    echo 'Run command: git checkout master'
    git checkout master || exit 1

    echo Run command: git pull
    git pull || exit 1

    echo
    echo Run command: composer install
    composer install --no-dev --optimize-autoloader

    echo
    echo Run command: npm ci
    npm ci

    echo
    echo Run command: gulp rebuild
    gulp rebuild
"

shutdown 0
