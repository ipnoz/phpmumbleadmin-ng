(function($) {

    "use strict";

    if (typeof bootstrap === 'undefined' || typeof bootstrap.Popover === 'undefined') {
        return;
    }

    $('[data-bs-toggle="popover"], [data-bs-tooltip="popover"]').each(function() {
        new bootstrap.Popover($(this), {
            trigger: 'hover',
            placement: 'top'
        });
    });

})(jQuery);
