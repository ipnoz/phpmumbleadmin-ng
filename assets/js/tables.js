(function($) {

    'use strict';

    if (typeof $({}).DataTable !== 'function') {
        return;
    }

    let storagePrefix = 'phpMumbleAdmin_Datatables_';

    $(document).ready(function () {
        let datatablesOptions = {
            pagingType: 'full_numbers',
            stateSave: true,
            stateSaveCallback: function(settings, data) {
                sessionStorage.setItem(storagePrefix + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function(settings) {
                return JSON.parse(sessionStorage.getItem(storagePrefix + settings.sInstance));
            }
        };
        if ('en' !== getLanguage()) {
            datatablesOptions.language = {
                url: getBaseUrl() + '/assets/vendor/datatables/locales/'+getLanguage()+'.json'
            };
        }
        $('.dataTables').DataTable(datatablesOptions);
    });

})(jQuery);
